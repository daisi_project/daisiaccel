vals = [2211.45
2209.6
2209.29
2209.26
2210.42
2209.03
2209.79
2209.82
2209.31
2208.91
2209.72
2211.33
2210.11
2209.42
2209.7
2209.24
2208.7
2207.7
2209.36
2209.43
2209.83
2209.34
2208.72
2209.42
2209.76
2208.89
2208.71
2209.4
2208.59
2208.52
2208.76
2209.54
2208.74
2209.06
2208.76
2209.54
2210.84
]

default = 2200;

fileID = fopen('booster_errs.txt','w');
k=1;
for j=0:4
    for i=0:9
       if(k>length(vals))
           break;
       end
       fprintf(fileID,'SELECT, FLAG=ERROR, PATTERN = "bH%d_%d";\n', i, j);
       fprintf(fileID,'EFCOMP, order = 0, radius = 0.030, DKNR: = { %0.22f, 0, 0, 0 };\n\n', vals(k)/default-1);
       k=k+1;
    end
    fprintf(fileID,'\n');
end


