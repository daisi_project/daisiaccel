#include <cmath>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>

#include <armadillo>
#include <boost/property_tree/json_parser.hpp>
#include <gtest/gtest.h>

#include <common_tools/helpers.hpp>

#include <base/componentnames.h>
#include <base/imodel.h>
#include <base/modelnames.h>
#include <common/keywords.h>
#include <common/standartview.h>
#include <synchrotron/opticsequence.h>

#define STR1(x) #x
#define STR(x) STR1(x)

class TestModelBuild : public ::testing::Test
{
  protected:
    virtual void SetUp()
    {
        path = STR(TEST_PATH);
    }
    std::string path;
};

TEST_F(TestModelBuild, SynchrotronFail)
{
    // ASSERT_FALSE(daisi::build_model(daisi::modelnames::Synchrotron, {}));
}

TEST_F(TestModelBuild, test1)
{
    auto optic_content = commtools::file_to_string(path + "/booster.opt");
    auto line_content  = commtools::file_to_string(path + "/booster.line");
    auto errs_content  = commtools::file_to_string(path + "/booster_errs.txt");

    ASSERT_FALSE(optic_content.empty());

    std::map<std::string, std::string> content;

    content["optic"]  = optic_content;
    content["line"]   = line_content;
    content["errors"] = errs_content;

    daisi::accel::Sequence seq;

    seq.from_model_view("", content);
}
