#include <gtest/gtest.h>

#include <common_tools/helpers.hpp>

#include <synchrotron/solvers/tools.h>

#define STR1(x) #x
#define STR(x) STR1(x)

class TestLoad : public ::testing::Test
{
    virtual void SetUp()
    {
        path = STR(TEST_PATH);
    }

  protected:
    std::string path;
};

TEST_F(TestLoad, Test1)
{
    auto tt = daisi::accel::loadcm(commtools::file_to_string(path + "/mad_out_cm_0one"));

    int ttt = 0;
}