#include <cmath>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>

#include <armadillo>
#include <boost/property_tree/json_parser.hpp>
#include <gtest/gtest.h>

#include <common_tools/helpers.hpp>

#include <base/componentnames.h>
#include <base/imodel.h>
#include <base/modelnames.h>
#include <common/keywords.h>
#include <common/standartview.h>
#include <database/pqxxcommunicator.h>

#define STR1(x) #x
#define STR(x) STR1(x)

class TestConnection : public ::testing::Test
{
  protected:
    virtual void SetUp()
    {
        path = STR(TEST_PATH);
    }
    std::string path;
};

// TEST_F(TestConnection, Test1)
// {
//     auto connection_info = commtools::file_to_string(path + "/connection.json");

//     try
//     {

//         auto conn = std::make_shared<daisi::PQXXCommunicator>(connection_info);

//         auto rr = conn->make_query(
//             "SELECT row_to_json(t)  FROM (SELECT * FROM modelcomponentsviewssimpleitempattern) t");

//         auto data =
//             conn->get_model_view_json(0, "Optic_elements_sequence", "MADX_sequence_view", false);

//         int tt = 0;
//     }
//     catch (const std::exception& ex)
//     {
//         auto v  = ex.what();
//         int  tt = 0;
//     }
//     // ASSERT_FALSE(daisi::build_model(daisi::modelnames::Synchrotron, {}));
// }
