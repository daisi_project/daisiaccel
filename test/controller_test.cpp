#include <cmath>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>

#include <armadillo>
#include <boost/property_tree/json_parser.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <common_tools/gtest_extension.hpp>
#include <common_tools/helpers.hpp>

#include <base/idatacommunicator.h>
#include <base/imodel.h>
#include <base/imodelcomponent.h>

#include <base/modelnames.h>
#include <common/keywords.h>
#include <common/standartview.h>
#include <controller.h>
#include <database/pqxxcommunicator.h>

#define STR1(x) #x
#define STR(x) STR1(x)

using ::testing::DoAll;
using ::testing::Return;

class TestCommunicator : public daisi::IDataCommunicator
{
  public:
    MOCK_SPEC_METHOD5(noexcept, get_model_view_json,
                      daisi::q_res_t(const int model_id, const std::string& component_name,
                                     const std::string& pseudoname_name, const bool is_result,
                                     const std::string& name));

    MOCK_SPEC_METHOD1(noexcept, get_model_components_views, daisi::q_res_t(const int model_id));

    MOCK_METHOD3(add_results,
                 int(const int model_id, const std::string& solver_name, const std::string& name));
};

class TestController : public ::testing::Test
{
  protected:
    virtual void SetUp()
    {
        path = STR(TEST_PATH);

        communicator = (TestCommunicator*)(&daisi::IDataCommunicator::get<TestCommunicator>());
    }
    std::string       path;
    TestCommunicator* communicator;

    const int model_id = 0;

    const std::string component_seq = "Optic_elements_sequence";
    const std::string view_seq      = "MADX_sequence_view";

    const std::string component_flow = "Flow";
    const std::string view_flow      = "Flow_parameters";

    const std::string solver_cm_mad = "Center_mass_dynamics_madx";

    const std::string solver_cm_mad_name = "Center mass dynamics madx";

    void add_call_expect_seq()
    {

        std::map<std::string, std::string> row_optic;

        row_optic["get_model_view_json"] =
            "{\"content\"  : \"qF: QUADRUPOLE,  L=      0.47,  "
            "K1=0.8338714084;qF1: qF;\", \"item_typename\" : \"TextArea\", "
            "\"item_label\" : \"optic\"}";

        std::map<std::string, std::string> row_line;

        row_line["get_model_view_json"] =
            "{\"content\"  : \"MACHINE: LINE=(qF,qF);USE, period=MACHINE;\", "
            "\"item_typename\" : \"TextArea\", "
            "\"item_label\" : \"line\"}";

        EXPECT_CALL(*communicator, get_model_view_json(model_id, component_seq, view_seq, false,
                                                       solver_cm_mad_name))
            .WillOnce(Return(daisi::q_res_t{true, {row_optic, row_line}}));
    }

    void add_call_expect_solver_cm_mad()
    {

        // std::map<std::string, std::string> row;

        // row["get_model_view_json"] =
        //     "{\"content\"  : \"\", \"item_typename\" : \"TextArea\", "
        //     "\"item_label\" : \"optic\"}";

        EXPECT_CALL(*communicator,
                    get_model_view_json(model_id, solver_cm_mad, "", false, solver_cm_mad_name))
            .WillOnce(Return(daisi::q_res_t{true, {}}));
    }

    void add_call_expect_add_results()
    {
        EXPECT_CALL(*communicator, add_results(model_id, solver_cm_mad, solver_cm_mad_name))
            .WillOnce(Return(0));
    }

    void add_call_expect_flow()
    {
        std::map<std::string, std::string> row;

        row["get_model_view_json"] =
            "{\"content\"  : \"{\\\"Mass_number\\\": 1, \\\"Charge_number\\\": 1, "
            "\\\"Ehergy__eV\\\": 1e9, \\\"Center_mass_X__m\\\" : 0.001, "
            "\\\"Center_mass_Y__m\\\": 0.001, \\\"Center_mass_dX___dZ__rad\\\": "
            "0.001, \\\"Center_mass_dY___dZ__rad\\\": 0.001}\", \"item_typename\" : "
            "\"VariablesList\",\"item_label\" : \"\"}";
        EXPECT_CALL(*communicator, get_model_view_json(model_id, component_flow, view_flow, false,
                                                       solver_cm_mad_name))
            .WillOnce(Return(daisi::q_res_t{true, {row}}));
    }

    void add_call_expect_components_views()
    {
        std::map<std::string, std::string> row_sequence;
        std::map<std::string, std::string> row_flow;
        row_sequence["get_model_components_views"] = "{\"component_type\" : "
                                                     "\"Optic_elements_sequence\", \"label\" "
                                                     ": \"MADX_sequence_view\"}";

        row_flow["get_model_components_views"] = "{\"component_type\" : "
                                                 "\"Flow\", \"label\" "
                                                 ": \"Flow_parameters\"}";

        EXPECT_CALL(*communicator, get_model_components_views(model_id))
            .WillOnce(Return(daisi::q_res_t{true, {row_sequence, row_flow}}));
    }
};

TEST_F(TestController, Test1)
{

    add_call_expect_seq();

    daisi::DaisiController controller(2);

    auto result = controller.make_model_component(model_id, component_seq, view_seq,
                                                  "Optic elements sequence");

    EXPECT_TRUE(result);
}

TEST_F(TestController, Test2)
{

    add_call_expect_seq();

    add_call_expect_components_views();

    add_call_expect_flow();

    daisi::DaisiController controller(2);

    auto result = controller.make_model(model_id);
    EXPECT_TRUE(result);
}

TEST_F(TestController, Test3)
{

    add_call_expect_solver_cm_mad();

    daisi::DaisiController controller(2);

    auto result = controller.make_solver(model_id, solver_cm_mad, "Center mass dynamics madx");
    EXPECT_TRUE(result);
}

TEST_F(TestController, Test4)
{

    add_call_expect_seq();

    add_call_expect_components_views();

    add_call_expect_flow();

    add_call_expect_solver_cm_mad();

    add_call_expect_add_results();

    daisi::DaisiController controller(2);

    auto result =
        controller.run_calculation(0, model_id, solver_cm_mad, "Center mass dynamics madx");
    EXPECT_TRUE(result);
}
