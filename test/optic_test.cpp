#include <cmath>
#include <iostream>
#include <sstream>
#include <string>

#include <armadillo>
#include <gtest/gtest.h>

#include <synchrotron/opticelements.h>
#include <synchrotron/opticsequence.h>

#define STR1(x) #x
#define STR(x) STR1(x)

class TestLoadOptics : public ::testing::Test
{
  protected:
    virtual void SetUp()
    {
        path = STR(TEST_PATH);
    }
    std::shared_ptr<daisi::accel::IOpticElement> from_madx(const std::string& data)
    {
        return daisi::accel::Sequence::from_madx(data);
    }
    std::string path;
};

std::vector<int> t = {0, 1, 2, 3, 4};

TEST_F(TestLoadOptics, testFailQUADRUPOLE)
{
    std::vector<std::shared_ptr<int>> v;
    v.push_back(std::shared_ptr<int>(&t[0]));
    v.push_back(std::shared_ptr<int>(&t[2]));
    v.push_back(std::shared_ptr<int>(&t[4]));

    

    ASSERT_TRUE(from_madx("qF: QUADRUPOLE,  L=      0.47,  K1=0.8338714084;"));
    ASSERT_FALSE(from_madx("qF: QUADRUPOLE, K1=0.1,  L=-0.47;"));
    ASSERT_TRUE(from_madx("qF: QUADRUPOLE,  L=      0.47,  K1=0.8338714084;"));

    ASSERT_FALSE(from_madx("qF: QUADRUPOLE,  L=      0.47;"));
    ASSERT_FALSE(from_madx("qF: QUADRUPOLE;"));
    ASSERT_FALSE(from_madx("qF: QUADRUPOLE,  Lds=      0.47,  K1=0.8338714084;"));
    ASSERT_FALSE(from_madx("qF: QUADRUPOLEf,  L=      0.47,  K1=0.8338714084;"));
    ASSERT_FALSE(from_madx("qF: QUADRUPOLE,  L=      0.47, K1t=0.833871408fdfd4;"));
    ASSERT_FALSE(from_madx("qF: QUADRUPOLE,  L=      0.47,  K1=0.8338714084, h;"));
    ASSERT_FALSE(from_madx("qF: QUADRUPOLE,  L=      fdfdf,  K1=0.8338714084;"));
    ASSERT_FALSE(from_madx("qF: QUADRUPOLE,  1=      L,  K1=0.8338714084;"));
}

TEST_F(TestLoadOptics, QUADRUPOLE)
{
    auto test = [&](const std::string& data, const double L, const double K1) {

        auto el = from_madx(data);

        ASSERT_TRUE(el);
        EXPECT_EQ("QUADRUPOLE", el->get_key());

        auto el_ch = static_cast<daisi::accel::Quadrupole*>(el.get());

        EXPECT_DOUBLE_EQ(L, el_ch->L);
        EXPECT_DOUBLE_EQ(K1, el_ch->K1);

    };

    test("qF: QUADRUPOLE,  L=      0.47,  K1=0.8338714084;", 0.47, 0.8338714084);
    test("qF: QUADRUPOLE, K1=0.1,  L=1.47;", 1.47, 0.1);
}
