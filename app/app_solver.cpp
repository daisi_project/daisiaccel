#include <iostream>
#include <memory>

#include <common_tools/boostlog.hpp>
#include <common_tools/threadpool.hpp>

#include <daisiaccel/interface.hpp>
#include "../src/base/idatacommunicator.h"

class PGGLSINK : public commtools::BaseSink
{
  public:
    BUILD_CHILD(PGGLSINK, commtools::BaseSink)
    void consume(logging::record_view const& rec, const std::string& command_line,
                 const std::string& tag) noexcept override final
    {
        daisi::IDataCommunicator::get().set_task_log_data(std::stoi(tag), command_line);
    }
};

REGISTER_CHILD_NON_TEMPLATE(PGGLSINK, commtools::BaseSink, "PGGLSINK")

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        BL_ERROR() << "Invalid number of arguments";
        return 0;
    }

    try
    {
        daisi::DaisiSolverController controller(argv[1], "PGGLSINK", true);
        controller.run();
    }
    catch (const std::exception& ex)
    {
        BL_ERROR() << "Error creation controller: " << ex.what();
        return 0;
    }

    return 0;
}
