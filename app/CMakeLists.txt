# Test framework
project(daisiaccel_app)

find_package(Threads REQUIRED)

aux_source_directory(. SOURCES)

add_executable(${PROJECT_NAME} app.cpp)
target_link_libraries(
                     ${PROJECT_NAME}
                     ${CMAKE_THREAD_LIBS_INIT}
                     daisiaccel
                     )
                  

add_executable(${PROJECT_NAME}_solver app_solver.cpp)
target_link_libraries(
                     ${PROJECT_NAME}_solver
                     ${CMAKE_THREAD_LIBS_INIT}
                     daisiaccel
                     )