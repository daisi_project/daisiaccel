import psutil
import os
import time
import socket
import psycopg2
import sys
import json

def checkIfProcessRunning(processName):
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False

is_added = False

if len(sys.argv) != 2:
     print('incorrect number of daisi arguments')
  
else:

    try:     
        with open(str(sys.argv[1])) as json_file:  
                data = json.load(json_file) 
                conn = psycopg2.connect(port=data['db']['port'], host=data['db']['host'], dbname=data['db']['db'], user=data['db']['user'], password=data['db']['password'])

        ip = str(socket.gethostbyname(socket.gethostname()))
        cur = conn.cursor()

        cur.execute("SELECT * FROM services WHERE ip = %s", [ip])
        rows = cur.fetchall()

        if len(rows) == 0:
            print('Register new service on ip ' + ip)
            print(cur.execute("INSERT INTO services(ip, status) VALUES(%s, %s)", [ip, 'Starting']))
            is_added = True

        conn.commit()

        cur.close()


        while(1):

            cur = conn.cursor()

            cur.execute("SELECT * FROM services WHERE ip = %s", [ip])
            status = cur.fetchone()[1]

            print("*** Check if a process is running or not ***")

            new_status = ''

            if checkIfProcessRunning('daisiaccel_app_solver'):
                print('daisi is running on' + ip)
                new_status = 'Work'

                if status == 'Stopping':
                    os.system('sudo service daisi_service stop')
                    new_status = 'Stop'

            else:
                new_status = 'Stop'
                print('daisi is off now')
                if status == 'Starting':
                    print('start daisi')
                    os.system('sudo service daisi_service start')
                    new_status = 'Work'

            cur.execute("UPDATE services SET status = %s WHERE ip = %s", [new_status, ip])

            conn.commit()

            cur.close()

            time.sleep(1)

    except Exception as err:
        print(str(err))
   

if is_added:
    cur = conn.cursor()
    cur.execute("DELETE FROM services WHERE ip=%s", [ip])

    cur.close()
    conn.commit()

    conn.close()
