#include <iostream>
#include <memory>

#include <common_tools/boostlog.hpp>

#include "../src/base/idatacommunicator.h"
#include <daisiaccel/interface.hpp>


std::string name_user;

class PGGLSINK : public commtools::BaseSink
{
  public:
    BUILD_CHILD(PGGLSINK, commtools::BaseSink)
    void consume(logging::record_view const& rec, const std::string& command_line,
                 const std::string& tag) noexcept override final
    {
        std::cout << "set log " << name_user << std::endl;

        daisi::IDataCommunicator::get().set_current_log_data(name_user, command_line);
    }
};

REGISTER_CHILD_NON_TEMPLATE(PGGLSINK, commtools::BaseSink, "PGGLSINK")

int main(int argc, char** argv)
{

    if (argc < 3)
    {
        BL_ERROR() << "Invalid number of arguments";
        return 1;
    }

    bool        success = false;
    std::string operation;

    try
    {
        daisi::DaisiSolverController controller(argv[1], "PGGLSINK", false);

        operation = argv[2];

        BL_INFO() << "Try to perform operation: " << operation;
        name_user = argv[3];

        daisi::IDataCommunicator::get().clear_current_log_data(name_user);

        if (operation == "check_model_component")
        {
            int arg1;

            std::string arg2;
            std::string arg3;
            std::string arg4;

            // if (argc > 5)
            // {
            //     arg1 = std::stoi(argv[3]);
            //     arg2 = argv[4];
            //     arg4 = argv[5];
            // }
            // else
            // {
            //     BL_ERROR() << "Incorrect number of arguments";
            //     return 1;
            // }

            // if (argc == 6)
            // {
            //     arg3 = "";
            // }
            // else
            // {
            //     arg3 = argv[6];
            // }

            // std::string arg_last = argv[5];
            // for (size_t i = 6; i < argc; i++)
            // {
            //     arg_last += " ";
            //     arg_last += argv[i];
            // }

            arg1 = std::stoi(argv[4]);
            arg2 = argv[5];
            arg3 = argv[6];
            for (size_t i = 7; i < argc - 1; i++)
            {
                arg3 += " ";
                arg3 += argv[i];
            }
            arg4 = argv[argc - 1];

            if (arg4 == "empty_")
            {
                arg4 = "";
            }

            success = controller.check_model_component(arg1, arg2, arg4, arg3);
        }
        else if (operation == "add_task")
        {
            if (argc < 7)
            {
                BL_ERROR() << "Incorrect number of arguments";
                return 1;
            }
            std::string arg_last = argv[6];
            for (size_t i = 7; i < argc; i++)
            {
                arg_last += " ";
                arg_last += argv[i];
            }

            success = controller.add_task(std::stoi(argv[4]), argv[5], arg_last);
        }
        else
        {
            BL_ERROR() << "Unknown operation: " << operation;
            return 1;
        }
    }
    catch (const std::exception& ex)
    {
        BL_ERROR() << "Error creation controller: " << ex.what();
        // return 1;
    }

    if (success)
    {
        BL_INFO() << "Success perform operation: " << operation;
        return 0;
    }
    BL_INFO() << "Fail to perform operation: " << operation;

    return 1;
}
