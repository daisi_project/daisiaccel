#include <memory>
#include <string>

#include <common_tools/propagate_const.hpp>

namespace daisi
{
class DaisiController;
class DaisiSolverController
{
  public:
    explicit DaisiSolverController(const std::string& config_path, const std::string& sink,
                                   const bool is_solver);

    ~DaisiSolverController();

    bool check_model_component(const int model_id, const std::string& component_pseudoname,
                               const std::string& view_pseudoname,
                               const std::string& name) noexcept;

    bool run() noexcept;

    bool add_task(const int model_id, const std::string& solver_pseudoname,
                  const std::string& name) noexcept;

  private:
    void init(const std::string& config, const std::string& sink, const bool is_solver);

    commtools::pc_unique_ptr<DaisiController> m_controller;
};
} // namespace daisi
