#!/bin/bash

cd /opt/daisi/daisiaccel_build/debug
cmake -DCMAKE_BUILD_TYPE=Debug ../../daisiaccel
make -j4
cp /opt/daisi/daisiaccel/app/config.json /opt/daisi/daisiaccel_build/debug/app/config.json