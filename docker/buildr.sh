#!/bin/bash

cd /opt/daisi/daisiaccel_build/release
cmake -DCMAKE_BUILD_TYPE=Release ../../daisiaccel
make -j4
cp /opt/daisi/daisiaccel/app/config.json /opt/daisi/daisiaccel_build/release/app/config.json