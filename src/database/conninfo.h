#ifndef DAISI_CONNINFO_H
#define DAISI_CONNINFO_H

#include <string>

#include <serreflection/defines.hpp>

namespace daisi
{
struct ConnectionInfo
{
    std::string host;
    std::string port;
    std::string db;
    std::string user;
    std::string password;
};
}

SERIALIZIBLE_STRUCT(daisi::ConnectionInfo, srfl::CheckModes::FATAL,
                    (std::string, host, DEF_D())(std::string, port, DEF_D())(
                        std::string, db, DEF_D())(std::string, user, DEF_D())(std::string, password,
                                                                              DEF_D()))

#endif
