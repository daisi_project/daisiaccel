#ifndef DAISI_PQXXCOMMUNICATOR_H
#define DAISI_PQXXCOMMUNICATOR_H

#include <list>
#include <map>
#include <mutex>
#include <string>

#include <pqxx/connection>

#include <common_tools/propagate_const.hpp>
#include <common_tools/query_former.hpp>

#include "../base/idatacommunicator.h"
#include "conninfo.h"

namespace daisi
{
class PQXXCommunicator final : public IDataCommunicator
{
  public:
    friend class IDataCommunicator;

    virtual ~PQXXCommunicator();

    q_res_t get_model_view_json(const int model_id, const std::string& component_name,
                                const std::string& pseudoname_name, const bool is_result,
                                const std::string& name) noexcept override final;

    int add_results(const int model_id, const std::string& solver_name,
                    const std::string& name) override final;

    q_res_t get_model_components_views(const int model_id) noexcept override final;

    q_res_t get_tasks() noexcept override final;

    q_res_t make_query(const std::string& query) noexcept;

    bool set_result_status(const int results_id, const std::string& status) noexcept override final;

    bool set_result_progress(const int results_id, const double& progress) noexcept override final;

    bool set_model_component_valid(const int model_id, const std::string& component_name,
                                   const bool         is_valid,
                                   const std::string& name) noexcept override final;

    bool set_result_data(const int results_id, const std::string& plot_label,
                         const std::vector<std::vector<float>>& x_data,
                         const std::vector<std::vector<float>>& y_data) noexcept override final;

    bool set_result_text_data(const int results_id, const std::string& label,
                              const std::string& content) noexcept override final;

    bool set_result_data(const int results_id, const std::string& plot_label,
                         const std::vector<std::vector<float>>& x_data,
                         const std::vector<std::vector<float>>& y_data,
                         const std::vector<std::string>&        labels) noexcept override final;

    bool set_current_log_data(const std::string& user_name,
                              const std::string& content_in) noexcept override final;

    bool clear_current_log_data(const std::string& user_name) noexcept override final;

    bool set_task_log_data(const int& id, const std::string& content_in) noexcept;

  private:
    template <class... Args>
    q_res_t make_query_p(const std::string& f_name, const Args&... args) noexcept
    {
        return make_query(commtools::QueryFormer::make_func_query(f_name, args...));
    }

    std::mutex m_mutex;

    void init(const ConnectionInfo& conn_info);

    static std::list<std::map<std::string, std::string>>
    query_to_string(const pqxx::result& query_res);

    explicit PQXXCommunicator(const std::string& conn_info);
    explicit PQXXCommunicator(const ConnectionInfo& conn_info);

    commtools::pc_unique_ptr<pqxx::connection> connection;
};
} // namespace daisi

#endif
