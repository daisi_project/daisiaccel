#include <iomanip>

#include <pqxx/transaction>
#include <pqxx/result_iterator.hxx>

#include <common_tools/boostlog.hpp>
#include <serreflection/read_json.hpp>

#include "conninfo.h"
#include "pqxxcommunicator.h"

namespace daisi
{

PQXXCommunicator::~PQXXCommunicator() = default;

void PQXXCommunicator::init(const ConnectionInfo& conn_info)
{
    std::ostringstream conn_string("");
    conn_string << "host=" << conn_info.host << " port=" << conn_info.port
                << " user=" << conn_info.user << " password=" << conn_info.password
                << " dbname=" << conn_info.db;

    auto constr = conn_string.str();

    connection = std::make_unique<pqxx::connection>(constr);
}

PQXXCommunicator::PQXXCommunicator(const std::string& conn_info)
{
    auto conn_struct = srfl::read_json_string<ConnectionInfo>(conn_info);

    if (!conn_struct)
    {
        throw std::runtime_error("Error read connection parameters");
    }

    init(*conn_struct);
}

bool PQXXCommunicator::set_result_data(const int results_id, const std::string& plot_label,
                                       const std::vector<std::vector<float>>& x_data,
                                       const std::vector<std::vector<float>>& y_data,
                                       const std::vector<std::string>&        labels) noexcept
{
    return make_query_p("set_result_data_ext", results_id, plot_label, x_data, y_data, labels)
        .first;
}

bool PQXXCommunicator::set_result_data(const int results_id, const std::string& plot_label,
                                       const std::vector<std::vector<float>>& x_data,
                                       const std::vector<std::vector<float>>& y_data) noexcept
{
    return make_query_p("set_result_data", results_id, plot_label, x_data, y_data).first;
}

bool PQXXCommunicator::set_result_text_data(const int results_id, const std::string& label,
                                            const std::string& content) noexcept
{
    return make_query_p("set_result_text_data", results_id, label, content).first;
}

PQXXCommunicator::PQXXCommunicator(const ConnectionInfo& conn_info)
{
    init(conn_info);
}

std::list<std::map<std::string, std::string>>
PQXXCommunicator::query_to_string(const pqxx::result& query_res)
{
    std::list<std::map<std::string, std::string>> result;
    for (auto i = query_res.begin(), r_end = query_res.end(); i != r_end; ++i)
    {
        result.emplace_back();
        auto& inseted = result.back();
        for (auto f = i->begin(), f_end = i->end(); f != f_end; ++f)
        {
            inseted[f->name()] = f->c_str();
        }
    }
    return result;
}

q_res_t PQXXCommunicator::get_model_view_json(const int model_id, const std::string& component_name,
                                              const std::string& pseudoname_name,
                                              const bool         is_result,
                                              const std::string& name) noexcept
{
    return make_query_p("get_model_view_items_join_json", model_id, component_name, pseudoname_name,
                        is_result, name);
}

q_res_t PQXXCommunicator::get_tasks() noexcept
{
    return make_query_p("get_tasks");
}

bool PQXXCommunicator::set_current_log_data(const std::string& user_name,
                                            const std::string& content_in) noexcept
{
    return make_query_p("set_current_log_data", user_name, content_in).first;
}

bool PQXXCommunicator::set_task_log_data(const int& id, const std::string& content_in) noexcept
{
    return make_query_p("set_task_log_data", id, content_in).first;
}

bool PQXXCommunicator::clear_current_log_data(const std::string& user_name) noexcept
{
    return make_query_p("clear_current_log_data", user_name).first;
}

bool PQXXCommunicator::set_result_status(const int results_id, const std::string& status) noexcept
{
    return make_query_p("set_result_status", results_id, status).first;
}

bool PQXXCommunicator::set_result_progress(const int results_id, const double& progress) noexcept
{
    return make_query_p("set_result_progress", results_id, progress).first;
}

bool PQXXCommunicator::set_model_component_valid(const int          model_id,
                                                 const std::string& component_name,
                                                 const bool         is_valid,
                                                 const std::string& name) noexcept
{
    return make_query_p("set_model_component_valid", model_id, component_name, is_valid, name)
        .first;
}

q_res_t PQXXCommunicator::make_query(const std::string& query) noexcept
{
    std::lock_guard<std::mutex> lock(m_mutex);

    try
    {
        pqxx::work xact(*connection);

        auto result = std::make_pair(true, query_to_string(xact.exec(query)));

        xact.commit();

        return result;
    }
    catch (const std::exception& ex)
    {
        // BL_ERROR() << "Failed to execute query: " << query << " : " << ex.what();
        return std::make_pair(false, std::list<std::map<std::string, std::string>>{});
    }
}

int PQXXCommunicator::add_results(const int model_id, const std::string& solver_name,
                                  const std::string& name)
{
    auto tmp = make_query_p("add_results", model_id, solver_name, name);

    if (tmp.second.size() != 1 || !tmp.first)
    {
        throw std::runtime_error("Invalid add_results request result");
    }
    return std::stoi(tmp.second.front().at("add_results"));
}

q_res_t PQXXCommunicator::get_model_components_views(const int model_id) noexcept
{
    return make_query_p("get_model_components_views_json", model_id);
}
} // namespace daisi
