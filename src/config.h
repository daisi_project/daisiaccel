#ifndef DAISI_CONFIG
#define DAISI_CONFIG

#include <common_tools/boostlog.hpp>
#include <serreflection/defines.hpp>

#include "database/conninfo.h"

namespace daisi
{
struct DAISIConfig
{
    commtools::BoostLogConfig log;

    ConnectionInfo db;

    int max_tasks;
    int thread_pool_size;
};
}
SERIALIZIBLE_STRUCT(commtools::BoostLogConfig, srfl::CheckModes::FATAL,
                    (std::string, level, DEF_D())(std::string, path,
                                                  DEF_D())(std::string, general_file, DEF_D()))

SERIALIZIBLE_STRUCT(daisi::DAISIConfig, srfl::CheckModes::FATAL,
                    (commtools::BoostLogConfig, log,
                     DEF_D())(daisi::ConnectionInfo, db,
                              DEF_D())(int, max_tasks, DEF_D())(int, thread_pool_size, DEF_D()))

#endif
