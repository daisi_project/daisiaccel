#include <common_tools/constants.h>
#include <numeric>
#include <type_traits>

#include "../base/idatacommunicator.h"

#include "gesadevice.h"
#include "gesapicsim.h"
#include "simpleflow.h"

namespace daisi
{
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(GESAPICSim, daisi::IModelComponent, "Gesa_PIC_sim")

template <class T>
using Container = List<T>;
using CalcType  = float;

SolverResult GESAPICSim::run_concrete_solver(const int                      results_id,
                                             const std::unique_ptr<IModel>& model) const noexcept
{

    SolverResult result;
    result.status = false;

    auto device   = model->get_component<GesaDevice>("Gesa device");
    auto electons = model->get_component<SimpleFlow>("Electron flow");
    auto ions     = model->get_component<SimpleFlow>("Ion flow");

    const double T  = 120e3;
    const double tp = 50e-9;

    int it = 0;

    const double R0          = device->get_anode_radius();
    const double v0          = 5600;
    const double L           = 0.5;
    const double sigma_alpha = 1.6e-20;

    auto mesh = device->get_gesa_mesh(m_parameters->number_of_mesh_steps);

    std::vector<decltype(std::mem_fn(&SimpleFlow::get_gesa_flow<CalcType, Container>))::result_type>
        flows(2);

    flows[0] = electons->get_gesa_flow<CalcType, Container>(device->get_cathode_radius(),
                                                            device->get_cathode_radius(), R0, -1.0);

    flows[1] = ions->get_gesa_flow<CalcType, Container>(R0, device->get_cathode_radius(), R0, 1.0);

    const auto v_anode = device->get_v_anode();

    const auto beta_max =
        (*std::max_element(flows.begin(), flows.end(),
                           [&](const auto& v1, const auto& v2) {
                               return v1->get_beta_by_T(v_anode) < v2->get_beta_by_T(v_anode);
                           }))
            ->get_beta_by_T(v_anode);

    double t = 0;

    const auto dt = mesh.get_regular_step() / (beta_max * m_parameters->CFL_value);

    std::vector<float>  j_el;
    std::vector<float>  times;
    std::vector<float>  j_io;
    std::vector<double> j_el_all;
    std::vector<float>  Rs;
    std::vector<float>  E_cat;
    std::vector<float>  E_an;

    double       j_anode;
    double       intergal_j = 0;
    double       j_ions     = 0;
    const double dt_s       = dt / commtools::LIGHT_VELOCITY();
    double       int_j      = 0;
    double       Rp         = R0;

    mesh.recalculate_v(m_parameters->Poisson_tolerance);

    size_t step = 0;

    size_t n_points = 500;

    double n_steps_est = m_parameters->max_time * 1e-9 * commtools::LIGHT_VELOCITY() / dt;

    size_t each_point = std::max(size_t(n_steps_est / n_points), size_t(1));

    while (t < m_parameters->max_time * 1e-9 * commtools::LIGHT_VELOCITY())
    {
        auto j_el_cur = mesh.get_current(flows[0]->get_charge(), flows[0]->get_mass(), true);
        // j_ions        = mesh.get_current(flows[1]->get_charge(), flows[1]->get_mass(), false);

        j_el_all.push_back(j_el_cur);

        if (step % each_point == 0)
        {
            times.push_back(1e9 * t / commtools::LIGHT_VELOCITY());

            j_el.push_back(j_el_cur);
            j_io.push_back(j_ions);
            E_cat.push_back(mesh.get_E().back());
            E_an.push_back(mesh.get_E()[0]);
            Rs.push_back(Rp);

            while (!IDataCommunicator::get().set_result_progress(
                results_id, step / static_cast<double>(n_steps_est)))
                ;
        }
        mesh.zeroing_field();

        flows[0]->set_current_density(j_el_cur);
        flows[1]->set_current_density(j_ions);

        for (size_t i_flow = 0; i_flow < flows.size(); i_flow++)
        {
            auto& flow = flows[i_flow];
            flow->add_particle(dt);
            flow->calculate_W(mesh.get_anode_radius(), mesh.get_first_step(),
                              mesh.get_regular_step());
            mesh.add_charges<CalcType, Container>(flow->get_W(), flow->get_charges());
        }
        mesh.charge_to_density();
        mesh.recalculate_v(m_parameters->Poisson_tolerance);

        for (size_t i_flow = 0; i_flow < flows.size(); i_flow++)
        {
            auto&      flow = flows[i_flow];
            const auto E    = mesh.get_fields(flow->get_W());
            flow->update_momentums(E, dt);
            flow->update_positions(dt);
        }

        j_anode = flows[0]->get_anode_current(dt_s);
        j_anode = j_el_cur * device->get_cathode_radius() / R0;

        auto get_jt = [&](const double t, const double je) -> double {
            return R0 * sigma_alpha * je * je * std::log((R0 + v0 * t) / R0) /
                   (std::abs(commtools::ELECTRON_CHARGE()) * v0);
        };

        // j_anode = 84460;

        if (t / commtools::LIGHT_VELOCITY() > 0.001538843908981357)
        {
            static double j_lim    = j_ions;
            static double j_anode_ = j_anode;

            static double tp_real =
                (R0 / v0) * (std::exp(std::abs(commtools::ELECTRON_CHARGE()) * v0 * j_lim /
                                      (R0 * sigma_alpha * j_anode_ * j_anode_)) -
                             1.0);

            // static double tp_real = 0.001622588180726;

            it++;
            // std::cout << std::setprecision(17) << "dt=" << dt << std::endl;
            double t_real = tp_real + dt_s * it;

            int_j = int_j + get_jt(t_real, j_anode_) * dt_s;

            double Ni = (2 * M_PI * R0 * L / std::abs(commtools::ELECTRON_CHARGE())) *
                        (int_j - j_lim * dt_s * it);

            Rp = std::sqrt(R0 * R0 + std::sqrt(T * std::abs(commtools::ELECTRON_CHARGE()) /
                                               commtools::ELECTRON_MASS()) *
                                         0.5 * std::abs(commtools::ELECTRON_CHARGE()) * Ni * Rp /
                                         (j_lim * M_PI * R0 * L));
        }

        const double dr = v0 * dt_s;

        // intergal_j += j_anode * dt_s;
        intergal_j = j_anode * std::log((R0 + v0 * t / commtools::LIGHT_VELOCITY()) / R0);

        // for (size_t i = 0; i < j_el_all.size(); i++)
        // {
        //     const double j_el_pr = j_el_all[i];
        //     const double r =
        //         R0 + 5600 * i * dt_s / commtools::LIGHT_VELOCITY();

        //     intergal_j +=
        //         j_el_pr * dr *
        //         (t / commtools::LIGHT_VELOCITY() - (r - R0) / 5600) / r;
        // }

        j_ions =
            R0 * 1.6e-20 * j_anode * intergal_j / (std::abs(commtools::ELECTRON_CHARGE()) * 5600);

        for (size_t i_flow = 0; i_flow < flows.size(); i_flow++)
        {
            flows[i_flow]->remove_particles();
        }

        t += dt;
        step++;
    }

    result.plots.emplace_back();
    auto& plot1 = result.plots.back();
    plot1.label = "Electron current density";
    plot1.XData.push_back(times);
    plot1.YData.push_back(j_el);

    result.plots.emplace_back();
    auto& plot2 = result.plots.back();
    plot2.label = "Ion current density";
    plot2.XData.push_back(times);
    plot2.YData.push_back(j_io);

    result.plots.emplace_back();
    auto& plot3 = result.plots.back();
    plot3.label = "Cloud radius";
    plot3.XData.push_back(times);
    plot3.YData.push_back(Rs);

    result.plots.emplace_back();
    auto& plot4 = result.plots.back();
    plot4.label = "Cathode field";
    plot4.XData.push_back(times);
    plot4.YData.push_back(E_cat);

    result.plots.emplace_back();
    auto& plot5 = result.plots.back();
    plot5.label = "Anode field";
    plot5.XData.push_back(times);
    plot5.YData.push_back(E_an);

    result.status = true;

    return result;
}
} // namespace accel
} // namespace daisi
