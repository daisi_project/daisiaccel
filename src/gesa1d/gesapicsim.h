#ifndef DAISI_GESA_PIC_SIM_H
#define DAISI_GESA_PIC_SIM_H

#include <serreflection/defines.hpp>

#include "../base/imodel.h"
#include "../base/isolver.h"

namespace daisi
{
namespace accel
{

struct GESAPICSimParams
{
    double number_of_mesh_steps;
    double CFL_value;
    double Poisson_tolerance;
    double max_time;
};

class GESAPICSim final : public ISolverCRTPParams<GESAPICSim, GESAPICSimParams>
{
  public:
    BUILD_CHILD(GESAPICSim, daisi::IModelComponent)

    SolverResult run_concrete_solver(const int                      results_id,
                                     const std::unique_ptr<IModel>& model) const
        noexcept override final;
};
} // namespace accel
} // namespace daisi

SERIALIZIBLE_STRUCT(daisi::accel::GESAPICSimParams, srfl::CheckModes::FATAL,
                    (double, number_of_mesh_steps, srfl::nan, 1.0,
                     srfl::inf)(double, CFL_value, srfl::nan, 1.0,
                                srfl::inf)(double, Poisson_tolerance, srfl::nan, -1e-200,
                                           srfl::inf)(double, max_time, srfl::nan, -1e-200,
                                                      srfl::inf))

#endif
