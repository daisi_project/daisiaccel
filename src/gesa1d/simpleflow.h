#ifndef DAISIACCEL_SIMPLE_FLOW
#define DAISIACCEL_SIMPLE_FLOW

#include <list>

#include <common_tools/constants.h>
#include <common_tools/propagate_const.hpp>
#include <common_tools/service_locator.hpp>

#include <serreflection/defines.hpp>

#include "../base/imodelcomponent.h"
#include "common.h"

namespace daisi
{
namespace accel
{
struct SimpleFlowParams
{
    double mass_number;
    double charge_number;
    double temperature__eV;
    double emission_period;
};

template <typename T>
using List = std::list<T>;

template <typename T>
using Vector = std::vector<T>;

template <class T, template <typename> class Cont>
class Gesa1dFlow
{
  public:
    void update_momentums(const std::vector<T>& W, const double dt);
    void update_positions(const double dt);

    void calculate_W(const double anode_radius, const double first_step,
                     const double regular_step) noexcept;

    const Wdata<T, Cont>& get_W() const noexcept;

    void   set_start_position(const double start_position) noexcept;
    void   set_left_border(const double left_border) noexcept;
    double get_beta_by_T(const double Temp) const noexcept;
    void   add_particle(const double dt) noexcept;
    void   set_current_density(const double new_j) noexcept;
    void   remove_particles() noexcept;

    Gesa1dFlow(const double mass, const double charge, const double momentum,
               const double emission_period, const double start_position, const double left_border,
               const double right_border) noexcept;

    const Cont<T>& get_charges() const noexcept;
    double         get_mass() const noexcept;
    double         get_charge() const noexcept;
    double         get_anode_current(const double dt) const noexcept;

  private:
    const double m_mass;
    const double m_charge;
    const double m_start_momentum;
    const int    m_emission_period;
    double       m_start_position;
    double       m_left_border;
    const double m_right_border;

    double m_current_density;

    const double m_alpha;

    Cont<T> m_charges;
    Cont<T> m_positions;
    Cont<T> m_momentums;

    Wdata<T, Cont> m_W;
};

class SimpleFlow final : public daisi::IModelComponentCRTPParams<SimpleFlow, SimpleFlowParams>
{
  public:
    std::shared_ptr<IModelComponentView> to_model_view(const std::string& name,
                                                       const std::string& type) const override;

    template <class T, template <typename> class Cont>
    std::shared_ptr<Gesa1dFlow<T, Cont>>
    get_gesa_flow(const double start_position, const double cathode_radius,
                  const double anode_radius, const double velocity_sign) const noexcept
    {
        auto params = *this->m_parameters;

        const auto mass   = params.mass_number * commtools::ELECTRON_MASS();
        const auto charge = params.charge_number * commtools::ELECTRON_CHARGE();

        const auto beta = t2beta(params.temperature__eV, mass, charge);

        const auto gamma = 1.0 / std::sqrt(1 - beta * beta);

        return std::make_shared<Gesa1dFlow<T, Cont>>(mass, charge, gamma * beta * velocity_sign,
                                                     params.emission_period, start_position,
                                                     anode_radius, cathode_radius);
    }

    BUILD_CHILD(SimpleFlow, daisi::IModelComponent)
};
} // namespace accel
} // namespace daisi

SERIALIZIBLE_STRUCT(daisi::accel::SimpleFlowParams, srfl::CheckModes::FATAL,
                    (double, mass_number, DEF_D())(double, charge_number, DEF_D())(
                        double, temperature__eV, DEF_D())(double, emission_period, DEF_D()))

#endif
