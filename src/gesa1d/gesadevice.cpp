#include <common_tools/constants.h>

#include "gesadevice.h"

namespace daisi
{
namespace accel
{

std::shared_ptr<IModelComponentView> GesaDevice::to_model_view(const std::string& name,
                                                               const std::string& type) const
{
}

void GesaDevice::GesaMesh::change_anode_radius(const double anode_radius) noexcept
{
    m_anode_radius = anode_radius;

    const size_t n_steps_anode_grid = std::floor((m_grid_radius - anode_radius) / m_regular_step);

    m_grid_point = n_steps_anode_grid + 1;

    m_n_points = m_grid_point + 1 + m_n_steps_grid_cathode;

    m_first_step = (m_grid_radius - m_anode_radius) - n_steps_anode_grid * m_regular_step;

    if (m_V.empty())
    {
        m_V.resize(m_n_points);

        m_V[0]              = m_v_an;
        m_V[m_grid_point]   = m_v_grid;
        m_V[m_n_points - 1] = 0;
    }

    const auto old_size = m_V.size();

    for (size_t i = 0; i < old_size - m_n_points; i++)
    {
        m_V.erase(m_V.begin() + 1);
    }

    m_E.resize(m_V.size());
    m_rho.resize(m_V.size());
    zeroing_field();
}

void GesaDevice::GesaMesh::zeroing_field() noexcept
{
    std::fill(m_E.begin(), m_E.end(), 0);
    std::fill(m_rho.begin(), m_rho.end(), 0);
}

GesaDevice::GesaMesh::GesaMesh(const double anode_radius, const double grid_radius,
                               const double cathode_radius, const size_t n_steps, const double v_an,
                               const double v_grid) noexcept
    : m_grid_radius(grid_radius),
      m_n_steps_grid_cathode(double(n_steps) * (cathode_radius - grid_radius) /
                             (cathode_radius - anode_radius)),
      m_regular_step((cathode_radius - grid_radius) / m_n_steps_grid_cathode),
      m_v_an(v_an),
      m_v_grid(v_grid)
{
    change_anode_radius(anode_radius);
}
double GesaDevice::GesaMesh::get_anode_radius() const noexcept
{
    return m_anode_radius;
}

double GesaDevice::GesaMesh::get_regular_step() const noexcept
{
    return m_regular_step;
}

double GesaDevice::GesaMesh::get_first_step() const noexcept
{
    return m_first_step;
}

const std::vector<double>& GesaDevice::GesaMesh::get_E() const noexcept
{
    return m_E;
}

double langmuir_law(const double rc, const double ra, const double dV, const double charge,
                    const double mass)
{
    const double gamma = std::log(rc / ra);

    double beta = gamma - 0.4 * std::pow(gamma, 2.0) + 0.091667 * std::pow(gamma, 3.0) -
                  0.014242 * std::pow(gamma, 4.0) + 0.001679 * std::pow(gamma, 5.0);

    return 4.0 * M_PI * commtools::VACUUM_PERMITTIVITY() * (2.0 / 9.0) *
           std::sqrt(2.0 * std::abs(charge) / mass) * (std::sqrt(dV) * dV) /
           (rc * beta * beta * 2.0 * M_PI * ra);
}

double GesaDevice::GesaMesh::get_current(const double charge, const double mass,
                                         const bool is_el) const noexcept
{
    double rc = get_r(m_n_points - 1);
    double ra = get_r(m_n_points - 2);

    double dV = std::abs(m_V[m_n_points - 2] - m_V[m_n_points - 1]);

    if (!is_el)
    {
        rc = get_r(0);
        ra = get_r(1);
        dV = std::abs(m_V[0] - m_V[1]);
    }

    return langmuir_law(rc, ra, dV, charge, mass);
}

void GesaDevice::GesaMesh::charge_to_density() noexcept
{
    for (size_t i = 1; i < m_rho.size(); i++)
    {
        double       r1 = get_r(i);
        const double r2 = get_r(i) + m_regular_step / 2.0;

        if (i == 1)
        {
            r1 -= m_first_step / 2.0;
        }
        else
        {
            r1 -= m_regular_step / 2.0;
        }

        m_rho[i] /= -M_PI * (r2 * r2 - r1 * r1) * commtools::VACUUM_PERMITTIVITY();
    }
}

double GesaDevice::GesaMesh::get_r(const size_t i) const noexcept
{
    if (0 == i)
    {
        return m_anode_radius;
    }
    if (1 == i)
    {
        return m_first_step + m_anode_radius;
    }
    return m_anode_radius + m_first_step + (i - 1) * m_regular_step;
}

void GesaDevice::GesaMesh::recalculate_v(const double tolerance)
{
    const auto hr = m_regular_step;

    const auto H     = m_first_step + hr;
    const auto sigma = hr / m_first_step;

    const double omega = 1.9;

    const auto k1 = 1.0 / (H * get_r(1));

    while (1)
    {
        double delta_sum = 0;
        auto   delta     = -m_V[1] * omega +
                     omega * (m_rho[1] - (2.0 / H) * (m_V[0] / m_first_step + m_V[2] / hr) -
                              k1 * (-sigma * m_V[0] + m_V[2] / sigma)) /
                         (k1 * ((sigma * sigma - 1.0) / sigma) -
                          (2.0 / H) * (1.0 / m_first_step + 1.0 / hr));

        delta_sum += std::abs(delta);
        m_V[1] += delta;

        for (size_t i = 2; i < m_n_points - 1; i++)
        {
            if (m_grid_point == i)
            {
                continue;
            }
            const auto k2 = 1.0 / (2.0 * hr * (get_r(i)));

            auto delta = -m_V[i] * omega +
                         omega * (m_rho[i] - (1.0 / (hr * hr)) * (m_V[i - 1] + m_V[i + 1]) -
                                  k2 * (m_V[i + 1] - m_V[i - 1])) /
                             (-2.0 / (hr * hr));

            if (i == m_n_points - 2)
            {
                int tt = 0;
            }

            delta_sum += std::abs(delta);
            m_V[i] += delta;
        }
        if (delta_sum < tolerance)
        {
            break;
        }
    }

    m_E[0] = -((1.0 / H) * (-(2.0 + sigma) * m_V[0] +
                            (1.0 + sigma) * (1.0 + sigma) * m_V[1] / sigma - m_V[2] / sigma));

    m_E[1] =
        -(1.0 / H) * (-sigma * m_V[0] + (sigma * sigma - 1.0) * m_V[1] / sigma + m_V[2] / sigma);

    for (size_t i = 2; i < m_n_points - 1; i++)
    {
        m_E[i] = -(m_V[i + 1] - m_V[i - 1]) / (2.0 * hr);
    }

    m_E[m_n_points - 1] = -(
        (m_V[m_n_points - 3] - 4.0 * m_V[m_n_points - 2] + 3.0 * m_V[m_n_points - 1]) / (2.0 * hr));
}

double GesaDevice::get_anode_radius() const noexcept
{
    return this->m_parameters->anode_radius__m;
}
double GesaDevice::get_cathode_radius() const noexcept
{
    return this->m_parameters->cathode_radius__m;
}

double GesaDevice::get_v_anode() const noexcept
{
    return this->m_parameters->anode_voltage__V;
}

GesaDevice::GesaMesh GesaDevice::get_gesa_mesh(const size_t n_steps) noexcept
{

    auto params = *this->m_parameters;
    return GesaDevice::GesaMesh(params.anode_radius__m, params.grid_radius__m,
                                params.cathode_radius__m, n_steps, params.anode_voltage__V,
                                params.grid_voltage__V);
}

REGISTER_CHILD_NON_TEMPLATE(GesaDevice, daisi::IModelComponent, "Gesa_device")

} // namespace accel
} // namespace daisi
