#ifndef DAISIACCEL_GESA_DEVICE
#define DAISIACCEL_GESA_DEVICE

#include <list>

#include <common_tools/propagate_const.hpp>
#include <common_tools/service_locator.hpp>

#include <serreflection/defines.hpp>

#include "../base/imodelcomponent.h"
#include "common.h"

namespace daisi
{
namespace accel
{
struct GesaDeviceParameters
{
    double cathode_radius__m;
    double anode_radius__m;
    double grid_radius__m;
    double anode_voltage__V;
    double grid_voltage__V;
    double grid_transparency;
    double sigma_alpha__cmcm;
};

class GesaDevice final : public daisi::IModelComponentCRTPParams<GesaDevice, GesaDeviceParameters>
{
  public:
    class GesaMesh
    {
      public:
        double get_current(const double charge, const double mass, const bool is_el) const noexcept;
        void   recalculate_v(const double tol);

        GesaMesh(const double anode_radius, const double grid_radius, const double cathode_radius,
                 const size_t n_steps, const double v_an, const double v_grid) noexcept;

        void   change_anode_radius(const double anode_radius) noexcept;
        void   zeroing_field() noexcept;
        double get_regular_step() const noexcept;
        double get_first_step() const noexcept;
        double get_anode_radius() const noexcept;
        void   charge_to_density() noexcept;
        double get_r(const size_t i) const noexcept;

        template <class T, template <typename> class Cont>
        std::vector<T> get_fields(const Wdata<T, Cont>& W) const noexcept
        {
            std::vector<T> result(W.cell.size());

            auto it_res  = result.begin();
            auto it_cell = W.cell.begin();
            auto it_W1   = W.W1.begin();

            for (; it_res != result.end(); it_res++, it_cell++, it_W1++)
            {
                *it_res = m_E[*it_cell] * (*it_W1) + m_E[*it_cell + 1] * (1.0 - *it_W1);
            }

            return result;
        }

        const std::vector<double>& get_E() const noexcept;

        template <class T, template <typename> class Cont>
        void add_charges(const Wdata<T, Cont>& W, const Cont<T>& data) noexcept
        {
            assert(W.cell.size() == data.size());
            assert(W.W1.size() == data.size());

            auto it_ch   = data.begin();
            auto it_W1   = W.W1.begin();
            auto it_cell = W.cell.begin();

            for (; it_ch != data.end(); it_ch++, it_W1++, it_cell++)
            {
                m_rho[*it_cell] += *it_ch * (*it_W1);
                m_rho[*it_cell + 1] += *it_ch * (1.0 - *it_W1);
            }
        }

      private:
        double       m_anode_radius;
        const double m_grid_radius;

        double       m_first_step;
        size_t       m_n_points;
        size_t       m_grid_point;
        const size_t m_n_steps_grid_cathode;
        const double m_v_an;

        const double m_regular_step;

        const double m_v_grid;

        std::vector<double> m_V;
        std::vector<double> m_E;
        std::vector<double> m_rho;
    };

    double get_anode_radius() const noexcept;
    double get_cathode_radius() const noexcept;
    double get_v_anode() const noexcept;

    GesaMesh get_gesa_mesh(const size_t n_steps) noexcept;

    std::shared_ptr<IModelComponentView> to_model_view(const std::string& name,
                                                       const std::string& type) const override;

    BUILD_CHILD(GesaDevice, daisi::IModelComponent)
};
} // namespace accel
} // namespace daisi

SERIALIZIBLE_STRUCT(daisi::accel::GesaDeviceParameters, srfl::CheckModes::FATAL,
                    (double, cathode_radius__m, srfl::nan, -1e-200,
                     srfl::inf)(double, anode_radius__m, srfl::nan, -1e-200,
                                srfl::inf)(double, grid_radius__m, srfl::nan, -1e-200, srfl::inf)(
                        double, anode_voltage__V, srfl::nan, -1e-200,
                        srfl::inf)(double, grid_voltage__V, srfl::nan, -1e-200,
                                   srfl::inf)(double, grid_transparency, srfl::nan, -1e-200,
                                              srfl::inf)(double, sigma_alpha__cmcm, srfl::nan,
                                                         -1e-200, srfl::inf))

#endif
