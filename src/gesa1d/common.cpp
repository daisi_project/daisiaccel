#include <cmath>
#include <common_tools/constants.h>
#include <iomanip>

#include "common.h"

double t2beta(const double T, const double mass, const double charge)
{
    const auto v = std::sqrt(T * std::abs(charge) / mass);

    return v / commtools::LIGHT_VELOCITY();
}
