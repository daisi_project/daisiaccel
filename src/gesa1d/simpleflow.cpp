#include <common_tools/constants.h>

#include "simpleflow.h"

namespace daisi
{
namespace accel
{

template <typename T>
int sign(const T& val)
{
    return (T(0) < val) - (val < T(0));
}

template class Gesa1dFlow<double, Vector>;
template class Gesa1dFlow<double, List>;
template class Gesa1dFlow<float, Vector>;
template class Gesa1dFlow<float, List>;

template <class T, template <typename> class Cont>

Gesa1dFlow<T, Cont>::Gesa1dFlow(const double mass, const double charge, const double momentum,
                                const double emission_period, const double start_position,
                                const double left_border, const double right_border) noexcept
    : m_mass(mass), m_charge(charge), m_start_momentum(momentum),
      m_emission_period(emission_period), m_start_position(start_position),
      m_left_border(left_border), m_right_border(right_border), m_current_density(0),
      m_alpha(charge / (mass * commtools::LIGHT_VELOCITY() * commtools::LIGHT_VELOCITY()))
{
}

template <class T, template <typename> class Cont>

void Gesa1dFlow<T, Cont>::update_momentums(const std::vector<T>& E, const double dt)
{
    size_t i = 0;

    for (auto& momentum : m_momentums)
    {
        momentum += dt * m_alpha * E[i];
        i++;
    }
}

template <class T, template <typename> class Cont>

void Gesa1dFlow<T, Cont>::update_positions(const double dt)
{
    auto it_mom = m_momentums.begin();
    auto it_pos = m_positions.begin();

    for (; it_mom != m_momentums.end(); it_mom++, it_pos++)
    {
        const auto mom = *it_mom;

        const auto gamma = std::sqrt(1 + mom * mom);

        *it_pos += dt * mom / gamma;
    }
}

template <class T, template <typename> class Cont>

double Gesa1dFlow<T, Cont>::get_mass() const noexcept
{
    return m_mass;
}

template <class T, template <typename> class Cont>
double Gesa1dFlow<T, Cont>::get_charge() const noexcept
{
    return m_charge;
}

template <class T, template <typename> class Cont>
void Gesa1dFlow<T, Cont>::remove_particles() noexcept
{
    auto it_mom  = m_momentums.begin();
    auto it_pos  = m_positions.begin();
    auto it_ch   = m_charges.begin();
    auto it_cell = m_W.cell.begin();
    auto it_W1   = m_W.W1.begin();

    for (; it_mom != m_momentums.end(); it_mom++, it_pos++, it_ch++, it_cell++, it_W1++)
    {
        if (*it_pos < m_left_border || *it_pos > m_right_border)
        {
            *it_pos  = -1.0;
            *it_mom  = -1.0;
            *it_ch   = -1.0;
            *it_cell = -1;
            *it_W1   = -1.0;
        }
    }
    auto remove = [&](Cont<T>& val) {
        val.erase(std::remove(val.begin(), val.end(), -1.0), val.end());
    };
    auto remove_i = [&](Cont<int>& val) {
        val.erase(std::remove(val.begin(), val.end(), -1), val.end());
    };

    remove(m_momentums);
    remove(m_positions);
    remove(m_charges);
    remove_i(m_W.cell);
    remove(m_W.W1);
}

template <class T, template <typename> class Cont>
double Gesa1dFlow<T, Cont>::get_anode_current(const double dt) const noexcept
{
    double result = 0;

    auto it_pos = m_positions.begin();
    auto it_ch  = m_charges.begin();

    for (; it_pos != m_positions.end(); it_pos++, it_ch++)
    {
        if (*it_pos < m_left_border)
        {
            result += *it_ch;
        }
    }
    return result / dt;
}

template <class T, template <typename> class Cont>
void Gesa1dFlow<T, Cont>::calculate_W(const double anode_radius, const double first_step,
                                      const double regular_step) noexcept
{
    auto it_pos  = m_positions.begin();
    auto it_cell = m_W.cell.begin();
    auto it_W1   = m_W.W1.begin();

    for (; it_pos != m_positions.end(); it_pos++, it_cell++, it_W1++)
    {
        double r1;
        double r2;

        const auto dr = *it_pos - (anode_radius + first_step);
        if (dr < 0)
        {
            r1 = anode_radius;
            r2 = r1 + first_step;

            *it_cell = 0;
        }
        else
        {
            *it_cell = 1 + std::floor(dr / regular_step);

            r1 = (anode_radius + first_step) + (*it_cell - 1) * regular_step;
            r2 = r1 + regular_step;
        }
        *it_W1 = (r2 - *it_pos) / (r2 - r1);
    }
}

template <class T, template <typename> class Cont>
const Wdata<T, Cont>& Gesa1dFlow<T, Cont>::get_W() const noexcept
{
    return m_W;
}

template <class T, template <typename> class Cont>
void Gesa1dFlow<T, Cont>::add_particle(const double dt) noexcept
{
    m_momentums.push_back(m_start_momentum);
    m_positions.push_back(m_start_position);
    m_charges.push_back(2 * M_PI * m_start_position * dt * m_current_density * sign(m_charge) /
                        commtools::LIGHT_VELOCITY());

    m_W.cell.push_back(0);
    m_W.W1.push_back(0);
}

template <class T, template <typename> class Cont>
const Cont<T>& Gesa1dFlow<T, Cont>::get_charges() const noexcept
{
    return m_charges;
}

template <class T, template <typename> class Cont>
void Gesa1dFlow<T, Cont>::set_current_density(const double new_j) noexcept
{
    m_current_density = new_j;
}

template <class T, template <typename> class Cont>
double Gesa1dFlow<T, Cont>::get_beta_by_T(const double Temp) const noexcept
{
    return t2beta(Temp, m_mass, m_charge);
}

std::shared_ptr<IModelComponentView> SimpleFlow::to_model_view(const std::string& name,
                                                               const std::string& type) const
{
}

REGISTER_CHILD_NON_TEMPLATE(SimpleFlow, daisi::IModelComponent, "Simple_flow")

} // namespace accel
} // namespace daisi
