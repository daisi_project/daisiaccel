#ifndef DAISIACCEL_GESA_COMMON
#define DAISIACCEL_GESA_COMMON

#include <vector>

double t2beta(const double T, const double mass, const double charge);

template <class T, template <typename> class Cont>
struct Wdata
{
    Cont<int> cell;
    Cont<T>   W1;

    void resize(const size_t size) noexcept
    {
        cell.resize(size);
        W1.resize(size);
    }
};

#endif
