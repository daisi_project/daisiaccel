#ifndef DAISI_CONTROLLER_H
#define DAISI_CONTROLLER_H

#include <atomic>
#include <memory>

#include <common_tools/propagate_const.hpp>

namespace daisi
{
class IModelComponent;
class IDataCommunicator;
class IModel;
class ISolver;

class DaisiController
{
  public:
    ~DaisiController();

    DaisiController(const int n_tasks) noexcept;

    bool check_model_component(const int model_id, const std::string& component_pseudoname,
                               const std::string& view_pseudoname,
                               const std::string& name) noexcept;

    std::shared_ptr<IModelComponent> make_model_component(const int          model_id,
                                                          const std::string& component_pseudoname,
                                                          const std::string& view_pseudoname,
                                                          const std::string& name) noexcept;

    std::shared_ptr<ISolver> make_solver(const int model_id, const std::string& solver_pseudoname,
                                         const std::string& name) noexcept;

    std::unique_ptr<IModel> make_model(const int model_id) noexcept;

    bool run_calculation(const int results_id, const int model_id,
                         const std::string& solver_pseudoname, const std::string& name) noexcept;

    bool run() noexcept;

    bool add_task(const int model_id, const std::string& solver_pseudoname,
                  const std::string& name) noexcept;

  private:
    bool run_calculation_inner(const int results_id, const int model_id,
                               const std::string& solver_pseudoname,
                               const std::string& name) noexcept;

    std::atomic<int> m_n_tasks;
};
} // namespace daisi

#endif
