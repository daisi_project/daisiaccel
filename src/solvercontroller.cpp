

#include <chrono>
#include <thread>

#include <boost/thread.hpp>

#include <daisiaccel/interface.hpp>

#include <common_tools/boostlog.hpp>
#include <common_tools/helpers.hpp>
#include <common_tools/threadpool.hpp>
#include <serreflection/read_json.hpp>

#include "config.h"
#include "controller.h"
#include "database/pqxxcommunicator.h"

using namespace std::chrono_literals;

namespace daisi
{
DaisiSolverController::~DaisiSolverController() = default;

void DaisiSolverController::init(const std::string& config, const std::string& sink,
                                 const bool is_solver)
{
    auto config_struct = srfl::read_json_string<daisi::DAISIConfig>(config);

    if (!config_struct)
    {
        throw std::runtime_error("Invalid config");
    }
    const std::vector<std::pair<std::string, std::string>> dummy = {};

    if (is_solver)
    {
        commtools::ThreadPool::get(config_struct->thread_pool_size, {sink});
        commtools::BoostLog::get(config_struct->log, dummy, true);
    }
    else
    {
        commtools::ThreadPool::get(config_struct->thread_pool_size);
        commtools::BoostLog::get(config_struct->log, dummy, true, sink);
    }

    IDataCommunicator::get<PQXXCommunicator>(config_struct->db);
}

DaisiSolverController::DaisiSolverController(const std::string& config_file,
                                             const std::string& sink, const bool is_solver)
{
    const auto config_string = commtools::file_to_string(config_file);
    init(config_string, sink, is_solver);
}

bool DaisiSolverController::run() noexcept
{
    return m_controller->run();
}

bool DaisiSolverController::add_task(const int model_id, const std::string& solver_pseudoname,
                                     const std::string& name) noexcept
{
    return m_controller->add_task(model_id, solver_pseudoname, name);
}

bool DaisiSolverController::check_model_component(const int          model_id,
                                                  const std::string& component_pseudoname,
                                                  const std::string& view_pseudoname,
                                                  const std::string& name) noexcept
{
    return m_controller->check_model_component(model_id, component_pseudoname, view_pseudoname,
                                               name);
}
} // namespace daisi
