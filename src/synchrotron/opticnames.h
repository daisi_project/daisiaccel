#ifndef DAISIACCEL_OPTIC_NAMES_H
#define DAISIACCEL_OPTIC_NAMES_H

#include <string>

namespace daisi
{
namespace accel
{
namespace opticnames
{
extern const std::string Drift;
extern const std::string Kicker;
extern const std::string Monitor;
extern const std::string Quadrupole;
extern const std::string Rbend;
extern const std::string Sbend;
}
}
}

#endif
