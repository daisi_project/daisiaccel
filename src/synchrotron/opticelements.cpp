
#include <iomanip>

#include "opticelements.h"
#include "opticnames.h"

namespace daisi
{
namespace accel
{

namespace opticnames
{
const std::string Drift      = "DRIFT";
const std::string Kicker     = "KICKER";
const std::string Monitor    = "MONITOR";
const std::string Quadrupole = "QUADRUPOLE";
const std::string Rbend      = "RBEND";
const std::string Sbend      = "SBEND";
const std::string Sextupole  = "SEXTUPOLE";
} // namespace opticnames

REGISTER_CHILD_NON_TEMPLATE(Drift, IOpticElement, opticnames::Drift)
REGISTER_CHILD_NON_TEMPLATE(Kicker, IOpticElement, opticnames::Kicker)
REGISTER_CHILD_NON_TEMPLATE(Monitor, IOpticElement, opticnames::Monitor)
REGISTER_CHILD_NON_TEMPLATE(Quadrupole, IOpticElement, opticnames::Quadrupole)
REGISTER_CHILD_NON_TEMPLATE(Rbend, IOpticElement, opticnames::Rbend)
REGISTER_CHILD_NON_TEMPLATE(Sbend, IOpticElement, opticnames::Sbend)
REGISTER_CHILD_NON_TEMPLATE(Sextupole, IOpticElement, opticnames::Sextupole)

IOpticElement::~IOpticElement() = default;

std::list<std::string> Drift::get_madx_required() const noexcept
{
    return {"L"};
}
std::list<std::string> Sextupole::get_madx_required() const noexcept
{
    return {"L", "K2"};
}
std::list<std::string> Kicker::get_madx_required() const noexcept
{
    return {"HKICK", "VKICK"};
}
std::list<std::string> Quadrupole::get_madx_required() const noexcept
{
    return {"K1", "L"};
}

std::pair<arma::mat, arma::vec> Rbend::calc_transport_matrix() const noexcept
{
}

std::pair<arma::mat, arma::vec> Sbend::calc_transport_matrix() const noexcept
{
    std::pair<arma::mat, arma::vec> result;
    result.second.zeros(5);

    auto& M = result.first;
    M.zeros(5, 5);

    for (size_t d = 0; d < 5; d++)
    {
        M(d, d) = 1;
    }

    double pp = L / ANGLE;

    M(0, 0) = std::cos(ANGLE);
    M(0, 1) = pp * std::sin(ANGLE);
    M(0, 4) = pp * (1 - std::cos(ANGLE));
    M(1, 0) = -std::sin(ANGLE) / pp;
    M(1, 1) = std::cos(ANGLE);
    M(1, 4) = std::sin(ANGLE);
    M(2, 3) = pp * ANGLE;

    result.second(0) = sigma_b_err * pp * (1 - std::cos(ANGLE));
    result.second(1) = sigma_b_err * std::sin(ANGLE);

    return result;
}
std::pair<arma::mat, arma::vec> Kicker::calc_transport_matrix() const noexcept
{
    std::pair<arma::mat, arma::vec> result;

    auto& Tm = result.first;
    Tm.zeros(5, 5);

    result.second.zeros(5);

    auto& B = result.second;

    for (size_t d = 0; d < 5; d++)
        Tm(d, d) = 1;

    Tm(0, 1) = L;
    Tm(2, 3) = L;

    B(0) = 0.5 * L * HKICK;
    B(1) = HKICK;

    B(2) = 0.5 * L * VKICK;
    B(3) = VKICK;

    return result;
}

std::pair<arma::mat, arma::vec> Quadrupole::calc_transport_matrix() const noexcept
{
    std::pair<arma::mat, arma::vec> result;

    double Kx = std::abs(K1);
    double Ky = std::abs(K1);

    // FIXME hard code ANGLE
    double ANGLE = 0.0659;

    double pp = L / ANGLE;

    double ksix = L * std::sqrt(Kx);
    double ksiy = L * std::sqrt(Ky);

    auto& T = result.first;

    result.second.zeros(5);

    T.zeros(5, 5);
    if (K1 > 0)
    {
        T(0, 0) = std::cos(ksix);
        T(0, 1) = 1.0 / std::sqrt(Kx) * std::sin(ksix);
        T(1, 0) = -std::sqrt(Kx) * std::sin(ksix);
        T(1, 1) = std::cos(ksix);

        T(2, 2) = std::cosh(ksiy);
        T(2, 3) = 1.0 / std::sqrt(Ky) * std::sinh(ksiy);
        T(3, 2) = std::sqrt(Ky) * std::sinh(ksiy);
        T(3, 3) = std::cosh(ksiy);

        T(4, 4) = 1;
    }
    else
    {
        T(0, 0) = std::cosh(ksix);
        T(0, 1) = 1.0 / std::sqrt(Kx) * std::sinh(ksix);
        T(1, 0) = std::sqrt(Kx) * std::sinh(ksix);
        T(1, 1) = std::cosh(ksix);

        T(2, 2) = std::cos(ksiy);
        T(2, 3) = 1.0 / std::sqrt(Ky) * std::sin(ksiy);
        T(3, 2) = -std::sqrt(Ky) * std::sin(ksiy);
        T(3, 3) = std::cos(ksiy);
        T(4, 4) = 1;
    }

    return result;
}
} // namespace accel
} // namespace daisi
