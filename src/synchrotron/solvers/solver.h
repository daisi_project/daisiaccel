#ifndef DAISI_SYNC_MAIN_SOLVER_H
#define DAISI_SYNC_MAIN_SOLVER_H

#include <common_tools/propagate_const.hpp>

#include "../../base/imodel.h"
#include "../../base/isolver.h"
#include "../../common/flow.h"
#include "../opticsequence.h"

namespace daisi
{

namespace accel
{
#define make_common()                                                                              \
    SolverResult run_concrete_solver(const int results_id, const std::unique_ptr<IModel>& model)   \
        const noexcept override final                                                              \
    {                                                                                              \
        auto seq  = model->get_component<Sequence>("Optic elements sequence");                     \
        auto flow = model->get_component<Flow>("Flow");                                            \
        return run_concrete_solver(results_id, flow, seq);                                         \
    }                                                                                              \
    virtual SolverResult run_concrete_solver(                                                      \
        const int results_id, const std::shared_ptr<Flow>& flow,                                   \
        const std::shared_ptr<Sequence>& sequence) const noexcept = 0;

template <class T, class Tparams>
class SolverSynchrotronParams : public ISolverCRTPParams<T, Tparams>
{
  public:
  make_common() 
};
template <class T>
class SolverSynchrotron : public ISolverCRTP<T>
{
   public:
  make_common() 
};

} // namespace accel
} // namespace daisi

#endif
