#include <map>
#include <sstream>
#include <vector>

#include <common_tools/algorithm.hpp>

#include "../../common/common_tools.h"

#include <common_tools/boostlog.hpp>

#include "../opticsequence.h"
#include "tools.h"

namespace daisi
{
namespace accel
{
template std::array<std::vector<float>, 5> calc_trace<float>(const arma::vec&                  x0,
                                                             const std::list<TransportMatrix>& Ms);
template std::array<std::vector<double>, 5>
calc_trace<double>(const arma::vec& x0, const std::list<TransportMatrix>& Ms);

template <class T>
std::array<std::vector<T>, 5> calc_trace(const arma::vec& x0, const std::list<TransportMatrix>& Ms)
{
    std::array<std::vector<T>, 5> result_;

    for (const auto& MB : Ms)
    {
        const arma::vec X_curr = MB.M * x0 + MB.B;
        for (size_t i = 0; i < 4; i++)
        {
            result_[i].push_back(X_curr(i));
        }
        result_[4].push_back(MB.S);
    }
    return result_;
}

std::vector<std::pair<std::string, std::array<double, 7>>> loadtw(const std::string& content)
{
    std::vector<std::pair<std::string, std::array<double, 7>>> result;

    std::stringstream strream;

    strream << content;

    int  MAX_LENGTH = 10000;
    char line[MAX_LENGTH];

    bool                read = false;
    std::vector<size_t> inds = {2, 3, 4, 5, 6, 7, 8};

    while (strream.getline(line, MAX_LENGTH))
    {
        auto splitted =
            commtools::strsplit_adapter<std::vector<std::string>>(line, std::string(": ,;=,"));

        if (read)
        {
            std::array<double, 7> curr;
            for (size_t i = 0; i < inds.size(); i++)
            {
                curr[i] = std::stod(splitted[inds[i]]);
            }
            result.push_back(std::make_pair(splitted[0], curr));
        }

        if ("%s" == splitted[1])
        {
            read = true;
        }
    }

    return result;
}

std::map<int, std::array<std::vector<float>, 6>> loadcm(const std::string& content)
{
    auto curr_content = content;
    auto element_data = extract_cmd_data("", curr_content, "", "#segment", false, false);

    std::stringstream strream;

    strream << curr_content.substr(element_data.second, curr_content.size() - element_data.second);

    int  MAX_LENGTH = 1000;
    char line[MAX_LENGTH];

    std::map<int, std::array<std::vector<float>, 6>> result;

    std::vector<size_t> inds = {2, 3, 4, 5, 8};

    double   addition     = 0;
    double   last_s       = 0;
    unsigned current_turn = 1;

    while (strream.getline(line, MAX_LENGTH))
    {
        auto splitted =
            commtools::strsplit_adapter<std::vector<std::string>>(line, std::string(": ,;=,"));
        if (line[0] != '#')
        {
            auto& ss = result[std::stoi(splitted[0])][inds.size() - 1];

            if (ss.size() > 0)
            {
                last_s = ss.back();
            }

            for (size_t i = 0; i < inds.size(); i++)
            {
                result[std::stoi(splitted[0])][i].push_back(std::stod(splitted[inds[i]]));
            }
            // last_s = ss.back();

            if (current_turn < std::stoi(splitted[1]))
            {
                addition     = last_s;
                current_turn = std::stoi(splitted[1]);
            }

            ss.back() += addition;
        }
        else
        {
            result[1][5].push_back(std::stod(splitted[4]));
        }
    }

    return result;
}
} // namespace accel
} // namespace daisi
