#include <fstream>

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <common_tools/helpers.hpp>

#include "../../base/idatacommunicator.h"
#include "../../common/plot_tools.h"

#include "fourier.h"
#include "fourier_common.h"
#include "madxcaller.h"
#include "tools.h"

namespace daisi
{
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(Solver_Fourier, daisi::IModelComponent, "Fourier_analysis")

bool Solver_Fourier::from_model_view(const std::string                         name,
                                     const std::map<std::string, std::string>& content)
{
    std::stringstream ss;
    ss << content.at("");

    boost::property_tree::ptree pt;

    boost::property_tree::read_json(ss, pt);

    m_n_members = pt.get<int>("Number_of_series_members");

    return true;
}

SolverResult Solver_Fourier::run_concrete_solver(const int                        results_id,
                                                 const std::shared_ptr<Flow>&     flow,
                                                 const std::shared_ptr<Sequence>& sequence) const
    noexcept
{
    SolverResult result;
    result.status = false;

    auto errors = sequence->get_errors("SBEND");

    return fourier_errors(errors, m_n_members, "dipoles");
}
} // namespace accel
} // namespace daisi
