#ifndef DAISI_SYNC_BETATRON_RESONANCES_H
#define DAISI_SYNC_BETATRON_RESONANCES_H

#include <utility>

#include "solver.h"

#include "betatron_resonances_config.h"
#include "madxcaller.h"
#include "tools.h"

namespace daisi
{
namespace accel
{

template <class T>
class Solver_BetatronResonansesBase : public SolverSynchrotron<T>
{
  public:
    struct SpectrumResult
    {
        std::pair<std::vector<float>, std::vector<float>> x;
        std::pair<std::vector<float>, std::vector<float>> y;

        std::pair<std::vector<float>, std::vector<float>> x_resonances;
        std::pair<std::vector<float>, std::vector<float>> y_resonances;
    };

    virtual bool from_model_view_other(const std::string                         name,
                                       const std::map<std::string, std::string>& content) = 0;

    SpectrumResult calculate_spectrums(const int results_id, const std::shared_ptr<Flow>& flow,
                                       const std::shared_ptr<Sequence>& sequence) const noexcept
    {

        SpectrumResult result;

        auto flow_current = std::make_shared<Flow>(*flow);

        unsigned n_x = unsigned(this->m_parameters->n_particles_x);
        unsigned n_y = unsigned(this->m_parameters->n_particles_y);

        double hx = 2.0 * this->m_parameters->x_amplitude_max / double(n_x - 1);
        double hy = 2.0 * this->m_parameters->y_amplitude_max / double(n_y - 1);

        std::vector<double> initials_x(n_x);
        std::vector<double> initials_y(n_y);
        std::vector<double> zeros_y(n_x);
        std::vector<double> zeros_x(n_y);

        for (size_t i = 0; i < n_y; i++)
        {
            initials_y[i] = -this->m_parameters->y_amplitude_max + double(i) * hy;
        }
        for (size_t j = 0; j < n_x; j++)
        {
            initials_x[j] = -this->m_parameters->x_amplitude_max + double(j) * hx;
        }

        auto process =
            [results_id, flow_current, sequence, result,
             this](const std::vector<double>& loc_initials_x,
                   const std::vector<double>& loc_initials_y,
                   const double& cur_progr) -> std::pair<std::vector<float>, std::vector<float>> {
            std::pair<std::vector<float>, std::vector<float>> results;
            results.first.resize(loc_initials_x.size());
            results.second.resize(loc_initials_x.size());

            flow_current->set_xy_dev(loc_initials_x[0], loc_initials_y[0]);
            flow_current->set_n_circles(this->m_parameters->number_of_circles);
            flow_current->set_beam_initials(
                std::vector<double>(loc_initials_x.begin() + 1, loc_initials_x.end()),
                std::vector<double>(loc_initials_y.begin() + 1, loc_initials_y.end()));

            MadXCenterBeam caller(results_id, m_monitors);

            const auto caller_result = caller.run_madx(results_id, flow_current, sequence);

            auto tmp_res = loadcm(commtools::file_to_string(caller.get_file_path()));

            for (const auto& p : tmp_res)
            {
                auto& X = p.second[0];
                auto& Y = p.second[2];

                auto ss = X.size();

                std::vector<float> A(X.size());
                std::vector<float> R(X.size());


                for (size_t j = 0; j < X.size(); j++)
                {
                    A[j] = std::sqrt(std::pow((X[j] - loc_initials_x[p.first]), 2.0) +
                                     std::pow((Y[j] - loc_initials_y[p.first]), 2.0));

                    R[j] = std::sqrt(std::pow(X[j], 2.0) + std::pow(Y[j], 2.0));           
                }
                auto min_el = std::min_element(A.begin() + 1, A.end());
                auto n_c    = std::distance(A.begin(), min_el);

                results.first[p.first - 1] =
                    (4.0 * double(this->m_parameters->number_of_circles)) / double(n_c);

               

                results.second[p.first - 1] = *std::max_element(R.begin() + 1, R.begin() + n_c);
            }

            // for (size_t i = 0; i < loc_initials_x.size(); i++)
            // {
            //     while (!IDataCommunicator::get().set_result_progress(
            //         results_id, cur_progr + 0.5 * i /
            //         static_cast<double>(loc_initials_x.size())))
            //         ;

            //     MadXCenterMass    caller(results_id, m_monitors);
            //     MadXTwiss         caller_tw(results_id);
            //     const std::string mad_out = caller_tw.get_file_path();

            //     flow_current->set_xy_dev(loc_initials_x[i], loc_initials_y[i]);
            //     flow_current->set_n_circles(this->m_parameters->number_of_circles);
            //     const auto caller_result = caller.run_madx(results_id, flow_current, sequence);

            //     const auto caller_result_tw = caller_tw.run_madx(results_id, flow_current,
            //     sequence);

            //     // if (caller_result.first.empty())
            //     // {
            //     //     return result;
            //     // }
            //     auto tmp_res    = loadcm(commtools::file_to_string(caller.get_file_path()));
            //     auto tmp_res_tw =
            //     loadtw(commtools::file_to_string(caller_tw.get_file_path_twiss()));

            //     std::vector<double> beta_x;
            //     std::vector<double> beta_y;

            //     std::vector<std::string> cur_mons;
            //     cur_mons.push_back("MACHINE$START");
            //     cur_mons.insert(cur_mons.begin() + 1, m_monitors.begin(), m_monitors.end());
            //     cur_mons.push_back("MACHINE$END");

            //     for (const auto& el : tmp_res_tw)
            //     {
            //         for (const auto& mon : cur_mons)
            //         {
            //             auto tt = "\"" + mon + "\"";
            //             if (tt == el.first)
            //             {
            //                 beta_x.push_back(el.second[1]);
            //                 beta_y.push_back(el.second[4]);
            //                 break;
            //             }
            //         }
            //     }

            //     auto& cm_ = tmp_res.begin()->second;
            //     auto& X   = cm_[0];
            //     auto& Y   = cm_[2];

            //     auto ss = X.size();

            //     std::vector<float> A(X.size());
            //     for (size_t j = 0; j < X.size(); j++)
            //     {
            //         A[j] = std::sqrt(std::pow((X[j] - loc_initials_x[i]), 2.0) +
            //                          std::pow((Y[j] - loc_initials_y[i]), 2.0));
            //     }
            //     auto min_el = std::min_element(A.begin() + 1, A.end());
            //     auto n_c    = std::distance(A.begin(), min_el);
            //     results[i]  = (4.0 * double(this->m_parameters->number_of_circles)) /
            //     double(n_c);
            // }

            return results;
        };

        const auto res_x = process(initials_x, zeros_y, 0.0);

        const auto res_y = process(zeros_x, initials_y, 0.5);

        int nCharts = 50;

        result.x = getChart(res_x.first, nCharts, 0, -1, false);
        result.y = getChart(res_y.first, nCharts, 0, -1, false);

        result.x_resonances =
            std::pair<std::vector<float>, std::vector<float>>(res_x.first, res_x.second);
        result.y_resonances =
            std::pair<std::vector<float>, std::vector<float>>(res_y.first, res_y.second);

        return result;
    }

    bool from_model_view(const std::string                         name,
                         const std::map<std::string, std::string>& content) override final
    {

        BL_FTRACE();

        const std::string fail_msg = "failed to load from view " + name;

        auto data = this->extract_content(fail_msg, content, "");

        if (!data.first)
        {
            return false;
        }

        m_parameters = srfl::read_json_string<BetatronResonancesConfig>(data.second);

        if (!m_parameters)
        {
            return false;
        }

        auto data_mon = this->extract_content(fail_msg, content, "Monitors labels");

        if (!data_mon.first)
        {
            return false;
        }

        m_monitors = commtools::strsplit_adapter<std::vector<std::string>>(data_mon.second,
                                                                           std::string(": ,;=,"));

        return from_model_view_other(name, content);
    }

  protected:
    std::shared_ptr<BetatronResonancesConfig> m_parameters;
    std::vector<std::string>                  m_monitors;
};

class Solver_BetatronResonanses final
    : public Solver_BetatronResonansesBase<Solver_BetatronResonanses>
{
  public:
    BUILD_CHILD(Solver_BetatronResonanses, daisi::IModelComponent)

    SolverResult run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                     const std::shared_ptr<Sequence>& sequence) const
        noexcept override;

    virtual bool
    from_model_view_other(const std::string                         name,
                          const std::map<std::string, std::string>& content) override final;

  private:
};
} // namespace accel
} // namespace daisi

#endif
