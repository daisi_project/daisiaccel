#include <fstream>

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <common_tools/helpers.hpp>
#include <common_tools/json_helper.h>

#include "../../base/idatacommunicator.h"
#include "../../common/plot_tools.h"

#include "fourier_common.h"
#include "fourier_file.h"
#include "madxcaller.h"
#include "tools.h"

namespace daisi
{
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(SolverFourierFile, daisi::IModelComponent, "Fourier_analysis_file")

bool SolverFourierFile::from_model_view(const std::string name,
                                        const std::map<std::string, std::string>& content)
{
    BL_FTRACE();

    const std::string fail_msg = "failed to load from view " + name;

    auto data = this->extract_content(fail_msg, content, "Processed intervals");

    if (!data.first)
    {
        return false;
    }

    m_config = srfl::read_json_string<Config>(data.second);

    data = this->extract_content(fail_msg, content, "Data file");

    if (!data.first)
    {
        return false;
    }

    m_exel_data = std::make_shared<std::vector<std::vector<std::string>>>();

    BL_TRACE() << data.second;

    auto pt = commtools::readJSONString(data.second);

    if (!pt)
    {
        return false;
    }
    for (auto& item : pt->get_child("content"))
    {
        m_exel_data->push_back(commtools::json_vector<std::string>(item.second, ""));
    }
    // m_exel_data = srfl::read_json_string<ExelData>(data.second);

    if (!m_exel_data || !m_config)
    {
        return false;
    }

    return true;
}

SolverResult SolverFourierFile::run_concrete_solver(const int                        results_id,
                                                    const std::shared_ptr<Flow>&     flow,
                                                    const std::shared_ptr<Sequence>& sequence) const
    noexcept
{
    int tt = 0;

    std::vector<double> errors;

    try
    {
        for (size_t i = m_config->Effective_length_row_start;
             i < m_config->Effective_length_row_end; i++)
        {
            auto v = m_exel_data->at(i - 2);
            errors.push_back(std::stod(v.at(m_config->Effective_length_column - 1)));
        }
    }
    catch (const std::exception& e)
    {
        SolverResult res;
        res.status = false;

        BL_ERROR() << e.what();
        return res;
    }
    
    return fourier_errors(errors, m_config->Number_of_series_members, "effective lenghts");
}
} // namespace accel
} // namespace daisi
