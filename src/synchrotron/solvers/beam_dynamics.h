#ifndef DAISI_SYNC_BEAM_DYN_H
#define DAISI_SYNC_BEAM_DYN_H

#include "solver_madx.h"

namespace daisi
{
namespace accel
{
class Solver_BeamDynamics final : public SolverSynchrotron<Solver_BeamDynamics>
{
  public:
    BUILD_CHILD(Solver_BeamDynamics, daisi::IModelComponent)

    bool from_model_view(const std::string                         name,
                         const std::map<std::string, std::string>& content) override final;

    SolverResult run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                     const std::shared_ptr<Sequence>& sequence) const
        noexcept override final;

  private:
    bool m_use_mad;
    int  m_n_particles;
    int  m_distrType;

    std::pair<std::vector<std::array<std::vector<float>, 5>>, std::string>
    calc_using_daisi(const std::vector<arma::vec>     x0,
                     const std::shared_ptr<Sequence>& sequence) const noexcept;

    std::pair<std::array<std::vector<float>, 5>, std::string>
    calc_using_mad(SolverResult& result, const int results_id, const std::shared_ptr<Flow>& flow,
                   const std::shared_ptr<Sequence>& sequence) const noexcept;
};
} // namespace accel
} // namespace daisi
#endif
 