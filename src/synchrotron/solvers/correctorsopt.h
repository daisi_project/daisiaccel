#ifndef DAISI_SYNC_SOLVER_CORRECTORS_OPT_H
#define DAISI_SYNC_SOLVER_CORRECTORS_OPT_H

#include <serreflection/defines.hpp>

#include "solver_madx.h"

namespace notk
{
template <class Targ, class Tfit>
class NOTKController;
}

namespace daisi
{
namespace accel
{
class Solver_CorrectorsOpt final : public SolverSynchrotron<Solver_CorrectorsOpt>
{
  public:
    BUILD_CHILD(Solver_CorrectorsOpt, daisi::IModelComponent)

    bool from_model_view(const std::string                         name,
                         const std::map<std::string, std::string>& content) override final;

    SolverResult run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                     const std::shared_ptr<Sequence>& sequence) const
        noexcept override final;

    struct Config
    {
        double coefficient_left;
        double coefficient_right;
        double penalty_deviation;
        double min_angle;
        double max_angle;
    };

  private:
    std::shared_ptr<Config> m_config;

    std::shared_ptr<notk::NOTKController<double, double>> m_notk;
    std::shared_ptr<notk::NOTKController<double, double>> m_notk_ext;

    std::vector<std::pair<std::string, std::string>> m_monitors_areas;
};
} // namespace accel
} // namespace daisi

SERIALIZIBLE_STRUCT(daisi::accel::Solver_CorrectorsOpt::Config, srfl::CheckModes::FATAL,
                    (double, penalty_deviation, srfl::nan, -1e-200,
                     srfl::inf)(double, coefficient_left, srfl::nan, -1e-200,
                                srfl::inf)(double, coefficient_right, srfl::nan, -1e-200,
                                           srfl::inf)(double, min_angle, srfl::nan, -1.0,
                                                      -1e-200)(double, max_angle, srfl::nan,
                                                               -1e-200, 1.0))

#endif
