#include "../../common/solverresult.h"
namespace daisi
{
namespace accel
{
SolverResult fourier_errors(const std::vector<double>& err_in, const unsigned n_members,
                            const std::string& postfix);
}
}