#ifndef DAISI_SYNC_SOLVET_FOURIER_FILE_H
#define DAISI_SYNC_SOLVET_FOURIER_FILE_H

#include <serreflection/defines.hpp>

#include "solver.h"

namespace daisi
{
namespace accel
{
class SolverFourierFile final : public SolverSynchrotron<SolverFourierFile>
{
  public:
    struct Config
    {
        double Effective_length_column;
        double Effective_length_row_start;
        double Effective_length_row_end;
        double Number_of_series_members;
    };

    BUILD_CHILD(SolverFourierFile, daisi::IModelComponent)

    bool from_model_view(const std::string                         name,
                         const std::map<std::string, std::string>& content) override final;

    SolverResult run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                     const std::shared_ptr<Sequence>& sequence) const
        noexcept override final;

  private:
    std::shared_ptr<Config>                                m_config;
    std::shared_ptr<std::vector<std::vector<std::string>>> m_exel_data;
};
} // namespace accel
} // namespace daisi

SERIALIZIBLE_STRUCT(daisi::accel::SolverFourierFile::Config, srfl::CheckModes::FATAL,
                    (double, Effective_length_column, srfl::nan, 1.0,
                     srfl::inf)(double, Effective_length_row_start, srfl::nan, 1.0,
                                srfl::inf)(double, Effective_length_row_end, srfl::nan, 1.0,
                                           srfl::inf)(double, Number_of_series_members, srfl::nan,
                                                      1.0, srfl::inf))
#endif
