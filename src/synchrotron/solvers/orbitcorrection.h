#ifndef DAISI_SYNC_ORBIT_CORRECTION_H
#define DAISI_SYNC_ORBIT_CORRECTION_H

#include "solver_madx.h"

namespace daisi
{
namespace accel
{
class Solver_OrbitCorrection final : public SolverSynchrotron<Solver_OrbitCorrection>
{
  public:
    BUILD_CHILD(Solver_OrbitCorrection, daisi::IModelComponent)

    enum class CorrMethod
    {
        MIKADO,
        SVD
    };

    bool from_model_view(const std::string                         name,
                         const std::map<std::string, std::string>& content) override final;

    SolverResult run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                     const std::shared_ptr<Sequence>& sequence) const
        noexcept override final;

  private:
    CorrMethod method;

    int Nkx;
    int Nky;

    std::pair<std::array<std::vector<float>, 5>, std::string>
    calc_using_daisi(const int results_id, const std::shared_ptr<Flow>& flow,
                     const std::shared_ptr<Sequence>& sequence) const noexcept;

    std::pair<std::array<std::vector<float>, 5>, std::string>
    calc_using_mad(SolverResult& result, const int results_id, const std::shared_ptr<Flow>& flow,
                   const std::shared_ptr<Sequence>& sequence) const noexcept;
};
} // namespace accel
} // namespace daisi
#endif
