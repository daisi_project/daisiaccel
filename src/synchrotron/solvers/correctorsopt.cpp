#include <fstream>

#include <boost/filesystem.hpp>

#include <common_tools/algorithm.hpp>
#include <common_tools/helpers.hpp>
#include <notk/controller.hpp>
#include <serreflection/read_json.hpp>

#include "../../common/mad_safe_wrapper.h"
#include "correctorsopt.h"
#include "madxcaller.h"
#include "madxcentermass.h"
#include "tools.h"

namespace daisi
{
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(Solver_CorrectorsOpt, daisi::IModelComponent, "Correctors_optimization")

bool Solver_CorrectorsOpt::from_model_view(const std::string                         name,
                                           const std::map<std::string, std::string>& content)
{
    m_config = srfl::read_json_string<Config>(content.at("Main parameters"));

    if (!m_config)
    {
        throw std::runtime_error("Incorrect main parameters");
    }

    m_notk = std::make_shared<notk::NOTKController<double, double>>();

    if (!m_notk->set_problem_config_str(content.at("NOTK config")))
    {
        throw std::runtime_error("Incorrect NOTK config");
    }

    m_notk_ext = std::make_shared<notk::NOTKController<double, double>>();

    if (!m_notk_ext->set_problem_config_str(content.at("External NOTK config")))
    {
        throw std::runtime_error("Incorrect NOTK config");
    }

    std::stringstream strream_monitors;
    strream_monitors << content.at("Monitors areas");

    int  MAX_LENGTH = 1000;
    char line[MAX_LENGTH];

    while (strream_monitors.getline(line, MAX_LENGTH))
    {
        auto splitted =
            commtools::strsplit_adapter<std::vector<std::string>>(line, std::string(","));

        if (splitted.size() != 2)
        {
            throw std::runtime_error("Incorrect monitors areas description");
        }
        m_monitors_areas.push_back(std::make_pair(splitted[0], splitted[1]));
    }

    return true;
}

SolverResult
Solver_CorrectorsOpt::run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                          const std::shared_ptr<Sequence>& sequence) const noexcept
{
    BL_FTRACE();
    SolverResult result;
    result.status = false;

    std::vector<size_t> areas_indexes;

    for (const auto& area : m_monitors_areas)
    {
        auto i1 = sequence->get_element_number_by_label(area.first);
        auto i2 = sequence->get_element_number_by_label(area.second);

        if (i1 < 0 || i2 < 0 || i1 >= i2)
        {
            BL_WARNING()
                << "Skipped elements area between " << area.first << " and " << area.second
                << " because of invalid elements labels. Force use default mode for center mass "
                   "deviation minimization";
        }
        else
        {
            for (size_t i = static_cast<size_t>(i1); i <= static_cast<size_t>(i2); i++)
            {
                areas_indexes.push_back(i);
            }
        }
    }

    double min_res     = std::numeric_limits<double>::infinity();
    double min_res_ext = std::numeric_limits<double>::infinity();

    MadXCenterMass caller(results_id);

    const auto correctors = sequence->get_elements_list("KICKER");

    auto seq_new  = std::make_shared<Sequence>(*sequence);
    auto seq_best = std::make_shared<Sequence>(*sequence);

    std::vector<double> currents_best;
    std::vector<float>  fit_best;

    auto fitness_ext = [&](const std::vector<double>& coefficient_rel) -> double {
        auto fitness = [&](const std::vector<double>& args) -> double {
            seq_new->set_corrector_angles(args);

            const auto caller_result = caller.run_madx(results_id, flow, seq_new);

            auto res = loadcm(commtools::file_to_string(caller.get_file_path()));

            if (res.size() != 1)
            {
                throw std::runtime_error("Error read MADX results file");
            }
            auto& cm_ = res.begin()->second;

            std::array<std::vector<float>, 5> cm;

            size_t i0 = 0;

            for (size_t i = 1; i < cm_[5].size(); i++)
            {
                for (size_t ii = i0; ii < cm_[5][i]; ii++)
                {
                    for (size_t k = 0; k < 5; k++)
                    {
                        cm[k].push_back(cm_[k][i]);
                    }
                }
                i0 = cm_[5][i];
            }

            std::vector<size_t> all_inds(cm[4].size());

            std::iota(all_inds.begin(), all_inds.end(), 0);

            std::vector<size_t> diff;

            std::set_difference(all_inds.begin(), all_inds.end(), areas_indexes.begin(),
                                areas_indexes.end(), std::inserter(diff, diff.begin()));

            double result_fit = 0;

            for (const auto& ind : areas_indexes)
            {
                result_fit += cm[0][ind] * cm[0][ind] + cm[2][ind] * cm[2][ind];
            }

            auto get_penalty = [&](const double val) -> double {
                if (std::abs(val) > m_config->penalty_deviation)
                {
                    result_fit += coefficient_rel[0] *
                                  std::pow(std::abs(val) - m_config->penalty_deviation, 2.0);
                }
            };

            for (const auto& ind : diff)
            {
                get_penalty(cm[0][ind]);
                get_penalty(cm[2][ind]);
            }

            if (result_fit < min_res)
            {
                min_res       = result_fit;
                seq_best      = std::make_shared<Sequence>(*seq_new);
                currents_best = args;
            }

            return result_fit;
        };

        std::vector<double> left(2 * correctors.size());
        std::vector<double> right(2 * correctors.size());

        std::fill(left.begin(), left.end(), m_config->min_angle);
        std::fill(right.begin(), right.end(), m_config->max_angle);

        if (!m_notk->set_borders_fitness(fitness, left, right, sequence->get_corrector_angles()))
        {
            throw std::runtime_error("Error NOTK configuration");
        }

        bool flag_abort = true;

        auto result_notk = m_notk->process(flag_abort);

        if (!result_notk)
        {
            std::runtime_error("Error NOTK calculation");
        }

        const auto cur_res = result_notk->get_last_it_res().second;

        if (cur_res < min_res_ext)
        {
            min_res_ext = cur_res;
            fit_best    = result_notk->get_fit_array<float>();
        }

        return cur_res;
    };

    std::vector<double> left(1);
    std::vector<double> right(1);
    left[0]  = m_config->coefficient_left;
    right[0] = m_config->coefficient_right;

    if (!m_notk_ext->set_borders_fitness(fitness_ext, left, right))
    {
        throw std::runtime_error("Error NOTK configuration");
    }

    bool flag_abort = true;

    auto result_notk_ext = m_notk_ext->process(flag_abort);

    Solver_MadXCenterMass cm_solver;

    result = cm_solver.run_concrete_solver(results_id, flow, seq_best);

    auto generate_x_scale = [&](std::vector<float>& scale, const size_t size) {
        scale.resize(size);
        std::iota(scale.begin(), scale.end(), 0.0f);
    };

    result.plots.emplace_back();
    auto& plot1 = result.plots.back();
    plot1.label = "Fitness";
    plot1.YData.push_back(fit_best);
    plot1.XData.emplace_back();
    generate_x_scale(plot1.XData.back(), plot1.YData.back().size());

    result.plots.emplace_back();
    auto& plot2 = result.plots.back();
    plot2.label = "HKICK and VKICK";
    plot2.YData.resize(2);
    for (size_t i = 0; i < currents_best.size() - 1; i = i + 2)
    {
        plot2.XData.resize(2);
        generate_x_scale(plot2.XData[0], plot2.YData[0].size());
        generate_x_scale(plot2.XData[1], plot2.YData[1].size());
        plot2.YData[0].push_back(currents_best[i]);
        plot2.YData[1].push_back(currents_best[i + 1]);
    }

    result.status = true;
    return result;
}
} // namespace accel
} // namespace daisi
