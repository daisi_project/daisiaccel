#include <fstream>

#include <boost/filesystem.hpp>

#include <common_tools/helpers.hpp>

#include "../../common/mad_safe_wrapper.h"
#include "madxcaller.h"
#include "madxcentermass.h"
#include "tools.h"

namespace daisi
{
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(Solver_MadXCenterMass, daisi::IModelComponent,
                            "Center_mass_dynamics_madx")

bool Solver_MadXCenterMass::from_model_view(const std::string                         name,
                                            const std::map<std::string, std::string>& content)
{
    std::stringstream ss;

    ss << content.at("");

    boost::property_tree::ptree pt;

    boost::property_tree::read_json(ss, pt);

    m_use_mad = false;

    m_use_mad = pt.get<bool>("MAD");

    return true;
}

std::pair<std::array<std::vector<float>, 5>, std::string>
Solver_MadXCenterMass::calc_using_mad(SolverResult& result, const int results_id,
                                      const std::shared_ptr<Flow>&     flow,
                                      const std::shared_ptr<Sequence>& sequence) const noexcept
{
    MadXCenterMass caller(results_id);

    std::pair<std::array<std::vector<float>, 5>, std::string> result_;

    const auto caller_result = caller.run_madx(results_id, flow, sequence);

    if (caller_result.first.empty())
    {
        result_.second = "Error creation madx caller ";
        return result_;
    }

    result.status = caller_result.second;

    result.text.emplace_back("MADX generated input", caller_result.first);

    auto mad_out_res_string = commtools::file_to_string(caller.get_file_path());

    result.text.emplace_back("MADX output", mad_out_res_string);

    auto tmp_res = loadcm(mad_out_res_string);

    if (tmp_res.size() != 1)
    {
        result_.second = "Error read MADX results file ";
        return result_;
    }

    auto& cm_ = tmp_res.begin()->second;

    auto& cm = result_.first;

    size_t i0 = 0;

    for (size_t i = 1; i < cm_[5].size(); i++)
    {
        for (size_t ii = i0; ii < cm_[5][i]; ii++)
        {
            for (size_t k = 0; k < 5; k++)
            {
                cm[k].push_back(cm_[k][i]);
            }
        }
        i0 = cm_[5][i];
    }

    return result_;
}

std::pair<std::array<std::vector<float>, 5>, std::string>
Solver_MadXCenterMass::calc_using_daisi(const int results_id, const std::shared_ptr<Flow>& flow,
                                        const std::shared_ptr<Sequence>& sequence) const noexcept
{
    const arma::vec x0 = flow->get_initial_data();
    const auto      Ms = sequence->get_matrixes();

    std::pair<std::array<std::vector<float>, 5>, std::string> result_;

    result_.first = calc_trace<float>(x0, Ms);

    return result_;
}
// 3.5+0.47+0.265+0.16+0.16
SolverResult
Solver_MadXCenterMass::run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                           const std::shared_ptr<Sequence>& sequence) const noexcept
{
    BL_FTRACE();
    SolverResult result;
    result.status = false;
    std::pair<std::array<std::vector<float>, 5>, std::string> result_;

    if (m_use_mad)
    {
        result_ = calc_using_mad(result, results_id, flow, sequence);
    }
    else
    {
        result_ = calc_using_daisi(results_id, flow, sequence);
        result.text.emplace_back("MADX generated input", "no data");
        result.text.emplace_back("MADX output", "no data");
    }

    if (!result_.second.empty())
    {
        BL_ERROR() << result_.second;
        return result;
    }

    result.write_cm_data(result_.first);


    result.status = true;
    return result;
}
} // namespace accel
} // namespace daisi
