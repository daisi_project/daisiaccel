#ifndef DAISI_SYNC_SOLVET_FOURIER_H
#define DAISI_SYNC_SOLVET_FOURIER_H

#include "solver_madx.h"

namespace daisi
{
namespace accel
{
class Solver_Fourier final : public SolverSynchrotron<Solver_Fourier>
{
  public:
    BUILD_CHILD(Solver_Fourier, daisi::IModelComponent)

    bool from_model_view(const std::string name,
                         const std::map<std::string, std::string>& content) override final;

    SolverResult run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                     const std::shared_ptr<Sequence>& sequence) const
        noexcept override final;

  private:
    int m_n_members;
};
}
}
#endif
