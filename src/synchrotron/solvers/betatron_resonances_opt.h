#ifndef DAISI_SYNC_BETATRON_RESONANCES_OPT_H
#define DAISI_SYNC_BETATRON_RESONANCES_OPT_H

#include <notk/controller.hpp>

#include "solver.h"

#include "betatron_resonances_config.h"

#include "betatron_resonances.h"

namespace notk
{
template <class Targ, class Tfit>
class NOTKController;
}

namespace daisi
{
namespace accel
{
class Solver_BetatronResonansesOpt final
    : public Solver_BetatronResonansesBase<Solver_BetatronResonansesOpt>
{
  public:
    BUILD_CHILD(Solver_BetatronResonansesOpt, daisi::IModelComponent)

    SolverResult run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                     const std::shared_ptr<Sequence>& sequence) const
        noexcept override final;

    virtual bool
    from_model_view_other(const std::string                         name,
                          const std::map<std::string, std::string>& content) override final;

    struct Config
    {
        double Expected_frequency_x;
        double Expected_frequency_y;
        double min_angle;
        double max_angle;
        double frequency_deviation_weight;
    };

  private:
    std::shared_ptr<notk::NOTKController<double, double>> m_notk;

    std::shared_ptr<Config> m_config;
};
} // namespace accel
} // namespace daisi
SERIALIZIBLE_STRUCT(daisi::accel::Solver_BetatronResonansesOpt::Config, srfl::CheckModes::FATAL,
                    (double, Expected_frequency_x, srfl::nan, -1e-200,
                     srfl::inf)(double, Expected_frequency_y, srfl::nan, -1e-200,
                                srfl::inf)(double, min_angle, srfl::nan, -0.0001,
                                           0.0)(double, max_angle, srfl::nan, -1e-200,
                                                0.0001)(double, frequency_deviation_weight,
                                                        srfl::nan, 0.0, srfl::inf))

#endif
