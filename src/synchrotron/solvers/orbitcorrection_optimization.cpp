#include <fstream>

#include <boost/filesystem.hpp>

#include <common_tools/helpers.hpp>
#include <notk/controller.hpp>

#include "../../common/mad_safe_wrapper.h"
#include "orbitcorrection_optimization.h"
#include "tools.h"

namespace daisi
{
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(Solver_OrbitCorrectionOptimization, daisi::IModelComponent,
                            "Orbit_correction_optimization")

bool Solver_OrbitCorrectionOptimization::from_model_view(
    const std::string name, const std::map<std::string, std::string>& content)
{
    std::stringstream ss;

    m_notk = std::make_shared<notk::NOTKController<double, double>>();

    if (!m_notk->set_problem_config_str(content.at("NOTK config")))
    {
        throw std::runtime_error("Incorrect NOTK config");
    }

    boost::property_tree::ptree pt;

    ss << content.at("Parameters");
    boost::property_tree::read_json(ss, pt);

    m_min_angle = pt.get<double>("min_angle");
    m_max_angle = pt.get<double>("max_angle");

    return true;
}

SolverResult Solver_OrbitCorrectionOptimization::run_concrete_solver(
    const int results_id, const std::shared_ptr<Flow>& flow,
    const std::shared_ptr<Sequence>& sequence) const noexcept
{
    BL_FTRACE();
    SolverResult result;
    result.status = false;

    auto       seq_new    = std::make_shared<Sequence>(*sequence);
    const auto correctors = sequence->get_elements_list("KICKER");

    std::vector<double> left(2 * correctors.size());
    std::vector<double> right(2 * correctors.size());

    std::fill(left.begin(), left.end(), m_min_angle);
    std::fill(right.begin(), right.end(), m_max_angle);

    auto Ms = sequence->get_matrixes("MONITOR");

    const arma::vec x0 = flow->get_initial_data();

    auto resf = calc_trace<float>(x0, Ms);

    std::array<std::vector<float>, 5> res_;

    auto fitness = [&](const std::vector<double>& args) -> double {
        seq_new->set_corrector_angles(args);

        auto Ms = seq_new->get_matrixes("MONITOR");

        auto res = calc_trace<double>(x0, Ms);

        double result = 0;

        for (const auto& val : res[0])
        {
            result += val * val;
        }
        for (const auto& val : res[2])
        {
            result += val * val;
        }
        return result;
    };

    if (!m_notk->set_borders_fitness(fitness, left, right, sequence->get_corrector_angles()))
    {
        return result;
    }

    bool flag_abort = true;

    auto result_notk = m_notk->process(flag_abort);

    if (!result_notk)
    {
        result.error_message = "Error NOTK calculation";
    }

    const auto cur_res  = result_notk->get_last_it_res().first;
    auto       fit_best = result_notk->get_fit_array<float>();

    seq_new->set_corrector_angles(cur_res);

    result.text.emplace_back("Corrected optic", seq_new->generate_mad_optic());

    auto Ms_ = seq_new->get_matrixes("MONITOR");
    res_     = calc_trace<float>(x0, Ms_);

    result.text.emplace_back("Corrected optic", seq_new->generate_mad_optic());

    auto generate_x_scale = [&](std::vector<float>& scale, const size_t size) {
        scale.resize(size);
        std::iota(scale.begin(), scale.end(), 0.0f);
    };

    result.plots.emplace_back();
    auto& plotf = result.plots.back();
    plotf.label = "Fitness";
    plotf.YData.push_back(fit_best);
    plotf.XData.emplace_back();

    generate_x_scale(plotf.XData.back(), plotf.YData.back().size());

    result.plots.emplace_back();
    auto& plot1 = result.plots.back();
    plot1.label = "X(Z)";
    plot1.XData.push_back(res_[4]);
    plot1.XData.push_back(res_[4]);
    plot1.YData.push_back(resf[0]);
    plot1.YData.push_back(res_[0]);

    result.plots.emplace_back();
    auto& plot2 = result.plots.back();
    plot2.label = "Y(Z)";
    plot2.XData.push_back(res_[4]);
    plot2.XData.push_back(res_[4]);
    plot2.YData.push_back(resf[2]);
    plot2.YData.push_back(res_[2]);

    result.status = true;
    return result;
}
} // namespace accel
} // namespace daisi
