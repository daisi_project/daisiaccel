#include <fstream>

#include <armadillo>

#include <boost/filesystem.hpp>

#include <common_tools/algorithm.hpp>
#include <common_tools/helpers.hpp>
#include <notk/controller.hpp>

#include "../../common/common_tools.h"
#include "../../common/mad_safe_wrapper.h"
#include "../../common/plot_tools.h"
#include "betatron_resonances_opt.h"
#include "madxcaller.h"
#include "tools.h"

namespace daisi
{
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(Solver_BetatronResonansesOpt, daisi::IModelComponent,
                            "Betatron_resonances_opt")

bool Solver_BetatronResonansesOpt::from_model_view_other(
    const std::string name, const std::map<std::string, std::string>& content)
{

    BL_FTRACE();

    m_notk = std::make_shared<notk::NOTKController<double, double>>();

    if (!m_notk->set_problem_config_str(content.at("NOTK config")))
    {
        throw std::runtime_error("Incorrect NOTK config");
    }

    m_config = srfl::read_json_string<Config>(content.at("Optimization parametes"));

    if (!m_config)
    {
        throw std::runtime_error("Incorrect  parameters");
    }

    return true;
}

SolverResult Solver_BetatronResonansesOpt::run_concrete_solver(
    const int results_id, const std::shared_ptr<Flow>& flow,
    const std::shared_ptr<Sequence>& sequence) const noexcept
{
    BL_FTRACE();
    SolverResult result;
    result.status = false;

    const auto spectrums_old = calculate_spectrums(results_id, flow, sequence);

    auto       seq_new    = std::make_shared<Sequence>(*sequence);
    const auto correctors = sequence->get_elements_list("KICKER");

    auto fitness = [&](const std::vector<double>& args) -> double {
        seq_new->set_corrector_angles(args);

        const auto spectrums = calculate_spectrums(results_id, flow, seq_new);

        const auto x_g = estimate_gauss(spectrums.x.first, spectrums.x.second, 0.01);
        const auto y_g = estimate_gauss(spectrums.y.first, spectrums.y.second, 0.01);

        return m_config->frequency_deviation_weight *
                   (std::pow(x_g[0] - m_config->Expected_frequency_x, 2.0) +
                    std::pow(y_g[0] - m_config->Expected_frequency_y, 2.0)) +
               x_g[1] + y_g[1];
    };

    std::vector<double> left(2 * correctors.size());
    std::vector<double> right(2 * correctors.size());

    std::fill(left.begin(), left.end(), m_config->min_angle);
    std::fill(right.begin(), right.end(), m_config->max_angle);

    if (!m_notk->set_borders_fitness(fitness, left, right, sequence->get_corrector_angles()))
    {
        throw std::runtime_error("Error NOTK configuration");
    }

    bool flag_abort = true;

    auto result_notk = m_notk->process(flag_abort);

    if (!result_notk)
    {
        std::runtime_error("Error NOTK calculation");
    }

    const auto cur_res = result_notk->get_last_it_res().first;

    seq_new->set_corrector_angles(cur_res);

    const auto spectrums_new = calculate_spectrums(results_id, flow, seq_new);

    result.plots.emplace_back();
    auto& plot1 = result.plots.back();
    plot1.label = "X spectrum";

    auto chart_line_old = convertChart(spectrums_old.x.first, spectrums_old.x.second);
    auto chart_line_new = convertChart(spectrums_new.x.first, spectrums_new.x.second);

    plot1.XData.push_back(chart_line_old.first);
    plot1.YData.push_back(chart_line_old.second);
    plot1.XData.push_back(chart_line_new.first);
    plot1.YData.push_back(chart_line_new.second);

    result.plots.emplace_back();
    auto& plot2 = result.plots.back();
    plot2.label = "Y spectrum";

    chart_line_old = convertChart(spectrums_old.y.first, spectrums_old.y.second);
    chart_line_new = convertChart(spectrums_new.y.first, spectrums_new.y.second);

    plot2.XData.push_back(chart_line_old.first);
    plot2.YData.push_back(chart_line_old.second);
    plot2.XData.push_back(chart_line_new.first);
    plot2.YData.push_back(chart_line_new.second);

    auto generate_x_scale = [&](std::vector<float>& scale, const size_t size) {
        scale.resize(size);
        std::iota(scale.begin(), scale.end(), 0.0f);
    };

    auto fit_best = result_notk->get_fit_array<float>();

    result.plots.emplace_back();
    auto& plot3 = result.plots.back();
    plot3.label = "Fitness";
    plot3.YData.push_back(fit_best);
    plot3.XData.emplace_back();
    generate_x_scale(plot3.XData.back(), plot3.YData.back().size());

    result.status = true;
    return result;
} // namespace accel
} // namespace accel
} // namespace daisi
