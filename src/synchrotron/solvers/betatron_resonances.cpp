#include <fstream>

#include <algorithm>
#include <armadillo>

#include <boost/filesystem.hpp>

#include <common_tools/algorithm.hpp>
#include <common_tools/helpers.hpp>

#include "../../base/idatacommunicator.h"
#include "../../common/mad_safe_wrapper.h"
#include "../../common/plot_tools.h"
#include "betatron_resonances.h"
#include "madxcaller.h"

namespace daisi
{
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(Solver_BetatronResonanses, daisi::IModelComponent,
                            "Betatron_resonances")

bool Solver_BetatronResonanses::from_model_view_other(
    const std::string name, const std::map<std::string, std::string>& content)
{
    return true;
}

SolverResult Solver_BetatronResonanses::run_concrete_solver(
    const int results_id, const std::shared_ptr<Flow>& flow,
    const std::shared_ptr<Sequence>& sequence) const noexcept
{
    BL_FTRACE();
    SolverResult result;
    result.status = false;

    const auto spectrums = calculate_spectrums(results_id, flow, sequence);

    result.plots_ext.emplace_back();
    auto& plot1 = result.plots_ext.back();
    plot1.label = "X spectrum";

    auto chart_line = convertChart(spectrums.x.first, spectrums.x.second);
    plot1.XData.push_back(chart_line.first);
    plot1.YData.push_back(chart_line.second);
    plot1.labels.push_back("Y = 0");

    result.plots_ext.emplace_back();
    auto& plot2 = result.plots_ext.back();
    plot2.label = "Y spectrum";

    chart_line = convertChart(spectrums.y.first, spectrums.y.second);
    plot2.XData.push_back(chart_line.first);
    plot2.YData.push_back(chart_line.second);
    plot2.labels.push_back("X = 0");
    const double eps = 1e-3;

    auto is_eq = [eps](const auto& val1, const auto& val2) {
        return std::abs((val1 - val2) / val2) < eps;
    };

    auto fill_resonances = [eps, is_eq](SolverResult::ResultPlot& plot, const std::string& title,
                                        const std::vector<float>& res_x,
                                        const std::vector<float>& res_y) {
        plot.label = title;
        auto x     = res_x;
        std::sort(x.begin(), x.end());

        std::unique(x.begin(), x.end(), is_eq);

        std::vector<float> y;

        for (const auto& val : x)
        {
            std::vector<float> y_pretends;

            for (auto it1 = res_x.begin(), it2 = res_y.begin(); it1 < res_x.end(); it1++, it2++)
            {
                if (is_eq(*it1, val))
                {
                    y_pretends.push_back(*it2);
                }
            }
            y.push_back(*std::max_element(y_pretends.begin(), y_pretends.end()));
        }
        plot.XData.push_back(x);
        plot.YData.push_back(y);
    };

    result.plots.emplace_back();
    auto& plot3 = result.plots.back();

    fill_resonances(plot3, "Resonances X", spectrums.x_resonances.first,
                    spectrums.x_resonances.second);

    result.plots.emplace_back();
    auto& plot4 = result.plots.back();

    fill_resonances(plot4, "Resonances Y", spectrums.y_resonances.first,
                    spectrums.y_resonances.second);

    result.status = true;
    return result;
} // namespace accel
} // namespace accel
} // namespace daisi
