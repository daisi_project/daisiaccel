#include <fstream>

#include <boost/filesystem.hpp>

#include <common_tools/helpers.hpp>

#include "../../common/mad_safe_wrapper.h"
#include "beam_dynamics.h"
#include "madxcaller.h"
#include "tools.h"

namespace daisi
{
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(Solver_BeamDynamics, daisi::IModelComponent, "Beam_dynamics")

bool Solver_BeamDynamics::from_model_view(const std::string                         name,
                                          const std::map<std::string, std::string>& content)
{
    std::stringstream ss;

    ss << content.at("Engine");

    boost::property_tree::ptree pt;

    boost::property_tree::read_json(ss, pt);

    m_use_mad = false;

    m_use_mad = pt.get<bool>("MAD");

    ss.clear();

    ss << content.at("Parameters");
    boost::property_tree::read_json(ss, pt);

    m_n_particles = pt.get<int>("n_particles");

    ss.clear();

    ss << content.at("Distribution");
    boost::property_tree::read_json(ss, pt);

    m_distrType = 0;

    if (pt.get<bool>("Uniform"))
    {
        m_distrType = 1;
    }

    return true;
}

std::pair<std::array<std::vector<float>, 5>, std::string>
Solver_BeamDynamics::calc_using_mad(SolverResult& result, const int results_id,
                                    const std::shared_ptr<Flow>&     flow,
                                    const std::shared_ptr<Sequence>& sequence) const noexcept
{
    MadXCenterMass caller(results_id);

    std::pair<std::array<std::vector<float>, 5>, std::string> result_;

    const auto caller_result = caller.run_madx(results_id, flow, sequence);

    if (caller_result.first.empty())
    {
        result_.second = "Error creation madx caller ";
        return result_;
    }

    result.status = caller_result.second;

    result.text.emplace_back("MADX generated input", caller_result.first);

    auto mad_out_res_string = commtools::file_to_string(caller.get_file_path());

    result.text.emplace_back("MADX output", mad_out_res_string);

    auto tmp_res = loadcm(mad_out_res_string);

    if (tmp_res.size() != 1)
    {
        result_.second = "Error read MADX results file ";
        return result_;
    }

    auto& cm_ = tmp_res.begin()->second;

    auto& cm = result_.first;

    size_t i0 = 0;

    for (size_t i = 1; i < cm_[5].size(); i++)
    {
        for (size_t ii = i0; ii < cm_[5][i]; ii++)
        {
            for (size_t k = 0; k < 5; k++)
            {
                cm[k].push_back(cm_[k][i]);
            }
        }
        i0 = cm_[5][i];
    }

    return result_;
}

std::pair<std::vector<std::array<std::vector<float>, 5>>, std::string>
Solver_BeamDynamics::calc_using_daisi(const std::vector<arma::vec>     x0,
                                      const std::shared_ptr<Sequence>& sequence) const noexcept
{
    const auto Ms = sequence->get_matrixes();

    std::pair<std::vector<std::array<std::vector<float>, 5>>, std::string> result_;

    for (const auto& x0_cur : x0)
    {
        result_.first.push_back(calc_trace<float>(x0_cur, Ms));
    }

    return result_;
}

std::pair<std::vector<float>, std::vector<float>>
calculate_losses(const std::vector<std::array<std::vector<float>, 5>>& dyn,
                 const std::pair<double, double>&                      apertures)
{
    std::set<size_t> indexes;

    auto n_part = float(dyn.size());

    for (size_t i = 0; i < dyn.size(); i++)
    {
        indexes.insert(i);
    }

    std::pair<std::vector<float>, std::vector<float>> result;

    std::vector<std::vector<float>> transms(n_part);

    auto n_elem = dyn[0][0].size();

    result.second = dyn[0][4];
    result.first.resize(n_elem);
    std::vector<int> lo;

    for (size_t i = 0; i < n_part; i++)
    {

        transms[i].resize(n_elem);
        for (size_t j = 0; j < n_elem; j++)
        {
            if (std::pow(dyn[i][0][j] / apertures.first, 2.0) +
                    std::pow(dyn[i][2][j] / apertures.second, 2.0) <
                1.0)
            {
                transms[i][j] = 1.0;
            }
            else
            {
                lo.push_back(i);
                break;
            }
        }
    }
    for (size_t i = 0; i < n_elem; i++)
    {
        for (size_t j = 0; j < n_part; j++)
        {
            result.first[i] += transms[j][i];
        }
        result.first[i] = (n_part - result.first[i]) / n_part;
    }

    return result;
    //  std::iota(indexes.begin(), indexes.end(), 0);
}

SolverResult
Solver_BeamDynamics::run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                         const std::shared_ptr<Sequence>& sequence) const noexcept
{
    BL_FTRACE();
    SolverResult result;
    result.status = false;
    std::pair<std::vector<std::array<std::vector<float>, 5>>, std::string> result_;

    const auto x0 = flow->get_initial_beam_data(m_distrType, m_n_particles);

    if (m_use_mad)
    {
        // result_ = calc_using_mad(result, results_id, flow, sequence);
    }
    else
    {
        result_ = calc_using_daisi(x0, sequence);
    }

    if (!result_.second.empty())
    {
        BL_ERROR() << result_.second;
        return result;
    }

    const auto losses = calculate_losses(result_.first, flow->get_apertures());

    result.plots.emplace_back();
    result.plots[0].label = "X(Z)";

    result.plots.emplace_back();
    result.plots[1].label = "Y(Z)";

    for (const auto& trace : result_.first)
    {
        result.plots[0].XData.push_back(trace[4]);
        result.plots[0].YData.push_back(trace[0]);

        result.plots[1].XData.push_back(trace[4]);
        result.plots[1].YData.push_back(trace[2]);
    }

    result.plots.emplace_back();
    result.plots[2].label = "Losses";

    result.plots[2].XData.push_back(losses.second);
    result.plots[2].YData.push_back(losses.first);

    // result.write_cm_data(result_.first);

    BL_TRACE() << "Execution status: " << result.status;

    result.status = true;
    return result;
}
} // namespace accel
} // namespace daisi
