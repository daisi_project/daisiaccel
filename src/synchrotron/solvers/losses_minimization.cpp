#include <fstream>

#include <boost/filesystem.hpp>

#include <common_tools/helpers.hpp>
#include <notk/controller.hpp>

#include "../../common/mad_safe_wrapper.h"
#include "losses_minimization.h"
#include "tools.h"

namespace daisi
{
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(Solver_LossesMinimization, daisi::IModelComponent,
                            "Losses_minimization")

bool Solver_LossesMinimization::from_model_view(const std::string                         name,
                                                const std::map<std::string, std::string>& content)
{
    std::stringstream ss;

    m_notk = std::make_shared<notk::NOTKController<double, double>>();

    if (!m_notk->set_problem_config_str(content.at("NOTK config")))
    {
        throw std::runtime_error("Incorrect NOTK config");
    }

    ss << content.at("Engine");

    boost::property_tree::ptree pt;

    boost::property_tree::read_json(ss, pt);

    m_use_mad = false;

    m_use_mad = pt.get<bool>("MAD");

    ss.clear();

    ss << content.at("Parameters");
    boost::property_tree::read_json(ss, pt);

    m_n_particles = pt.get<int>("n_particles");

    m_min_angle = pt.get<double>("min_angle");
    m_max_angle = pt.get<double>("max_angle");

    ss.clear();

    ss << content.at("Distribution");
    boost::property_tree::read_json(ss, pt);

    m_distrType = 0;

    if (pt.get<bool>("Uniform"))
    {
        m_distrType = 1;
    }

    return true;
}

double calculate_losses(const std::vector<arma::vec> x0, const std::shared_ptr<Sequence>& sequence,
                        const std::pair<double, double>& apertures)
{
    auto   Ms     = sequence->get_matrixes();
    size_t losses = 0;

    double n_p = x0.size();

    for (const auto& x0_cur : x0)
    {
        auto res = calc_trace<float>(x0_cur, Ms);

        for (size_t j = 0; j < res[0].size(); j++)
        {
            if (std::pow(res[0][j] / apertures.first, 2.0) +
                    std::pow(res[2][j] / apertures.second, 2.0) >
                1.0)
            {
                losses++;
                break;
            }
        }
    }
    return losses / n_p;
}

SolverResult Solver_LossesMinimization::run_concrete_solver(
    const int results_id, const std::shared_ptr<Flow>& flow,
    const std::shared_ptr<Sequence>& sequence) const noexcept
{
    BL_FTRACE();
    SolverResult result;
    result.status = false;

    const auto x0 = flow->get_initial_beam_data(m_distrType, m_n_particles);

    const auto losses_old = calculate_losses(x0, sequence, flow->get_apertures());

    auto       seq_new    = std::make_shared<Sequence>(*sequence);
    const auto correctors = sequence->get_elements_list("KICKER");

    std::vector<double> left(2 * correctors.size());
    std::vector<double> right(2 * correctors.size());

    std::fill(left.begin(), left.end(), m_min_angle);
    std::fill(right.begin(), right.end(), m_max_angle);

    auto fitness = [&](const std::vector<double>& args) -> double {
        seq_new->set_corrector_angles(args);

        const auto losses = calculate_losses(x0, seq_new, flow->get_apertures());

        return losses;
    };

    if (!m_notk->set_borders_fitness(fitness, left, right, sequence->get_corrector_angles()))
    {
        return result;
    }

    bool flag_abort = true;

    auto result_notk = m_notk->process(flag_abort);

    if (!result_notk)
    {
        result.error_message = "Error NOTK calculation";
    }

    const auto cur_res  = result_notk->get_last_it_res().first;
    auto       fit_best = result_notk->get_fit_array<float>();

    seq_new->set_corrector_angles(cur_res);

    result.text.emplace_back("Corrected optic", seq_new->generate_mad_optic());

    auto generate_x_scale = [&](std::vector<float>& scale, const size_t size) {
        scale.resize(size);
        std::iota(scale.begin(), scale.end(), 0.0f);
    };

    result.plots.emplace_back();
    auto& plot1 = result.plots.back();
    plot1.label = "Losses";
    plot1.YData.push_back(fit_best);
    plot1.XData.emplace_back();

    generate_x_scale(plot1.XData.back(), plot1.YData.back().size());

    result.status = true;
    return result;
}
} // namespace accel
} // namespace daisi
