#ifndef DAISI_SYNC_SOLVET_MAD_TWISS_H
#define DAISI_SYNC_SOLVET_MAD_TWISS_H

#include "solver_madx.h"

namespace daisi
{
namespace accel
{
class Solver_Twiss final : public SolverSynchrotron<Solver_Twiss>
{
  public:
    BUILD_CHILD(Solver_Twiss, daisi::IModelComponent)

    bool from_model_view(const std::string                         name,
                         const std::map<std::string, std::string>& content) override final;

    SolverResult run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                     const std::shared_ptr<Sequence>& sequence) const
        noexcept override final;

  private:
    bool m_use_mad;

    std::pair<std::array<std::vector<float>, 7>, std::string>
    calc_using_daisi(const int results_id, const std::shared_ptr<Flow>& flow,
                     const std::shared_ptr<Sequence>& sequence) const noexcept;

    std::pair<std::array<std::vector<float>, 7>, std::string>
    calc_using_mad(SolverResult& result, const int results_id, const std::shared_ptr<Flow>& flow,
                   const std::shared_ptr<Sequence>& sequence) const noexcept;
};
} // namespace accel
} // namespace daisi
#endif
