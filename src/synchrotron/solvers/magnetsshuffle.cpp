#include <fstream>

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <common_tools/helpers.hpp>

#include "../../base/idatacommunicator.h"
#include "../../common/plot_tools.h"

#include "madxcaller.h"
#include "magnetsshuffle.h"
#include "tools.h"

namespace daisi
{
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(Solver_MagnetsShuffle, daisi::IModelComponent, "Magnets_shuffle")

bool Solver_MagnetsShuffle::from_model_view(const std::string                         name,
                                            const std::map<std::string, std::string>& content)
{
    std::stringstream ss;
    ss << content.at("");

    boost::property_tree::ptree pt;

    boost::property_tree::read_json(ss, pt);

    m_n_iteration = pt.get<int>("Number_of_iteration");

    return true;
}

SolverResult
Solver_MagnetsShuffle::run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                           const std::shared_ptr<Sequence>& sequence) const noexcept
{
    BL_FTRACE();
    SolverResult result;
    result.status = false;

    double r_min = 1e308;

    std::string best_shuffle;

    auto seq_new          = std::make_shared<Sequence>(*sequence);
    auto elements_shuffle = seq_new->get_elements_list("SBEND");
    auto seq_best         = std::make_shared<Sequence>(*sequence);

    MadXTwiss caller(results_id);

    const std::string mad_out = caller.get_file_path();

    std::vector<float> xms(m_n_iteration);
    std::vector<float> yms(m_n_iteration);

    for (int i = 0; i < m_n_iteration; i++)
    {
        BL_TRACE() << "MagnetsShuffle: run iteration: " << m_n_iteration;

        while (!IDataCommunicator::get().set_result_progress(
            results_id, i / static_cast<double>(m_n_iteration)))
            ;

        if (i > 0)
        {
            std::random_shuffle(elements_shuffle.begin(), elements_shuffle.end());
            seq_new->set_elements_list(elements_shuffle, "SBEND");
        }
        const auto caller_result = caller.run_madx(results_id, flow, seq_new);

        std::ifstream fS(mad_out, std::ifstream::in);

        float bx, by, xm, ym;

        fS >> bx;
        fS >> by;
        fS >> xm;
        fS >> ym;

        auto r = std::max(xm, ym);

        if (r < r_min)
        {
            r_min        = r;
            best_shuffle = caller_result.first;
            seq_best     = std::make_shared<Sequence>(*seq_new);
        }
        xms[i] = xm;
        yms[i] = ym;
    }

    result.status = true;
    result.text.emplace_back("MADX generated input for best shuffle", best_shuffle);

    int nCharts = 30;

    auto plot = [&](const std::vector<float>& data, const std::string& label) {
        auto chart      = getChart(data, nCharts, 0, -1);
        auto chart_line = convertChart(chart.first, chart.second);

        result.plots.emplace_back();
        auto& plot = result.plots.back();
        plot.label = label;
        plot.XData.push_back(chart_line.first);
        plot.YData.push_back(chart_line.second);
    };

    plot(xms, "X max");
    plot(yms, "Y max");

    const auto caller_result_best = caller.run_madx(results_id, flow, seq_best);

    result.text.emplace_back("MADX output",
                             commtools::file_to_string(caller.get_file_path_twiss()));

    result.status = true;
    return result;
}
} // namespace accel
} // namespace daisi
