#ifndef DAISI_MADXCALLER_H
#define DAISI_MADXCALLER_H

#include "../../common/flow.h"
#include "../opticsequence.h"

namespace daisi
{
namespace accel
{
class Solver_MADXCaller
{
  public:
    Solver_MADXCaller(const int results_id);
    virtual ~Solver_MADXCaller();
    std::pair<std::string, bool> run_madx(const int results_id, const std::shared_ptr<Flow>& flow,
                                          const std::shared_ptr<Sequence>& sequence) const noexcept;
    virtual std::string concrete_config(const int results_id, const std::shared_ptr<Flow>& flow,
                                        const std::shared_ptr<Sequence>& sequence) const
        noexcept = 0;

  protected:
    virtual void concrete_clean() const noexcept;

  private:
    void clean() const noexcept;

    const std::string m_mad_file_path;
    const std::string m_mad_out_all;
};

class MadXTwiss final : public Solver_MADXCaller
{
  public:
    MadXTwiss(const int results_id, int mode = 0);
    std::string concrete_config(const int results_id, const std::shared_ptr<Flow>& flow,
                                const std::shared_ptr<Sequence>& sequence) const
        noexcept override final;
    std::string get_file_path() const;
    std::string get_file_path_twiss() const;

  private:
    int  mode_;
    void concrete_clean() const noexcept override final;

    const std::string m_mad_result;
    const std::string m_mad_twiss;
};

class MadXCenterMass : public Solver_MADXCaller
{
  public:
    MadXCenterMass(const int results_id, const std::vector<std::string>& add_observers = {});
    std::string get_file_path() const;
    std::string concrete_config(const int results_id, const std::shared_ptr<Flow>& flow,
                                const std::shared_ptr<Sequence>& sequence) const
        noexcept override final;

  private:
    virtual void add_flow_data(std::stringstream&           mad_config,
                               const std::shared_ptr<Flow>& flow) const noexcept;

    const std::vector<std::string> m_add_observers;

    void concrete_clean() const noexcept override final;

    const std::string m_mad_out_cm;
};

class MadXCenterBeam final : public MadXCenterMass
{
  public:
    using MadXCenterMass::MadXCenterMass;

  private:
    virtual void add_flow_data(std::stringstream&           mad_config,
                               const std::shared_ptr<Flow>& flow) const noexcept;
};

} // namespace accel
} // namespace daisi
#endif
