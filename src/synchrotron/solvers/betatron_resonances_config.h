#ifndef DAISI_SYNC_BETATRON_RESONANCES_CONFIG_H
#define DAISI_SYNC_BETATRON_RESONANCES_CONFIG_H

#include <serreflection/defines.hpp>

namespace daisi
{
namespace accel
{
struct BetatronResonancesConfig
{
    int    number_of_circles;
    double x_amplitude_max;
    double y_amplitude_max;
    int    n_particles_x;
    int    n_particles_y;
};
} // namespace accel
} // namespace daisi

SERIALIZIBLE_STRUCT(daisi::accel::BetatronResonancesConfig, srfl::CheckModes::FATAL,
                    (int, number_of_circles, srfl::nan, -1e-200,
                     srfl::inf)(double, x_amplitude_max, srfl::nan, -1e-200,
                                srfl::inf)(double, y_amplitude_max, srfl::nan, -1e-200,
                                           srfl::inf)(int, n_particles_x, srfl::nan, -1e-200,
                                                      srfl::inf)(int, n_particles_y, srfl::nan,
                                                                 -1e-200, srfl::inf))

#endif
