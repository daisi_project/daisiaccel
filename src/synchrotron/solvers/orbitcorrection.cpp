#include <fstream>

#include <boost/filesystem.hpp>

#include <common_tools/helpers.hpp>

#include "orbitcorrection.h"
#include "tools.h"

namespace daisi
{
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(Solver_OrbitCorrection, daisi::IModelComponent, "Orbit_correction")

std::pair<arma::mat, arma::mat> CorrMatrix(const arma::vec&                 x0,
                                           const std::shared_ptr<Sequence>& seq)
{

    auto correctors = seq->get_elements_list("KICKER");
    auto monitors   = seq->get_elements_list("MONITOR");

    std::pair<arma::mat, arma::mat> result;

    auto& Rx = result.first;
    auto& Ry = result.second;

    const size_t k = correctors.size();
    const size_t p = monitors.size();

    double alpha = 1e-4;

    Rx = (arma::mat(p, k, arma::fill::zeros));
    Ry = (arma::mat(p, k, arma::fill::zeros));

    for (int j = 0; j < k; j++)
    {
        auto seq_new = std::make_shared<Sequence>(*seq);
        auto current_corr =
            std::dynamic_pointer_cast<Kicker>(seq_new->get_elements_list("KICKER")[j]);

        current_corr->HKICK += alpha;
        current_corr->VKICK += alpha;

        auto Ms = seq_new->get_matrixes("MONITOR");

        auto res = calc_trace<double>(x0, Ms);

        Rx.col(j) = arma::vec(res[0]);
        Ry.col(j) = arma::vec(res[2]);
    }

    return result;
}

bool Solver_OrbitCorrection::from_model_view(const std::string                         name,
                                             const std::map<std::string, std::string>& content)
{

    method = CorrMethod::SVD;

    Nkx = 1;
    Nky = 1;

    return true;
}

void invSVD(const arma::mat& R, const arma::mat& s, arma::mat& S)
{
    int p = R.n_rows;
    int k = R.n_cols;
    S     = (arma::mat(p, k, arma::fill::zeros));

    int l = std::min(p, k);
    for (int i = 0; i < l; i++)
    {
        if (s(i) != 0)
            S(i, i) = 1.0 / s(i);
    };
};

void Mikado(int flag, arma::mat& R, std::vector<int>& k1, int N, arma::mat& dx0,
            const std::shared_ptr<Sequence>& seq, const std::shared_ptr<Flow>& flow)
{
    auto correctors = seq->get_elements_list("KICKER");
    auto monitors   = seq->get_elements_list("MONITOR");

    const size_t k = correctors.size();
    const size_t p = monitors.size();

    const arma::vec x0 = flow->get_initial_data();
    arma::mat       R1;

    arma::mat U;
    arma::vec s;
    arma::mat V;
    arma::mat S;

    std::vector<int> numk(k);

    std::iota(numk.begin(), numk.end(), 0);

    for (int kk = 0; kk < N; kk++)
    {
        arma::mat Rt;
        arma::vec vdx(numk.size());

        for (int i = 0; i < numk.size(); i++)
        {
            Rt = arma::join_rows(R1, R.col(i));
            Rt.print();

            arma::svd(U, s, V, Rt);
            invSVD(Rt, s, S);
            arma::vec dI = V * arma::trans(S) * trans(U) * trans(dx0.row(0));

            auto seqj = std::make_shared<Sequence>(*seq);
            int  j;

            std::vector<int> kt = k1;
            kt.push_back(numk[i]);

            for (j = 0; j < dI.n_rows; j++)
            {
                auto current_corr =
                    std::dynamic_pointer_cast<Kicker>(seqj->get_elements_list("KICKER")[kt[j]]);
                switch (flag)
                {
                case 0:
                    current_corr->HKICK -= dI(j);
                    break;
                case 1:
                    current_corr->VKICK -= dI(j);
                    break;
                }
            }
            auto Ms = seqj->get_matrixes("MONITOR");

            auto res_ = calc_trace<double>(x0, Ms);

            arma::vec dx;

            switch (flag)
            {
            case 0:
                dx = arma::vec(res_[0]);
                break;
            case 1:
                dx = arma::vec(res_[2]);
                break;
            }

            vdx(i) = arma::sum(dx % dx);
        }

        int jx = vdx.index_min();
        R1     = arma::join_rows(R1, R.col(jx));
        k1.push_back(jx);
        R.shed_col(jx);
        numk.erase(numk.begin() + jx);
    }
    R = R1;
}

SolverResult
Solver_OrbitCorrection::run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                            const std::shared_ptr<Sequence>& sequence) const
    noexcept
{
    BL_FTRACE();
    SolverResult result;
    result.status = false;

    const arma::vec x0 = flow->get_initial_data();

    auto Rxy = CorrMatrix(x0, sequence);

    arma::mat& Rx = Rxy.first;
    arma::mat& Ry = Rxy.second;

    auto correctors = sequence->get_elements_list("KICKER");

    auto monitors = sequence->get_elements_list("MONITOR");

    const size_t p = monitors.size();

    const size_t k = correctors.size();

    arma::mat dx0 = (arma::mat(3, p, arma::fill::zeros));
    arma::mat dy0 = (arma::mat(3, p, arma::fill::zeros));

    auto Ms = sequence->get_matrixes("MONITOR");

    auto res  = calc_trace<double>(x0, Ms);
    auto resf = calc_trace<float>(x0, Ms);

    dx0.row(0) = arma::Row<double>(res[0]);
    dy0.row(0) = arma::Row<double>(res[2]);

    std::array<std::vector<float>, 5> res_;

    auto seq_new = std::make_shared<Sequence>(*sequence);

    switch (method)
    {
    case CorrMethod::MIKADO:
    {
        std::vector<int> numkx;
        std::vector<int> numky;

        Mikado(0, Rxy.first, numkx, Nkx, dx0, sequence, flow);
        Mikado(1, Rxy.second, numky, Nky, dy0, sequence, flow);

        arma::mat Ux;
        arma::vec sx;
        arma::mat Vx;

        arma::mat Uy;
        arma::vec sy;
        arma::mat Vy;

        arma::mat Sx1;
        arma::mat Sy1;

        arma::svd(Ux, sx, Vx, Rx);
        arma::svd(Uy, sy, Vy, Ry);

        invSVD(Rx, sx, Sx1);
        invSVD(Ry, sy, Sy1);

        arma::vec dIx = Vx * arma::trans(Sx1) * arma::trans(Ux) * arma::trans(dx0.row(0));
        arma::vec dIy = Vy * arma::trans(Sy1) * arma::trans(Uy) * arma::trans(dy0.row(0));

        dIx.print();
        dIy.print();

        for (int j = 0; j < dIx.n_rows; j++)
        {
            auto current_corr =
                std::dynamic_pointer_cast<Kicker>(seq_new->get_elements_list("KICKER")[numkx[j]]);

            current_corr->HKICK -= dIx(j);
        }
        for (int j = 0; j < dIy.n_rows; j++)
        {
            auto current_corr =
                std::dynamic_pointer_cast<Kicker>(seq_new->get_elements_list("KICKER")[numky[j]]);

            current_corr->VKICK -= dIy(j);
        }
    }
    break;
    case CorrMethod::SVD:

        arma::mat Ux;
        arma::vec sx;
        arma::mat Vx;

        arma::mat Uy;
        arma::vec sy;
        arma::mat Vy;

        arma::mat Sx1;
        arma::mat Sy1;

        arma::svd(Ux, sx, Vx, Rx);
        arma::svd(Uy, sy, Vy, Ry);

        invSVD(Rx, sx, Sx1);
        invSVD(Ry, sy, Sy1);

        arma::vec dIx = Vx * arma::trans(Sx1) * arma::trans(Ux) * arma::trans(dx0.row(0));
        arma::vec dIy = Vy * arma::trans(Sy1) * arma::trans(Uy) * arma::trans(dy0.row(0));

        for (int j = 0; j < k; j++)
        {
            auto current_corr =
                std::dynamic_pointer_cast<Kicker>(seq_new->get_elements_list("KICKER")[j]);

            current_corr->HKICK -= 1e-4 * dIx(j);
            current_corr->VKICK -= 1e-4 * dIy(j);
        }

        break;
    }

    auto Ms_ = seq_new->get_matrixes("MONITOR");

    result.text.emplace_back("Corrected optic", seq_new->generate_mad_optic());

    res_ = calc_trace<float>(x0, Ms_);

    // res_ = resf;

    result.plots.emplace_back();
    auto& plot1 = result.plots.back();
    plot1.label = "X(Z)";
    plot1.XData.push_back(res_[4]);
    plot1.XData.push_back(res_[4]);
    plot1.YData.push_back(resf[0]);
    plot1.YData.push_back(res_[0]);

    result.plots.emplace_back();
    auto& plot2 = result.plots.back();
    plot2.label = "Y(Z)";
    plot2.XData.push_back(res_[4]);
    plot2.XData.push_back(res_[4]);
    plot2.YData.push_back(resf[2]);
    plot2.YData.push_back(res_[2]);

    result.status = true;
    return result;
}
} // namespace accel
} // namespace daisi
