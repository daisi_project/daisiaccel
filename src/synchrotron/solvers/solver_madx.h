#ifndef DAISI_SYNC_MAIN_SOLVER_MADX_H
#define DAISI_SYNC_MAIN_SOLVER_MADX_H

#include "solver.h"

namespace daisi
{

namespace accel
{
template <class T>
class SolverSynchrotronMADX : public SolverSynchrotron<T>
{
  public:
    virtual SolverResult run_concrete_solver(const int                        results_id,
                                             const std::shared_ptr<Flow>&     flow,
                                             const std::shared_ptr<Sequence>& sequence) const
        noexcept
    {
        BL_FTRACE();

        SolverResult result;
        result.status = false;

        return result;
    }
};
}
}

#endif
