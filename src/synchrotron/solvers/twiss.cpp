#include <fstream>

#include <boost/filesystem.hpp>

#include <common_tools/helpers.hpp>

#include "../../common/mad_safe_wrapper.h"
#include "madxcaller.h"
#include "tools.h"
#include "twiss.h"

namespace daisi
{
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(Solver_Twiss, daisi::IModelComponent, "Twiss")

bool Solver_Twiss::from_model_view(const std::string                         name,
                                   const std::map<std::string, std::string>& content)
{
    std::stringstream ss;

    ss << content.at("Engine");

    boost::property_tree::ptree pt;

    boost::property_tree::read_json(ss, pt);

    m_use_mad = false;

    m_use_mad = pt.get<bool>("MAD");

    return true;
}

std::pair<std::array<std::vector<float>, 7>, std::string>
Solver_Twiss::calc_using_mad(SolverResult& result, const int results_id,
                             const std::shared_ptr<Flow>&     flow,
                             const std::shared_ptr<Sequence>& sequence) const noexcept
{
    MadXTwiss caller(results_id, 1);

    std::pair<std::array<std::vector<float>, 7>, std::string> result_;

    const auto caller_result = caller.run_madx(results_id, flow, sequence);

    if (caller_result.first.empty())
    {
        result_.second = "Error creation madx caller ";
        return result_;
    }

    result.status = caller_result.second;

    auto mad_out_res_string = commtools::file_to_string(caller.get_file_path_twiss());

    auto data = loadtw(mad_out_res_string);

    for (const auto& val : data)
    {
        for (size_t i = 0; i < 7; i++)
        {
            result_.first[i].push_back(val.second[i]);
        }
    }
    return result_;
}

std::pair<std::array<std::vector<float>, 7>, std::string>
Solver_Twiss::calc_using_daisi(const int results_id, const std::shared_ptr<Flow>& flow,
                               const std::shared_ptr<Sequence>& sequence) const noexcept
{
}
// 3.5+0.47+0.265+0.16+0.16
SolverResult Solver_Twiss::run_concrete_solver(const int                        results_id,
                                               const std::shared_ptr<Flow>&     flow,
                                               const std::shared_ptr<Sequence>& sequence) const
    noexcept
{
    BL_FTRACE();
    SolverResult result;
    result.status = false;
    std::pair<std::array<std::vector<float>, 7>, std::string> result_;

    if (m_use_mad)
    {
        result_ = calc_using_mad(result, results_id, flow, sequence);
    }
    else
    {
        result_ = calc_using_daisi(results_id, flow, sequence);
    }

    if (!result_.second.empty())
    {
        BL_ERROR() << result_.second;
        return result;
    }
    auto& tw = result_.first;

    result.plots.emplace_back();
    auto& plot1 = result.plots.back();
    plot1.label = "Alpha";
    plot1.XData.push_back(tw[0]);
    plot1.XData.push_back(tw[0]);
    plot1.YData.push_back(tw[1]);
    plot1.YData.push_back(tw[4]);

    result.plots.emplace_back();
    auto& plot2 = result.plots.back();
    plot2.label = "Beta";
    plot2.XData.push_back(tw[0]);
    plot2.XData.push_back(tw[0]);
    plot2.YData.push_back(tw[2]);
    plot2.YData.push_back(tw[5]);

    result.plots.emplace_back();
    auto& plot3 = result.plots.back();
    plot3.label = "Mu";
    plot3.XData.push_back(tw[0]);
    plot3.XData.push_back(tw[0]);
    plot3.YData.push_back(tw[3]);
    plot3.YData.push_back(tw[6]);

    result.status = true;
    return result;
}
} // namespace accel
} // namespace daisi
