#include <armadillo>

#include "fourier_common.h"

namespace daisi
{
namespace accel
{
SolverResult fourier_errors(const std::vector<double>& err_in, const unsigned n_members,
                            const std::string& postfix)
{
    SolverResult result;
    result.status = false;

    arma::vec err = err_in;

    arma::cx_vec err_fft = arma::fft(err);

    arma::vec fft_abs = arma::abs(err_fft);
    arma::vec fft_ph  = arma::arg(err_fft);

    std::vector<float>  x_data(err.n_elem);
    std::vector<double> x_data_d(err.n_elem);

    std::iota(x_data.begin(), x_data.end(), 0);
    std::iota(x_data_d.begin(), x_data_d.end(), 0);

    const double T = err.n_elem;

    arma::vec t_data = x_data_d;
    t_data           = t_data - T / 2.0;

    const double omega = 2 * M_PI / T;

    const double a0 = 1.0 / T * arma::sum(err);

    std::stringstream series;

    series << "F(t) = a0+sum_{k=1}^{" << n_members << "}A_k * cos(k*omega*t+Psi_k)\n";

    series << "a0 = " << a0;
    series << ", omega = " << omega << "\n";

    for (size_t k = 1; k < n_members; k++)
    {
        const double ak = arma::sum(err % arma::cos(double(k) * omega * t_data)) * (2.0 / T);
        const double bk = arma::sum(err % arma::sin(double(k) * omega * t_data)) * (2.0 / T);
        series << "A_" << k << "= " << std::sqrt(ak * ak + bk * bk);
        series << ", Psi_" << k << " = " << -std::atan(bk / ak) << "\n";
    }

    result.plots.emplace_back();
    auto& plot1 = result.plots.back();
    plot1.label = "Amplitudes, " + postfix;
    plot1.YData.push_back(std::vector<float>(fft_abs.begin(), fft_abs.end()));
    plot1.XData.push_back(x_data);

    result.plots.emplace_back();
    auto& plot2 = result.plots.back();
    plot2.label = "Phases, " + postfix;
    plot2.YData.push_back(std::vector<float>(fft_ph.begin(), fft_ph.end()));
    plot2.XData.push_back(x_data);

    result.plots.emplace_back();
    auto& plot3 = result.plots.back();
    plot3.label = postfix;
    plot3.YData.push_back(std::vector<float>(err_in.begin(), err_in.end()));
    plot3.XData.push_back(x_data);

    result.text.emplace_back("Fourier series coeffs, " + postfix, series.str());

    result.status = true;
    return result;
}
} // namespace accel
} // namespace daisi
