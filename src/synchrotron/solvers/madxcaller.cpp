
#include <boost/filesystem.hpp>

#include "../../common/mad_safe_wrapper.h"
#include "madxcaller.h"

namespace daisi
{
namespace accel
{

Solver_MADXCaller::Solver_MADXCaller(const int results_id)
    : m_mad_file_path("mad.config_" + std::to_string(results_id)),
      m_mad_out_all("mad_out_all_" + std::to_string(results_id))
{
}

Solver_MADXCaller::~Solver_MADXCaller()
{
    clean();
}

void Solver_MADXCaller::clean() const noexcept
{
    if (boost::filesystem::exists(m_mad_file_path))
    {
        boost::filesystem::remove(m_mad_file_path);
    }
    if (boost::filesystem::exists(m_mad_out_all))
    {
        boost::filesystem::remove(m_mad_out_all);
    }
    concrete_clean();
}

void Solver_MADXCaller::concrete_clean() const noexcept
{
}

std::pair<std::string, bool>
Solver_MADXCaller::run_madx(const int results_id, const std::shared_ptr<Flow>& flow,
                            const std::shared_ptr<Sequence>& sequence) const noexcept

{
    clean();

    std::stringstream mad_config;

    mad_config << "ASSIGN, ECHO=" + m_mad_out_all + ";\n\n";

    mad_config << flow->to_madx()[0];

    mad_config << sequence->generate_mad_optic();
    mad_config << sequence->generate_mad_line();

    mad_config << concrete_config(results_id, flow, sequence);

    try
    {
        std::ofstream mad_file(m_mad_file_path);

        mad_file << mad_config.str();
    }
    catch (const std::exception& ex)
    {
        BL_ERROR() << "Error write MADX config file: " << ex.what();
        return std::make_pair(std::string(""), false);
    }

    MADXSafeWrapper wr(m_mad_file_path);

    return std::make_pair(mad_config.str(), wr.get_result());
}

MadXTwiss::MadXTwiss(const int results_id, int mode)
    : Solver_MADXCaller(results_id), m_mad_result("data_" + std::to_string(results_id)),
      m_mad_twiss("data_twiss_" + std::to_string(results_id)), mode_(mode)
{
}

void MadXTwiss::concrete_clean() const noexcept
{
    if (boost::filesystem::exists(m_mad_result))
    {
        boost::filesystem::remove(m_mad_result);
    }
    if (boost::filesystem::exists(m_mad_twiss))
    {
        boost::filesystem::remove(m_mad_twiss);
    }
}

std::string MadXTwiss::get_file_path_twiss() const
{
    return m_mad_twiss;
}

std::string MadXTwiss::concrete_config(const int results_id, const std::shared_ptr<Flow>& flow,
                                       const std::shared_ptr<Sequence>& sequence) const noexcept
{
    std::stringstream mad_config;

    if (mode_ == 1)
    {
        auto res = flow->get_twiss_vector();

        mad_config << "initial: BETA0, BETX=" << res[0] << ", ALFX=" << res[1] << ", MUX=" << 0
                   << ", BETY=" << res[3] << ", ALFY=" << res[4] << ", MUY=" << 0 << ";\n";
        mad_config << "twiss, BETA0 = initial, save, file = " << m_mad_twiss << ";\n";
    }

    if (mode_ == 0)
    {
        mad_config << "TWISS, BETA0=initial; chrom = true, table = twiss_data, save, file = "
                   << m_mad_twiss << ";\n";
        mad_config << "bmx = TABLE(summ, BETXMAX);\n";
        mad_config << "bmy = TABLE(summ, BETYMAX);\n";
        mad_config << "xm = TABLE(summ, XCOMAX);\n";
        mad_config << "ym = TABLE(summ, YCOMAX);\n";
        mad_config << "ASSIGN, ECHO = \"" << m_mad_result << "\";\n";

        mad_config << "PRINTF, TEXT = \"%f %f %f %f\", VALUE = bmx, bmy, xm, ym;\n";
        mad_config << "ASSIGN, ECHO = \"Trash.txt\";\n";
    }

    return mad_config.str();
}

std::string MadXTwiss::get_file_path() const
{
    return m_mad_result;
}

MadXCenterMass::MadXCenterMass(const int results_id, const std::vector<std::string>& add_observers)
    : Solver_MADXCaller(results_id), m_mad_out_cm("mad_out_cm_" + std::to_string(results_id)),
      m_add_observers(add_observers)
{
}

std::string MadXCenterMass::get_file_path() const
{
    return m_mad_out_cm + "one";
}
std::string MadXCenterMass::concrete_config(const int results_id, const std::shared_ptr<Flow>& flow,
                                            const std::shared_ptr<Sequence>& sequence) const
    noexcept
{
    std::stringstream mad_config;

    mad_config << "ptc_create_universe;\nptc_create_layout, model = 2, method = 6, "
                  "nst = 10, exact = false; \n";

    mad_config << "ptc_start, " << flow->to_madx()[1] << "\n";

    add_flow_data(mad_config, flow);

    mad_config << sequence->generate_obs_commands(m_add_observers) << "\n";

    mad_config << "ptc_track, icase = 6, ";

    mad_config << "FILE = " + m_mad_out_cm +
                      ", element_by_element, "
                      "dump = true, onetable = true, turns = "
               << flow->get_n_circles()
               << ", ffile = 1, \n		norm_no = "
                  "1;\nptc_track_end;\nptc_end;";

    return mad_config.str();
}

void MadXCenterMass::concrete_clean() const noexcept
{
    if (boost::filesystem::exists(m_mad_out_cm))
    {
        boost::filesystem::remove(m_mad_out_cm);
    }
}
void MadXCenterMass::add_flow_data(std::stringstream&           mad_config,
                                   const std::shared_ptr<Flow>& flow) const noexcept
{
}
void MadXCenterBeam::add_flow_data(std::stringstream&           mad_config,
                                   const std::shared_ptr<Flow>& flow) const noexcept
{
    mad_config << flow->to_madx_beam();
}

} // namespace accel
} // namespace daisi
