#ifndef DAISI_SYNC_SOLVET_ORBIT_CORRECTION_OPTIMIZATION_H
#define DAISI_SYNC_SOLVET_ORBIT_CORRECTION_OPTIMIZATION_H

#include "solver_madx.h"

namespace notk
{
template <class Targ, class Tfit>
class NOTKController;
}

namespace daisi
{
namespace accel
{
class Solver_OrbitCorrectionOptimization final
    : public SolverSynchrotron<Solver_OrbitCorrectionOptimization>
{
  public:
    BUILD_CHILD(Solver_OrbitCorrectionOptimization, daisi::IModelComponent)

    bool from_model_view(const std::string                         name,
                         const std::map<std::string, std::string>& content) override final;

    SolverResult run_concrete_solver(const int results_id, const std::shared_ptr<Flow>& flow,
                                     const std::shared_ptr<Sequence>& sequence) const
        noexcept override final;

  private:
    double m_min_angle;
    double m_max_angle;

    std::shared_ptr<notk::NOTKController<double, double>> m_notk;
};
} // namespace accel
} // namespace daisi
#endif
