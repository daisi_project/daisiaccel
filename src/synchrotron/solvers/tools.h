#ifndef DAISI_SYNC_TOOLS_H
#define DAISI_SYNC_TOOLS_H

#include <armadillo>
#include <list>
#include <string>

namespace daisi
{
namespace accel
{
struct TransportMatrix;

std::map<int, std::array<std::vector<float>, 6>> loadcm(const std::string& content);

std::vector<std::pair<std::string, std::array<double, 7>>> loadtw(const std::string& content);

template <class T>
std::array<std::vector<T>, 5> calc_trace(const arma::vec&                  x0,
                                         const std::list<TransportMatrix>& matrixes);
} // namespace accel
} // namespace daisi

#endif
