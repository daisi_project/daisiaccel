#ifndef DAISIACCEL_OPTIC_ELEMENTS_H
#define DAISIACCEL_OPTIC_ELEMENTS_H

#include <list>

#include <armadillo>

#include <common_tools/child_factory.hpp>
#include <serreflection/defines.hpp>
#include <serreflection/read_json.hpp>
#include <serreflection/write_json.hpp>

#include "opticnames.h"

namespace daisi
{
namespace accel
{
struct IOpticElement
{
    virtual std::unique_ptr<boost::property_tree::ptree> to_json() const noexcept = 0;
    virtual bool from_json(const boost::property_tree::ptree& pt) noexcept        = 0;

    virtual std::list<std::string> get_madx_required() const noexcept = 0;

    virtual ~IOpticElement();

    virtual std::string get_label() const noexcept = 0;

    virtual std::string to_madx() const noexcept     = 0;
    virtual std::string to_madx_err() const noexcept = 0;

    virtual void insert_field_error(const double err) noexcept = 0;

    virtual void set_label(const std::string& label_in) noexcept = 0;

    virtual double get_L() noexcept = 0;

    virtual std::pair<arma::mat, arma::vec> calc_transport_matrix() const noexcept = 0;

    BUILD_CHILD_FACTORY(std::string, IOpticElement)
};

template <class T>
struct BaseOpticElement : IOpticElement
{
    double      L;
    std::string label;

    virtual void insert_field_error(const double err) noexcept override
    {
    }

    virtual double get_L() noexcept override final
    {
        return L;
    }

    void set_label(const std::string& label_in) noexcept override final
    {
        label = label_in;
    }

    std::string get_label() const noexcept override final
    {
        return label;
    }

    virtual std::string to_madx() const noexcept override
    {
        return to_madx_base() + to_madx_concrete() + ";\n";
    }

    std::string to_madx_err() const noexcept override final
    {
        return to_madx_err_concrete();
    }

    virtual std::string to_madx_err_concrete() const noexcept
    {
        return "";
    }

    std::string to_madx_base() const noexcept
    {
        std::stringstream result;
        result << std::setprecision(16) << label << ": " << this->get_key() << ", L= " << L;
        return result.str();
    }

    virtual std::string to_madx_concrete() const noexcept
    {
        return "";
    }

    virtual std::pair<arma::mat, arma::vec> calc_transport_matrix() const noexcept override
    {
        std::pair<arma::mat, arma::vec> result;
        result.first.zeros(5, 5);

        auto& Tm = result.first;

        result.second.zeros(5);

        for (size_t d = 0; d < 5; d++)
            Tm(d, d) = 1;

        Tm(0, 1) = L;
        Tm(2, 3) = L;

        return result;
    }

    std::unique_ptr<boost::property_tree::ptree> to_json() const noexcept override final
    {
        return srfl::write_json_pt(*(static_cast<const T*>(this)));
    }

    bool from_json(const boost::property_tree::ptree& pt) noexcept override final
    {
        return srfl::read_json(pt, *(static_cast<T*>(this)));
    }

    virtual std::list<std::string> get_madx_required() const noexcept override
    {
        return {};
    }
};

template <class T>
struct CommonOptic : BaseOpticElement<T>
{
    double z_err;
    double x_err;
    double y_err;
    double xy_err;
    double yz_err;
    double xz_err;
    double L_err;

    std::string to_common_optic() const noexcept
    {
        std::stringstream result;
        // result << std::setprecision(16) << label << ": " << this->get_key() << ", L= " << L;
        return result.str();
    }
};

struct Drift final : BaseOpticElement<Drift>
{
    BUILD_CHILD(Drift, IOpticElement)
    std::list<std::string> get_madx_required() const noexcept override final;
};

struct Sextupole final : BaseOpticElement<Sextupole>
{
    double      K2;
    std::string to_madx_concrete() const noexcept override
    {
        std::stringstream result;

        result << std::setprecision(16) << ", K2=" << K2;

        return result.str();
    }

    BUILD_CHILD(Sextupole, IOpticElement)
    std::list<std::string> get_madx_required() const noexcept override final;
};

struct Kicker final : BaseOpticElement<Kicker>
{
    double HKICK;
    double VKICK;
    BUILD_CHILD(Kicker, IOpticElement)
    std::list<std::string> get_madx_required() const noexcept override final;

    std::string to_madx_concrete() const noexcept override
    {
        std::stringstream result;

        result << std::setprecision(16) << ", HKICK=" << HKICK << ", VKICK=" << VKICK;

        return result.str();
    }
    std::pair<arma::mat, arma::vec> calc_transport_matrix() const noexcept override final;
};

struct Monitor final : BaseOpticElement<Monitor>
{
    BUILD_CHILD(Monitor, IOpticElement)
};

template <class T>
struct Magnet : CommonOptic<T>
{
    double ANGLE;
    double E1;
    double E2;
    double sigma_b_err;

    void insert_field_error(const double err) noexcept override final
    {
        sigma_b_err = err;
    }

    std::string to_madx_concrete() const noexcept override
    {
        std::stringstream result;

        result << std::setprecision(16) << ", ANGLE=" << ANGLE << ", E1=" << E1 << ", E2=" << E2;

        return result.str();
    }

    std::string to_madx_err_concrete() const noexcept override final
    {
        std::stringstream result;

        result << std::setprecision(16) << "SELECT, FLAG=ERROR, PATTERN = \"" << this->label
               << "\";\n";
        // TODO remode hardcoded radius
        result << "EFCOMP, order = 0, radius = 0.030, DKNR: = {" << sigma_b_err << ", 0, 0, 0 };\n";

        return result.str();
    }

    std::list<std::string> get_madx_required() const noexcept override final
    {
        return {"ANGLE", "L", "E1", "E2"};
    }
};

struct Quadrupole final : CommonOptic<Quadrupole>
{
    std::string to_madx_concrete() const noexcept override
    {
        std::stringstream result;

        result << std::setprecision(16) << ", K1=" << K1;

        return result.str();
    }

    double K1;
    double sigma_k_err;
    BUILD_CHILD(Quadrupole, IOpticElement)
    std::list<std::string>          get_madx_required() const noexcept override final;
    std::pair<arma::mat, arma::vec> calc_transport_matrix() const noexcept override final;
};

struct Rbend final : Magnet<Rbend>
{
    BUILD_CHILD(Rbend, IOpticElement)
    std::pair<arma::mat, arma::vec> calc_transport_matrix() const noexcept override final;
};

struct Sbend final : Magnet<Sbend>
{
    BUILD_CHILD(Sbend, IOpticElement)
    std::pair<arma::mat, arma::vec> calc_transport_matrix() const noexcept override final;
};
} // namespace accel
} // namespace daisi

#define SER_BASE_OPTIC() (double, L, srfl::nan, -1e-200, srfl::inf)(std::string, label, DEF_D())

#define SER_BASE_COMMON_OPTIC()                                                                    \
    (double, z_err, DEF_D())(double, x_err, DEF_D())(double, y_err, DEF_D())(                      \
        double, xy_err, DEF_D())(double, yz_err, DEF_D())(double, xz_err, DEF_D())(double, L_err,  \
                                                                                   DEF_D())

#define SER_BASE_MAGNET()                                                                          \
    (double, ANGLE, DEF_D())(double, E1, DEF_D())(double, E2, DEF_D())(double, sigma_b_err, DEF_D())

SERIALIZIBLE_STRUCT(daisi::accel::Sextupole, srfl::CheckModes::FATAL,
                    SER_BASE_OPTIC()(double, K2, DEF_D()))

SERIALIZIBLE_STRUCT(daisi::accel::Drift, srfl::CheckModes::FATAL, SER_BASE_OPTIC())

SERIALIZIBLE_STRUCT(daisi::accel::Kicker, srfl::CheckModes::FATAL,
                    SER_BASE_OPTIC()(double, HKICK, DEF_D())(double, VKICK, DEF_D()))

SERIALIZIBLE_STRUCT(daisi::accel::Monitor, srfl::CheckModes::FATAL, SER_BASE_OPTIC())

SERIALIZIBLE_STRUCT(daisi::accel::Rbend, srfl::CheckModes::FATAL,
                    SER_BASE_OPTIC() SER_BASE_COMMON_OPTIC() SER_BASE_MAGNET())

SERIALIZIBLE_STRUCT(daisi::accel::Sbend, srfl::CheckModes::FATAL,
                    SER_BASE_OPTIC() SER_BASE_COMMON_OPTIC() SER_BASE_MAGNET())

SERIALIZIBLE_STRUCT(daisi::accel::Quadrupole, srfl::CheckModes::FATAL,
                    SER_BASE_OPTIC()
                        SER_BASE_COMMON_OPTIC()(double, K1, DEF_D())(double, sigma_k_err, DEF_D()))

#endif
