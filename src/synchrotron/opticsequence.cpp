#include <common_tools/algorithm.hpp>
#include <serreflection/read_json.hpp>

#include "../base/componentnames.h"
#include "../base/imodelcomponentview.h"
#include "../common/common_tools.h"
#include "../common/defines.h"
#include "../common/keywords.h"
#include "../common/viewitems.h"

#include "opticsequence.h"

namespace daisi
{
namespace compnames
{
const std::string Sequence = "Optic_elements_sequence";
}
namespace accel
{

Sequence::Sequence(const Sequence& obj) : m_period(obj.m_period)
{
    for (const auto& val : obj.m_elements)
    {
        m_elements.emplace_back(val->clone());
    }
}

std::shared_ptr<IOpticElement>
Sequence::find_element(const std::map<std::string, std::shared_ptr<IOpticElement>>& elemnts_to_find,
                       const std::string&                                           key) noexcept
{
    auto it = elemnts_to_find.find(key);

    if (it == elemnts_to_find.end())
    {
        return nullptr;
    }
    return it->second;
}

int Sequence::get_element_number_by_label(const std::string& label) const noexcept
{
    auto it = std::find_if(m_elements.begin(), m_elements.end(),
                           [&](const auto& val) { return val->get_label() == label; });

    return it == m_elements.end() ? -1 : std::distance(m_elements.begin(), it);
}

std::vector<std::shared_ptr<IOpticElement>>
Sequence::get_elements_list(const std::string& type) const noexcept
{
    std::vector<std::shared_ptr<IOpticElement>> results;

    for (const auto& val : m_elements)
    {
        if (val->get_key() == type)
        {
            results.push_back(val);
        }
    }
    return results;
}

std::vector<double> Sequence::get_corrector_angles() const noexcept
{
    std::vector<double> result;
    for (const auto& val : m_elements)
    {
        if (val->get_key() == "KICKER")
        {
            auto corrector = std::dynamic_pointer_cast<Kicker>(val);
            result.push_back(corrector->HKICK);
            result.push_back(corrector->VKICK);
        }
    }
    return result;
}

std::vector<double> Sequence::get_errors(const std::string& type) const noexcept
{
    std::vector<double> errors;
    auto                magnets = get_elements_list(type);

    for (const auto el : magnets)
    {
        if (type == "SBEND")
        {
            auto sbend = std::dynamic_pointer_cast<Sbend>(el);

            errors.push_back(sbend->sigma_b_err);
        }
    }

    return errors;
}

void Sequence::set_corrector_angles(const std::vector<double>& angles) noexcept
{
    size_t i = 0;
    for (const auto& val : m_elements)
    {
        if (val->get_key() == "KICKER")
        {
            auto corrector = std::dynamic_pointer_cast<Kicker>(val);
            if (i < angles.size())
            {
                corrector->HKICK = angles[i];
            }
            i++;
            if (i < angles.size())
            {
                corrector->VKICK = angles[i];
            }
            i++;
        }
    }
}

void Sequence::set_elements_list(const std::vector<std::shared_ptr<IOpticElement>>& elements,
                                 const std::string&                                 type) noexcept
{
    auto it_in = elements.begin();
    for (auto& val : m_elements)
    {
        if (val->get_key() == type)
        {
            if (it_in < elements.end())
            {
                val = *it_in;
            }
            it_in++;
        }
    }
}

std::list<std::string> period_content(const std::string& fail_msg, const std::string& content,
                                      const std::string& period)
{
    auto period_str = extract_cmd_data(fail_msg, content, period + ": LINE=(", ")", false);

    return commtools::strsplit_adapter<std::list<std::string>>(period_str.first,
                                                               std::string(": ,;=, l"));
}

std::list<std::string> extract(const std::string& fail_msg, const std::string& content,
                               const std::string& period)
{
    std::list<std::string> result;
    for (const auto& val : period_content(fail_msg, content, period))
    {

        auto tmp = extract(fail_msg, content, val);
        if (tmp.empty())
        {
            result.push_back(val);
        }
        else
        {
            result.insert(result.end(), tmp.begin(), tmp.end());
        }
    }
    return result;
}

REGISTER_CHILD_NON_TEMPLATE(Sequence, daisi::IModelComponent, daisi::compnames::Sequence)

Sequence::Sequence() = default;

Sequence::Sequence(Sequence&&) = default;

std::shared_ptr<IModelComponentView> Sequence::to_model_view(const std::string& name,
                                                             const std::string& type) const
{
    BL_FTRACE();

    // auto result = IModelComponentView::make_child(type);

    // if (!result)
    // {
    //     BL_ERROR() << "unexpected model component view type: " << type;
    //     return nullptr;
    // }

    // commtools::ServiceLocator<IModelComponentViewItem> items;

    // auto optic   = std::make_shared<daisi::TextArea>();
    // optic->label = "optic";

    // items.set(optic, "optic");

    // return result;
}

std::string Sequence::generate_mad_optic() const noexcept
{
    std::string result;

    std::set<std::string> descripts;

    for (const auto& el : m_elements)
    {
        const auto curr = el->to_madx();

        if (descripts.find(curr) == descripts.end())
        {
            descripts.insert(curr);
            result += curr;
        }
    }
    result += "\n";

    return result;
}

std::string Sequence::generate_mad_line() const noexcept
{
    std::string result = m_period + ": LINE=(";

    size_t i = 0;

    for (const auto& el : m_elements)
    {
        if (0 != i)
        {
            result += ", ";
        }
        i++;
        result += el->get_label();
    }

    result += ");\n\n";

    result += "USE, period = " + m_period + ";\n\n";

    for (const auto& el : m_elements)
    {
        result += el->to_madx_err();
    }
    result += "\n";

    return result;
}

std::string Sequence::generate_obs_commands(const std::vector<std::string>& mons) const noexcept
{
    std::string result;

    if (mons.empty())
    {
        for (const auto& el : m_elements)
        {
            result += "ptc_observe, place = " + el->get_label() + ";\n";
        }
    }
    else
    {
        for (const auto& el : m_elements)
        {
            for (const auto& el_in : mons)
            {
                if (el->get_label() == el_in)
                {
                    result += "ptc_observe, place = " + el->get_label() + ";\n";
                }
            }
        }
    }

    return result;
}

std::list<TransportMatrix> Sequence::get_matrixes(const std::string& type) const noexcept
{
    std::list<TransportMatrix> result;
    // result.emplace_back();
    // result.back().M.zeros(5, 5);

    // for (size_t i = 0; i < 5; i++)
    // {
    //     result.back().M(i, i) = 1;
    // }

    // result.back().B.zeros(5);
    // result.back().S = 0;

    // auto M_prev = result.back().M;
    // auto B_prev = result.back().B;
    // auto S_prev = result.back().S;

    arma::mat M_prev;
    arma::vec B_prev;
    double    S_prev;
    M_prev.zeros(5, 5);

    for (size_t i = 0; i < 5; i++)
    {
        M_prev(i, i) = 1;
    }
    B_prev.zeros(5);
    S_prev = 0;

    for (const auto& el : m_elements)
    {

        const auto MB = el->calc_transport_matrix();

        M_prev = MB.first * M_prev;
        B_prev = MB.first * B_prev + MB.second;
        S_prev += el->get_L();
        // std::cout << "_____________" << std::endl;

        // M_prev.print();

        if (type.empty() || el->get_key() == type)
        {
            result.emplace_back(M_prev, B_prev, S_prev);
        }
    }

    return result;
}

bool Sequence::from_model_view(const std::string                         name,
                               const std::map<std::string, std::string>& content)
{
    BL_FTRACE();

    const std::string fail_msg = "failed to load sequence from view " + name;

    auto optic  = this->extract_content(fail_msg, content, "optic");
    auto line   = this->extract_content(fail_msg, content, "line");
    auto errors = this->extract_content(fail_msg, content, "errors");

    if (!optic.first || !line.first || !errors.first)
    {
        return false;
    }

    auto period = extract_cmd_data(fail_msg, line.second, "USE, period=", ";");
    if (period.first.empty())
    {
        return false;
    }
    m_period = period.first;

    auto elements = extract(fail_msg, line.second, period.first);

    m_elements.clear();

    auto curr_text = optic.second;

    std::map<std::string, std::shared_ptr<IOpticElement>> m_all_elements;

    while (!curr_text.empty())
    {
        auto element_data = extract_cmd_data_cut(fail_msg, curr_text, "", ";");
        if (element_data.empty())
        {
            break;
        }
        auto splitted = commtools::strsplit_adapter<std::list<std::string>>(element_data,
                                                                            std::string(": ,;=,"));

        if (splitted.size() < 2)
        {
            BL_ERROR() << fail_msg << "incorrect element description: " << element_data;
            return false;
        }

        std::string ref;

        const auto new_tag = splitted.front();

        if (find_element(m_all_elements, new_tag))
        {
            BL_ERROR() << fail_msg << "element " << new_tag << " is already descripted earlier";
            return false;
        }

        if (splitted.size() == 2)
        {
            ref = *(++splitted.begin());

            auto ref_ptr = find_element(m_all_elements, ref);
            if (!ref_ptr)
            {
                BL_ERROR() << fail_msg << "element " << ref << " is not descripted earlier";
                return false;
            }
            auto new_el = ref_ptr->clone();
            new_el->set_label(new_tag);
            m_all_elements[new_tag] = new_el;
        }
        else
        {
            auto tmp = from_madx(element_data);

            if (!tmp)
            {
                return false;
            }
            m_all_elements[new_tag] = tmp;
        }
    }

    for (const auto element_label : elements)
    {
        auto loc_msg = fail_msg + "element " + element_label + " is not precense in optics data";

        auto ref_ptr = find_element(m_all_elements, element_label);
        if (!ref_ptr)
        {
            BL_ERROR() << loc_msg;
            return false;
        }
        m_elements.emplace_back(ref_ptr);
    }

    auto curr_text_errors = errors.second;

    while (!curr_text_errors.empty())
    {
        auto element_label =
            extract_cmd_data_cut(fail_msg, curr_text_errors, "PATTERN = \"", "\";", false, true);

        auto ref_ptr = find_element(m_all_elements, element_label);

        if (!ref_ptr)
        {
            BL_WARNING() << "Insert error warning: element " << element_label
                         << " is not descripted in optic";
            continue;
        }

        auto DKNR =
            extract_cmd_data_cut(fail_msg, curr_text_errors, "DKNR: = {", "};", false, true);

        auto splitted =
            commtools::strsplit_adapter<std::vector<std::string>>(DKNR, std::string(": ,;=,"));

        if (splitted.size() != 4)
        {
            BL_WARNING() << "Incorrect errors description of element: " << element_label;
            continue;
        }

        try
        {
            const double err = std::stod(splitted[0]);
            ref_ptr->insert_field_error(err);
        }
        catch (const std::exception& ex)
        {
            BL_WARNING() << "Insert error warning: " << ex.what();
            continue;
        }
    }

    return true;
}

std::shared_ptr<IOpticElement> Sequence::from_madx(const std::string& data) noexcept
{
    BL_FTRACE();

    auto splitted =
        commtools::strsplit_adapter<std::list<std::string>>(data, std::string(": ,;=, "));

    if (splitted.size() < 2)
    {
        BL_ERROR() << "Incorrect input format: size() < 2";
        return nullptr;
    }

    if (splitted.size() % 2)
    {
        BL_ERROR() << "Incorrect number of arguments";
        return nullptr;
    }

    auto it = splitted.begin();

    auto tag = *(it);

    auto type = *(++it);

    auto element = IOpticElement::make_child(type);

    std::string element_msg = "element (element tag: " + tag + ", element type: " + type + ")";

    std::string fail_msg = "Failed to make " + element_msg;

    if (!element)
    {
        BL_ERROR() << fail_msg << ": unexpected type: " << type;
        return nullptr;
    }

    auto required_list = element->get_madx_required();

    auto pt = element->to_json();

    if (!pt)
    {
        BL_ERROR() << fail_msg << ": failed to load data to json";
        return nullptr;
    }

    for (auto data_it = ++it; data_it != splitted.end(); std::advance(data_it, 2))
    {
        auto child = pt->get_child_optional(*data_it);
        if (!child)
        {
            BL_ERROR() << fail_msg << ": unexpected property: " << *data_it;
            return nullptr;
        }
        auto it_val = data_it;
        try
        {
            pt->put(*data_it, std::stod(*(++it_val)));
            required_list.erase(std::remove(required_list.begin(), required_list.end(), *data_it),
                                required_list.end());
        }
        catch (const std::exception& ex)
        {
            BL_ERROR() << fail_msg << ": error putting value: " << *it_val << ": " << ex.what();
            return nullptr;
        }
    }

    pt->put("label", tag);

    if (!required_list.empty())
    {
        std::string missed = "";
        for (const auto& el : required_list)
        {
            missed += el + ", ";
        }
        BL_ERROR() << fail_msg
                   << ": the following required properties are not defiened: " << missed;
        return nullptr;
    }

    if (element->from_json(*pt))
    {
        auto el_ch = static_cast<daisi::accel::Quadrupole*>(element.get());

        BL_TRACE() << "Success make " << element_msg;
        return element;
    }

    BL_ERROR() << fail_msg << ": error load data from json";
    return nullptr;
}
} // namespace accel
} // namespace daisi
