#ifndef DAISIACCEL_OPTIC_SEQUENCE_H
#define DAISIACCEL_OPTIC_SEQUENCE_H

#include <armadillo>
#include <list>

#include <common_tools/propagate_const.hpp>
#include <common_tools/service_locator.hpp>

#include "../base/imodelcomponent.h"
#include "opticelements.h"

class TestLoadOptics;

namespace daisi
{
namespace accel
{
struct TransportMatrix
{
    arma::mat M;
    arma::vec B;
    double    S;
    TransportMatrix()
    {
    }

    TransportMatrix(const arma::mat& M_, const arma::vec& B_, const double S_) : M(M_), B(B_), S(S_)
    {
    }
};

class Sequence final : public daisi::IModelComponentCRTP<Sequence>
{
    friend class ::TestLoadOptics;

  public:
    Sequence();
    Sequence(const Sequence&);
    Sequence(Sequence&&);

    BUILD_CHILD(Sequence, daisi::IModelComponent)

    std::shared_ptr<IModelComponentView> to_model_view(const std::string& name,
                                                       const std::string& type) const override;

    bool from_model_view(const std::string                         name,
                         const std::map<std::string, std::string>& content) override;

    std::string generate_mad_optic() const noexcept;
    std::string generate_mad_line() const noexcept;
    std::string generate_obs_commands(const std::vector<std::string>& mons) const noexcept;

    std::vector<double> get_errors(const std::string& type) const noexcept;

    std::vector<std::shared_ptr<IOpticElement>> get_elements_list(const std::string& type) const
        noexcept;

    void set_elements_list(const std::vector<std::shared_ptr<IOpticElement>>& elements,
                           const std::string&                                 type) noexcept;

    void set_corrector_angles(const std::vector<double>& angles) noexcept;

    std::vector<double> get_corrector_angles() const noexcept;

    int get_element_number_by_label(const std::string& label) const noexcept;

    std::list<TransportMatrix> get_matrixes(const std::string& type = "") const noexcept;

  private:
    std::string m_period;

    static std::shared_ptr<IOpticElement> from_madx(const std::string& data) noexcept;

    std::list<std::shared_ptr<IOpticElement>> m_elements;

    static std::shared_ptr<IOpticElement>
    find_element(const std::map<std::string, std::shared_ptr<IOpticElement>>& elemnts_to_find,
                 const std::string&                                           ke) noexcept;
};
} // namespace accel
} // namespace daisi

#endif
