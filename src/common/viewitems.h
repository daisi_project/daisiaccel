#ifndef DAISI_VIEW_ITEMS_H
#define DAISI_VIEW_ITEMS_H

#include <serreflection/defines.hpp>

namespace daisi
{
struct ModelViewSimpleItem
{
    std::string item_label;
    std::string content;
    std::string item_typename;
};
}
SERIALIZIBLE_STRUCT(daisi::ModelViewSimpleItem, srfl::CheckModes::FATAL,
                    (std::string, item_label,
                     DEF_D())(std::string, content, DEF_D())(std::string, item_typename, DEF_D()))
#endif
