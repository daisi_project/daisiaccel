#ifndef DAISI_MAD_SAFE_WRAPPER_H
#define DAISI_MAD_SAFE_WRAPPER_H

#include <string>

namespace daisi
{
class MADXSafeWrapper
{
  public:
    MADXSafeWrapper(std::string path);

    bool get_result() const noexcept;

  private:
    bool m_result;

    static void* run(void* arg);
};
}
#endif
