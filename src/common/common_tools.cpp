#include <common_tools/boostlog.hpp>
#include <notk/controller.hpp>

#include "common_tools.h"

namespace daisi
{

std::vector<float> gaussmf(const std::vector<float>& x, const float mu, const float sigma)
{
    if (std::abs(sigma) < 1e-20)
    {
        throw std::runtime_error("gaussmf::too small sigma");
    }
    std::vector<float> result(x.size());

    float koef = 1.0 / (sigma * std::sqrt(2.0 * 3.14159265359));

    for (size_t i = 0; i < x.size(); i++)
    {
        result[i] = koef * std::exp(-(x[i] - mu) * (x[i] - mu) / (2.0 * sigma * sigma));
    }
    return result;
}

float fitness(const std::vector<float>& x, std::vector<float>& y, const std::vector<float>& param)
{
    std::vector<float> y_  = gaussmf(x, param[0], param[1]);
    float              fit = 0;
    for (size_t i = 0; i < y.size(); i++)
    {
        fit = fit + (y[i] - y_[i]) * (y[i] - y_[i]);
    }
    return fit;
}

std::vector<float> gauss_fit(const std::vector<float>& x, const std::vector<float>& y,
                             const double requiredAccuracy, const std::vector<float>& x1,
                             const std::vector<float>& x2)
{
    notk::NOTKController<float, float> NOTKController;

    std::string config =
        "{\"version\": \"0.3\",\"log_each_iteration\": \"10\", \"allow_maximization\": true, "
        "\"borders_type\": \"VECTOR\",\"methods\": "
        "[{\"type\": \"Gauss search\", \"use_random\": true, \"maximal_iterations\": 100, "
        "\"maximal_preprocess_iterations\": 10, \"required_accuracy\": 1e-3, \"accuracy_type\": "
        "\"RELATIVE\", \"accuracy_source_type\": \"FITNESS\",\"searcher1d\": {\"type\": \"Uniform "
        "1d search\", \"maximal_iterations\": 10, \"required_accuracy\": 1e-3, "
        "\"n_divisions_first\": 20, \"n_divisions\": 10, \"accuracy_type\": \"RELATIVE\", "
        "\"accuracy_source_type\": \"FITNESS\" }}]}";

    NOTKController.set_problem_config_str(config);
    NOTKController.set_borders_fitness(std::bind(fitness, x, y, std::placeholders::_1), x1, x2);
    bool flag_abort = true;

    auto result = NOTKController.process(flag_abort);

    /*const OptimizationAccuracy accuracy_mode(OptimizationAccuracy::AccuracyType::RELATIVE,
            OptimizationAccuracy::AccuracySourceType::FITNESS,
            requiredAccuracy);

    auto search1d = std::bind(uniformSearch1d, maxNumIterationsInner, nDivisions * 2, nDivisions,
            std::placeholders::_1, std::placeholders::_2, std::placeholders::_3,
            accuracy_mode);

    auto fit = std::bind(fitness, std::ref(x), std::ref(y), std::placeholders::_1);

    auto results = gaussMultiDimSearch(100, 0.8, x1, x2, fit, search1d, accuracy_mode);

    return results.best_fitness_argument;*/
    return result->get_last_argument();
}

std::vector<float> estimate_gauss(const std::vector<float>& x, const std::vector<float>& y,
                                  const double requiredAccuracy)
{
    std::vector<float> x1(2);
    std::vector<float> x2(2);
    std::vector<float> x_s = x;
    std::vector<float> y_s = y;

    for (size_t i = 0; i < x_s.size(); i++)
    {
        if (std::abs(y_s[i]) < 1e-17)
        {
            x_s.erase(x_s.begin() + i);
            y_s.erase(y_s.begin() + i);

            i--;
        }
    }

    x1[0] = *std::min_element(x_s.begin(), x_s.end());
    x2[0] = *std::max_element(x_s.begin(), x_s.end());

    float x_av = (x1[0] + x2[0]) / 2.0;

    x1[1] = 1e-9;
    x2[1] = 2 * (x2[0] - x_av);

    return gauss_fit(x, y, requiredAccuracy, x1, x2);
}

bool convertChart(const std::vector<float>& XChart, const std::vector<float>& YChart,
                  std::vector<float>& XChart_out, std::vector<float>& YChart_out)
{
    for (size_t i = 0; i < XChart.size() - 1; i++)
    {
        XChart_out.push_back(XChart[i]);
        XChart_out.push_back(XChart[i + 1]);
        YChart_out.push_back(YChart[i]);
        YChart_out.push_back(YChart[i]);
    }
    return true;
}

std::string extract_cmd_data_cut(const std::string& fail_msg, std::string& content,
                                 const std::string& search_cmd, const std::string& right,
                                 const bool print_err, const bool remove_spec_chars)
{

    auto res = extract_cmd_data(fail_msg, content, search_cmd, right, print_err, remove_spec_chars);

    content = content.substr(res.second + 1, content.size() - res.second);

    return res.first;
}

std::pair<std::string, size_t> extract_cmd_data(const std::string& fail_msg,
                                                const std::string& content,
                                                const std::string& search_cmd,
                                                const std::string& right, const bool print_err,
                                                const bool remove_spec_chars)
{
    auto use_pos = content.find(search_cmd);
    if (use_pos == std::string::npos)
    {
        if (print_err)
        {
            BL_ERROR() << fail_msg << "the command \"" << search_cmd << "\" is abcense";
        }
        return std::make_pair("", 0);
    }
    auto dot_pos = content.find(right, use_pos + 1);
    if (dot_pos == std::string::npos)
    {
        if (print_err)
        {
            BL_ERROR() << fail_msg << "the " << right << " after command \"" << search_cmd
                       << "\" is abcense";
        }
        return std::make_pair("", 0);
    }
    auto start = use_pos + search_cmd.size();

    auto result = content.substr(start, dot_pos - start);

    if (remove_spec_chars)
    {
        result.erase(std::remove(result.begin(), result.end(), ' '), result.end());
        result.erase(std::remove(result.begin(), result.end(), '\n'), result.end());
        result.erase(std::remove(result.begin(), result.end(), '\t'), result.end());
        result.erase(std::remove(result.begin(), result.end(), '\r'), result.end());
    }

    return std::make_pair(result, dot_pos);
}
} // namespace daisi
