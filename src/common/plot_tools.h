#ifndef DAISI_PLOT_TOOLS_H
#define DAISI_PLOT_TOOLS_H

#include <vector>

std::pair<std::vector<float>, std::vector<float>> getChart(const std::vector<float>& data,
                                                           const int nCharts, const int flag,
                                                           const double wmax_in,
                                                           const bool   percentize = true);

std::pair<std::vector<float>, std::vector<float>> convertChart(const std::vector<float>& XChart,
                                                               const std::vector<float>& YChart);

#endif
