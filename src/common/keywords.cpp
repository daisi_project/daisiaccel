#include "keywords.h"

namespace daisi
{
namespace viewtypes
{
const std::string Plot          = "Plot";
const std::string VariablesList = "VariablesList";
const std::string FlagBox       = "FlagBox";
}
namespace fieldnames
{
const std::string name        = "name";
const std::string type        = "type";
const std::string label       = "label";
const std::string content     = "content";
const std::string is_editable = "is_editable";
}
}
