#include "solverresult.h"

namespace daisi
{

void SolverResult::write_cm_data(const std::array<std::vector<float>, 5>& cm)
{
    plots.emplace_back();
    auto& plot1 = plots.back();
    plot1.label = "X(Z) and Y(Z)";
    plot1.XData.push_back(cm[4]);
    plot1.XData.push_back(cm[4]);
    plot1.YData.push_back(cm[0]);
    plot1.YData.push_back(cm[2]);

    plots.emplace_back();
    auto& plot2 = plots.back();
    plot2.label = "dX/dZ(Z) and dY/dZ(Z)";
    plot2.XData.push_back(cm[4]);
    plot2.XData.push_back(cm[4]);
    plot2.YData.push_back(cm[1]);
    plot2.YData.push_back(cm[3]);
}
}