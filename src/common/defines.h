#ifndef DAISI_DEFINES_H
#define DAISI_DEFINES_H

#define CHECK_COMPONENT(fail_msg, comp, label)                                                     \
    if (!comp)                                                                                     \
    {                                                                                              \
        BL_ERROR() << fail_msg << ": view content item with label" << label                        \
                   << " is not precense ";                                                         \
        return false;                                                                              \
    }

#endif
