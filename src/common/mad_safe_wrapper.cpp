#include <pthread.h>

#include <common_tools/boostlog.hpp>

extern "C" {
#include <mad_main.h>
}

#include "mad_safe_wrapper.h"

namespace daisi
{
MADXSafeWrapper::MADXSafeWrapper(std::string path) : m_result(false)
{
    char* argv[3];
    argv[0] = &path[0];
    argv[1] = &path[0];
    argv[2] = (char*)(&m_result);

    BL_FTRACE();

    pthread_t helper;
    pthread_create(&helper, NULL, run, argv);

    pthread_join(helper, NULL);
}

bool MADXSafeWrapper::get_result() const noexcept
{
    return m_result;
}

void* MADXSafeWrapper::run(void* arg)
{
    char** argv   = (char**)arg;
    bool&  result = *((bool*)(argv[2]));

    auto status = mad_start(2, argv);

    result = true;

    if (status == 0)
    {
        result = true;
    }
    else
    {
        result = false;
    }
    pthread_exit(NULL);
}
}
