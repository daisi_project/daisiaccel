#ifndef DAISI_BOOST_PT_VIEW_COMPONENT_H
#define DAISI_BOOST_PT_VIEW_COMPONENT_H

#include <common_tools/service_locator.hpp>

#include "../base/imodelcomponentview.h"

namespace daisi
{
class IDataCommunicator;

class StandartView final : public IModelComponentView
{
  public:
    StandartView(const int model_id, const std::string& component_pseudoname,
                 const std::string& view_pseudoname, const std::string& name);
    StandartView() = default;

    bool get_is_editable() const override final;

  private:
    std::map<std::string, std::string> get_view_items() override final;

    bool fill_view_component(const std::shared_ptr<IModelComponentViewItem>& component,
                             const std::string& label) const override final;
};
} // namespace daisi

#endif
