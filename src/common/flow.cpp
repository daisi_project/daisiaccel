#include <iomanip>

#include <common_tools/constants.h>
#include <serreflection/read_json.hpp>

#include "../base/componentnames.h"
#include "flow.h"

namespace daisi
{
namespace compnames
{
const std::string Flow = "Flow";
}
namespace accel
{

REGISTER_CHILD_NON_TEMPLATE(Flow, daisi::IModelComponent, daisi::compnames::Flow)

std::shared_ptr<IModelComponentView> Flow::to_model_view(const std::string& name,
                                                         const std::string& type) const
{
}

std::string Flow::to_madx_beam() const
{
    std::stringstream result;

    for (size_t i = 0; i < m_beam_x.size(); i++)
    {
        result << std::setprecision(16) << "ptc_start, "
               << "x = " << m_beam_x[i] << ", y = " << m_beam_y[i] << ", px = " << 0
               << ", py = " << 0 << ";";
    }

    return result.str();
}

std::pair<double, double> Flow::get_apertures() const
{
    std::pair<double, double> result;

    result.first  = m_parameters->X_aperture__m;
    result.second = m_parameters->Y_aperture__m;

    return result;
}

std::pair<double, double> Flow::get_unnorm_emitt() const
{
    std::pair<double, double> result;
    const auto                mass = m_parameters->Mass_number * commtools::PROTON_MASS();

    const double restEnergy =
        -mass * commtools::LIGHT_VELOCITY() * commtools::LIGHT_VELOCITY() /
        commtools::ELECTRON_CHARGE(); // ������� ����� ������� � ���������������

    const double gamma = (restEnergy + std::abs(m_parameters->Ehergy__eV)) / restEnergy;
    const double beta  = std::sqrt(gamma * gamma - 1) / gamma;

    result.first  = m_parameters->EmittanceX_norm__pi_mm_mrad / beta;
    result.second = m_parameters->EmittanceY_norm__pi_mm_mrad / beta;

    return result;
}

std::vector<arma::vec> Flow::get_initial_beam_data(const int distrType, const size_t nParticles)
{
    std::vector<arma::vec> result(nParticles);

    std::default_random_engine             generator;
    std::normal_distribution<double>       distribution(0, 1);
    std::uniform_real_distribution<double> distributionUn(0, 1);

    const auto emit = get_unnorm_emitt();

    const double emittanceXunnorm = emit.first;
    const double emittanceYunnorm = emit.second;

    double Xparam[3];
    double Yparam[3];
    double Angles[3];
    double e[2];

    double alpha[3];

    Xparam[0] = m_parameters->X_max__m;         // double X
    Yparam[0] = m_parameters->dX___dZ_max__rad; // dX
    e[0]      = emittanceXunnorm * 1e-6;        // em

    Xparam[1] = m_parameters->Y_max__m;         // double Y
    Yparam[1] = m_parameters->dY___dZ_max__rad; // dY
    e[1]      = emittanceYunnorm * 1e-6;        // em

    const double R = std::max(Xparam[2], Xparam[1]);

    double gammaE[2];
    double betaE[2];
    double phi[2];
    double k[2];

    double Major[2];
    double Minor[2];

    for (int i = 0; i < 2; i++)
    {
        //	e[i] = Xparam[i] * Yparam[i] / std::sqrt(1 + alpha[i] * alpha[i]);

        alpha[i]  = std::sqrt(Xparam[i] * Xparam[i] * Yparam[i] * Yparam[i] - e[i] * e[i]) / e[i];
        gammaE[i] = Yparam[i] * Yparam[i] / std::abs(e[i]);
        betaE[i]  = Xparam[i] * Xparam[i] / std::abs(e[i]);

        phi[i] = std::atan(2 * alpha[i] / (gammaE[i] - betaE[i])) / 2;

        k[i] = std::tan(phi[i]);

        double x =
            std::sqrt(std::abs(e[i]) / (gammaE[i] + 2 * alpha[i] * k[i] + betaE[i] * k[i] * k[i]));
        double y = k[i] * x;
        Major[i] = std::sqrt(x * x + y * y);
        Minor[i] = std::abs(e[i]) / (Major[i]);

        if (Xparam[i] > 0)
            phi[i] = phi[i] + commtools::PI() / 2;
    };

    // double EnergyDistr(particlesPerBunch);
    // double PhaseDistr[particlesPerBunch];
    // double r_Distr[particlesPerBunch];
    // double pr_Distr[particlesPerBunch];
    std::vector<std::vector<double>> Data(6);

    auto generate = [&](const double Major, const double Minor, const double phi,
                        const double centerMassX,
                        const double centerMassdX) -> std::pair<double, double> {
        double                    xtmp, ytmp;
        std::pair<double, double> result;
        if (distrType == 0)
        {
            xtmp = (Major / 3) * distribution(generator);
            ytmp = (Minor / 3) * distribution(generator);
        };
        if (distrType == 1)
        {
            while (1)
            {
                xtmp = -Major + distributionUn(generator) * (2 * Major);
                ytmp = -Minor + distributionUn(generator) * (2 * Minor);
                if ((xtmp * xtmp / (Major * Major) + ytmp * ytmp / (Minor * Minor)) < 1)
                    break;
            }
        };

        if (Xparam[0] < 0)
        {
            result.first  = xtmp * std::cos(phi) - ytmp * std::sin(phi) + centerMassX;
            result.second = xtmp * std::sin(phi) + ytmp * std::cos(phi) + centerMassdX;
        }
        else
        {
            result.first  = xtmp * std::cos(phi) - ytmp * std::sin(phi) + centerMassdX;
            result.second = xtmp * std::sin(phi) + ytmp * std::cos(phi) + centerMassX;
        }
        return result;
    };

    for (int i = 0; i < nParticles; i++)
    {
        auto xx = generate(Major[0], Minor[0], phi[0], m_parameters->Center_mass_X__m,
                           m_parameters->Center_mass_dX___dZ__rad);
        auto yy = generate(Major[1], Minor[1], phi[1], m_parameters->Center_mass_Y__m,
                           m_parameters->Center_mass_dY___dZ__rad);

        result[i].resize(5);

        result[i](0) = xx.second;
        result[i](1) = xx.first;

        result[i](2) = yy.second;
        result[i](3) = yy.first;
        result[i](4) = 0;
    }
    return result;
}

std::array<double, 6> Flow::get_twiss_vector() const
{
    const auto emit = get_unnorm_emitt();

    const double emittanceXunnorm = emit.first;
    const double emittanceYunnorm = emit.second;

    double ey = emittanceYunnorm * 1e-6;
    double ex = emittanceXunnorm * 1e-6;

    auto dXmax = m_parameters->dX___dZ_max__rad;
    auto dYmax = m_parameters->dY___dZ_max__rad;

    auto Xmax = m_parameters->X_max__m;
    auto Ymax = m_parameters->Y_max__m;

    double alphaey = std::sqrt(Ymax * Ymax * dYmax * dYmax - ey * ey) / ey;
    double gammaey = dYmax * dYmax / ey;
    double betaey  = Ymax * Ymax / ey;
    if (dYmax < 0 || Ymax < 0)
        alphaey = -alphaey;

    double alphaex = std::sqrt(Xmax * Xmax * dXmax * dXmax - ex * ex) / ex;
    double gammaex = dXmax * dXmax / ex;
    double betaex  = Xmax * Xmax / ex;
    if (dXmax < 0 || Xmax < 0)
        alphaex = -alphaex;

    std::array<double, 6> result;
    result[0] = betaex;
    result[1] = alphaex;
    result[2] = gammaex;
    result[3] = betaey;
    result[4] = alphaey;
    result[5] = gammaey;
    return result;
}

unsigned Flow::get_n_circles() const
{
    if (!m_parameters)
    {
        throw std::runtime_error("Invalid Flow");
    }
    return m_parameters->n_turns;
}

void Flow::set_n_circles(const unsigned n_in) const
{
    m_parameters->n_turns = n_in;
}

void Flow::set_xy_dev(const double x, const double y) const
{
    m_parameters->Center_mass_X__m = x;
    m_parameters->Center_mass_Y__m = y;
}

std::array<std::string, 2> Flow::to_madx() const
{
    if (!m_parameters)
    {
        throw std::runtime_error("Invalid Flow");
    }

    std::array<std::string, 2> result;

    std::stringstream beam;
    beam << std::setprecision(16)
         << "beam, particle = ion, energy = " << (m_parameters->Ehergy__eV * 1e-9) << ";\n\n";
    result[0] = beam.str();

    std::stringstream cm;
    cm << std::setprecision(16) << "x = " << m_parameters->Center_mass_X__m
       << ", y = " << m_parameters->Center_mass_Y__m
       << ", px = " << m_parameters->Center_mass_dX___dZ__rad
       << ", py = " << m_parameters->Center_mass_dY___dZ__rad << ";";

    result[1] = cm.str();

    return result;
}
arma::vec Flow::get_initial_data() const
{
    arma::vec result(5);

    result(0) = m_parameters->Center_mass_X__m;
    result(1) = m_parameters->Center_mass_dX___dZ__rad;
    result(2) = m_parameters->Center_mass_Y__m;
    result(3) = m_parameters->Center_mass_dY___dZ__rad;
    result(4) = 0;

    return result;
}

void Flow::set_beam_initials(const std::vector<double>& x, const std::vector<double>& y)
{
    if (x.size() != y.size())
    {
        throw std::runtime_error("nonequal sizes of flow initials");
    }

    m_beam_x = x;
    m_beam_y = y;
}

} // namespace accel
} // namespace daisi
