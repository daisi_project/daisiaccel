#ifndef DAISI_VIEW_TYPES_H
#define DAISI_VIEW_TYPES_H

#include <string>

namespace daisi
{
namespace viewtypes
{
extern const std::string TextArea;
extern const std::string Plot;
extern const std::string VariablesList;
extern const std::string FlagBox;
}
namespace fieldnames
{
extern const std::string name;
extern const std::string type;
extern const std::string label;
extern const std::string content;
extern const std::string is_editable;
}
}
#endif
