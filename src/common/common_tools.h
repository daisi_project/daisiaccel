#ifndef DAISI_COMMON_TOOLS_H
#define DAISI_COMMON_TOOLS_H

#include <string>

namespace daisi
{
std::pair<std::string, size_t>
extract_cmd_data(const std::string& fail_msg, const std::string& content,
                 const std::string& search_cmd, const std::string& right,
                 const bool print_err = true, const bool remove_spec_chars = true);

std::string extract_cmd_data_cut(const std::string& fail_msg, std::string& content,
                                 const std::string& search_cmd, const std::string& right,
                                 const bool print_err = true, const bool remove_spec_chars = true);

std::vector<float> estimate_gauss(const std::vector<float>& x, const std::vector<float>& y,
                                  const double requiredAccuracy);

} // namespace daisi

#endif
