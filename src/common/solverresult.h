#ifndef DAISI_SOLVER_RESULT_H
#define DAISI_SOLVER_RESULT_H

#include <array>
#include <string>
#include <vector>

namespace daisi
{
struct SolverResult
{
    void write_cm_data(const std::array<std::vector<float>, 5>& cm);

    struct ResultPlot
    {
        std::string label;

        std::vector<std::vector<float>> XData;
        std::vector<std::vector<float>> YData;
    };

    struct ResultPlotExtended : ResultPlot
    {
        std::vector<std::string> labels;
    };

    std::vector<ResultPlot>         plots;
    std::vector<ResultPlotExtended> plots_ext;

    std::vector<std::pair<std::string, std::string>> text;

    bool        status;
    std::string error_message;
};
} // namespace daisi
#endif
