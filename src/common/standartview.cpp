#include "standartview.h"
#include "../base/idatacommunicator.h"
#include "../base/viewtypes.h"
#include "../common/keywords.h"
#include "../common/viewitems.h"

#include <serreflection/read_json.hpp>

#define BOOST_PT_VIEW_COMMON(ret)                                                                  \
    auto it = m_data.find(label);                                                                  \
    if (m_data.end() == it)                                                                        \
    {                                                                                              \
        BL_ERROR() << "unexpected component label: " << label;                                     \
        return ret;                                                                                \
    }

namespace daisi
{

StandartView::StandartView(const int model_id, const std::string& component_pseudoname,
                           const std::string& view_pseudoname, const std::string& name)
    : IModelComponentView(model_id, component_pseudoname, view_pseudoname, name)
{
}

bool StandartView::get_is_editable() const
{
    return true;
}

std::map<std::string, std::string> StandartView::get_view_items()
{
    const auto err = "Error read model view item";

    auto res = IDataCommunicator::get().get_model_view_json(m_model_id, m_component_pseudoname,
                                                            m_view_pseudoname, false, m_name);

    if (!res.first)
    {
        throw std::runtime_error(err);
    }

    std::map<std::string, std::string> result;
    for (const auto& item : res.second)
    {
        auto item_data =
            srfl::read_json_string<ModelViewSimpleItem>(item.at("get_model_view_items_join_json"));

        if (!item_data)
        {
            throw std::runtime_error(err);
        }
        result[item_data->item_label] = item_data->content;
    }
    return result;
}

bool StandartView::fill_view_component(const std::shared_ptr<IModelComponentViewItem>& component,
                                       const std::string&                              label) const
{
    // BOOST_PT_VIEW_COMMON(false)
    // return component->from_json(it->second);
    return true;
}
} // namespace daisi
