#ifndef DAISIACCEL_FLOW_H
#define DAISIACCEL_FLOW_H

#include <armadillo>

#include <serreflection/defines.hpp>

#include "../base/imodelcomponent.h"

namespace daisi
{
namespace accel
{
struct FlowParameters
{
    double Center_mass_X__m;
    double Center_mass_dX___dZ__rad;
    double Center_mass_dY___dZ__rad;
    double Mass_number;
    double Center_mass_Y__m;

    double Charge_number;
    double Ehergy__eV;
    int    n_turns;

    double X_max__m;
    double Y_max__m;
    double dX___dZ_max__rad;
    double dY___dZ_max__rad;
    double EmittanceX_norm__pi_mm_mrad;
    double EmittanceY_norm__pi_mm_mrad;
    double X_aperture__m;
    double Y_aperture__m;
};

class Flow final : public daisi::IModelComponentCRTPParams<Flow, FlowParameters>
{
  public:
    BUILD_CHILD(Flow, daisi::IModelComponent)

    std::shared_ptr<IModelComponentView> to_model_view(const std::string& name,
                                                       const std::string& type) const override;

    unsigned get_n_circles() const;
    void     set_n_circles(const unsigned n_in) const;
    void     set_xy_dev(const double x, const double y) const;
    void     set_beam_initials(const std::vector<double>& x, const std::vector<double>& y);

    std::array<std::string, 2> to_madx() const;

    std::string to_madx_beam() const;

    arma::vec get_initial_data() const;

    std::pair<double, double> get_apertures() const;

    std::vector<arma::vec>    get_initial_beam_data(const int distrType, const size_t nParticles);
    std::array<double, 6>     get_twiss_vector() const;
    std::pair<double, double> get_unnorm_emitt() const;

  private:
    std::vector<double> m_beam_x;
    std::vector<double> m_beam_y;
};
} // namespace accel
} // namespace daisi
SERIALIZIBLE_STRUCT(
    daisi::accel::FlowParameters, srfl::CheckModes::FATAL,
    (double, Center_mass_X__m, DEF_D())(double, Center_mass_Y__m, DEF_D())(
        double, Center_mass_dX___dZ__rad, DEF_D())(double, Center_mass_dY___dZ__rad, DEF_D())(
        double, Mass_number, DEF_D())(double, Charge_number, DEF_D())(double, Ehergy__eV, DEF_D())(
        int, n_turns, srfl::nan, 1.0, srfl::inf)(double, dX___dZ_max__rad, DEF_D())(
        double, dY___dZ_max__rad, DEF_D())(double, X_max__m, DEF_D())(double, Y_max__m, DEF_D())(
        double, EmittanceX_norm__pi_mm_mrad,
        DEF_D())(double, EmittanceY_norm__pi_mm_mrad,
                 DEF_D())(double, X_aperture__m, DEF_D())(double, Y_aperture__m, DEF_D()))
#endif
