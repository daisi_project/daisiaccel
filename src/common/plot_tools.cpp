#include <algorithm>
#include <cmath>

#include "plot_tools.h"

std::pair<std::vector<float>, std::vector<float>> getChart(const std::vector<float>& data,
                                                           const int nCharts, const int flag,
                                                           const double wmax_in,
                                                           const bool   percentize)
{
    std::pair<std::vector<float>, std::vector<float>> result;

    if (data.size() == 0)
    {
        return result;
    }

    auto minmax = std::minmax_element(data.begin(), data.end());

    volatile float wmin = *minmax.first;
    volatile float wmax = *minmax.second;

    // if (std::abs(wmax - wmin) < 1e-7)
    //     return result;

    if (wmax_in != -1)
    {
        wmax = wmax_in;
    }

    if (wmin < 0)
        wmin = wmin * 1.04;
    else
        wmin = wmin * 0.96;

    if (wmax > 0)
        wmax = wmax * 1.04;
    else
        wmax = wmax * 0.96;

    float d = (wmax - wmin) / float(nCharts);
    result.first.resize(nCharts + 1);
    result.second.resize(nCharts + 1);

    result.first[0]     = wmin;
    result.first.back() = wmax;

    for (int i          = 1; i < nCharts; i++)
        result.first[i] = wmin + i * d;

    for (int i = 0; i < nCharts; i++)
    {
        for (int j = 0; j < data.size(); j++)
        {
            if (flag == 0)
            {
                if (data[j] >= result.first[i] && data[j] < result.first[i + 1])
                    result.second[i]++;
            }
            if (flag == 1)
            {
                if (data[j] < result.first[i])
                    result.second[i]++;
            }
        }
    }

    if (percentize)
    {
        for (auto& y_ch : result.second)
        {
            y_ch = 100.0 * y_ch / data.size();
        }
    }
    return result;
}

std::pair<std::vector<float>, std::vector<float>> convertChart(const std::vector<float>& XChart,
                                                               const std::vector<float>& YChart)
{
    std::pair<std::vector<float>, std::vector<float>> result;

    if (XChart.empty() || XChart.size() != YChart.size())
    {
        return result;
    }

    for (size_t i = 0; i < XChart.size() - 1; i++)
    {
        result.first.push_back(XChart[i]);
        result.first.push_back(XChart[i + 1]);
        result.second.push_back(YChart[i]);
        result.second.push_back(YChart[i]);
    }
    return result;
}
