#include <common_tools/boostlog.hpp>

#include "imodelcomponentview.h"

namespace daisi
{
IModelComponentView::IModelComponentView(const int          model_id,
                                         const std::string& component_pseudoname,
                                         const std::string& view_pseudoname,
                                         const std::string& name)
    : m_model_id(model_id), m_component_pseudoname(component_pseudoname),
      m_view_pseudoname(view_pseudoname), m_name(name)
{
}

std::string IModelComponentView::get_name() const noexcept
{
    return m_view_pseudoname;
}
} // namespace daisi
