#ifndef DAISIACCEL_COMPONENT_NAMES_H
#define DAISIACCEL_COMPONENT_NAMES_H

#include <string>

namespace daisi
{
namespace compnames
{
extern const std::string Sequence;
extern const std::string Flow;
}
}

#endif
