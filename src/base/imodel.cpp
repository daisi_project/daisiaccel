
#include <common_tools/boostlog.hpp>

#include "imodel.h"
#include "imodelcomponent.h"

namespace daisi
{
void IModel::add_component(const std::shared_ptr<IModelComponent>& component,
                           const std::string&                      name) noexcept
{
    m_components.set_code(component, component->hash_code(), name);
}

bool IModel::change_component(const std::string&                          comp_name,
                              const std::shared_ptr<IModelComponentView>& view) noexcept
{
    // BL_FTRACE();
    // std::string fail_msg = "Failed to change component (" + comp_name + ")";

    // auto component_tmp = IModelComponent::make_unique_child(comp_name);
    // if (!component_tmp)
    // {
    //     BL_ERROR() << fail_msg << ": unexpected component type: " << comp_name;
    //     return false;
    // }
    // auto changed_comp = m_components.get_base_code(component_tmp->hash_code());
    // if (!changed_comp)
    // {
    //     BL_ERROR() << fail_msg << ": component with name: " << comp_name << " is not inside model
    //     "
    //                << get_key();
    //     return false;
    // }

    // if (!changed_comp->from_model_view(view))
    // {
    //     BL_ERROR() << fail_msg << ": error read from view";
    // }

    // BL_TRACE() << "Success change component " << comp_name;

    return true;
}
} // namespace daisi
