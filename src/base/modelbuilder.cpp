#include <common_tools/boostlog.hpp>

#include "imodel.h"
#include "imodelcomponent.h"

namespace daisi
{
// std::unique_ptr<IModel>
// build_model(const std::string name,
//             const std::map<std::string, std::shared_ptr<IModelComponentView>>& view) noexcept
// {
//     BL_FTRACE();

//     std::string model_msg = "model (model type: " + name + ")";

//     std::string fail_msg = "Failed to make " + model_msg;

//     auto model = IModel::make_unique_child(name);

//     if (!model)
//     {
//         BL_ERROR() << fail_msg << ": unexpected type: " << name;
//         return nullptr;
//     }

//     auto required_components = model->get_required_components();

//     for (const auto& comp_name : required_components)
//     {
//         auto it = view.find(comp_name);
//         if (view.end() == it)
//         {
//             BL_ERROR() << fail_msg << ": component \"" << comp_name
//                        << "\" is not present in input view";
//             return nullptr;
//         }
//         model->add_component(IModelComponent::make_child(comp_name));
//         if (!model->change_component(comp_name, it->second))
//         {
//             return nullptr;
//         }
//     }

//     BL_TRACE() << "Success make " << model_msg;

//     return model;
// }
}
