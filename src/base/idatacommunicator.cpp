#include "idatacommunicator.h"

namespace daisi
{
IDataCommunicator::~IDataCommunicator() = default;

IDataCommunicator* IDataCommunicator::m_instance = nullptr;

bool IDataCommunicator::set_result_status(const int results_id, const std::string& status) noexcept
{
    return true;
}

bool IDataCommunicator::set_result_progress(const int results_id, const double& progress) noexcept
{
    return true;
}

bool IDataCommunicator::set_model_component_valid(const int          model_id,
                                                  const std::string& component_name,
                                                  const bool         is_valid,
                                                  const std::string& name) noexcept
{
    return true;
}
q_res_t IDataCommunicator::get_tasks() noexcept
{
    return q_res_t();
}
bool IDataCommunicator::set_result_data(const int results_id, const std::string& plot_label,
                                        const std::vector<std::vector<float>>& x_data,
                                        const std::vector<std::vector<float>>& y_data) noexcept
{
    return true;
}

bool IDataCommunicator::set_task_log_data(const int&         user_name,
                                          const std::string& content_in) noexcept
{
}

bool IDataCommunicator::set_current_log_data(const std::string& user_name,
                                             const std::string& content_in) noexcept
{
    return true;
}

bool IDataCommunicator::clear_current_log_data(const std::string& user_name) noexcept
{
    return true;
}

bool IDataCommunicator::set_result_data(const int results_id, const std::string& plot_label,
                                        const std::vector<std::vector<float>>& x_data,
                                        const std::vector<std::vector<float>>& y_data,
                                        const std::vector<std::string>&        labels) noexcept
{
    return true;
}

bool IDataCommunicator::set_result_text_data(const int results_id, const std::string& label,
                                             const std::string& content) noexcept
{
    return true;
}
} // namespace daisi
