#include <common_tools/boostlog.hpp>

#include "idatacommunicator.h"
#include "imodel.h"
#include "imodelcomponent.h"
#include "imodelcomponentview.h"
#include "isolver.h"

namespace daisi
{
std::shared_ptr<IModelComponentView> ISolver::to_model_view(const std::string& name,
                                                            const std::string& type) const
{
    throw std::logic_error("to_model_view is not implimented for ISolver childs");
}

bool ISolver::run(const int results_id, const std::unique_ptr<IModel>& model) const noexcept
{
    BL_FTRACE();

    auto results = run_concrete_solver(results_id, model);

    if (!results.status)
    {
        BL_ERROR() << results.error_message;
        return false;
    }
    
    auto err_msg = "Error add data to database";

    for (const auto& plot : results.plots)
    {
        if (!IDataCommunicator::get().set_result_data(results_id, plot.label, plot.XData,
                                                      plot.YData))
        {
            BL_ERROR() << err_msg;
            return false;
        }
    }

    for (const auto& plot : results.plots_ext)
    {
        if (!IDataCommunicator::get().set_result_data(results_id, plot.label, plot.XData,
                                                      plot.YData, plot.labels))
        {
            BL_ERROR() << err_msg;
            return false;
        }
    }

    for (const auto& text : results.text)
    {
        if (!IDataCommunicator::get().set_result_text_data(results_id, text.first, text.second))
        {
            BL_ERROR() << err_msg;
            return false;
        }
    }

    return results.status;
}
} // namespace daisi
