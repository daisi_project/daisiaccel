#ifndef DAISI_I_SOLVER_H
#define DAISI_I_SOLVER_H

#include <common_tools/child_factory.hpp>
#include <common_tools/propagate_const.hpp>

#include "../common/solverresult.h"
#include "imodelcomponent.h"

namespace daisi
{
class IModel;
class IDataCommunicator;

class ISolver : public IModelComponent
{
  public:
    std::shared_ptr<IModelComponentView> to_model_view(const std::string& name,
                                                       const std::string& type) const final;

    bool run(const int results_id, const std::unique_ptr<IModel>& model) const noexcept;

    virtual SolverResult run_concrete_solver(const int                      results_id,
                                             const std::unique_ptr<IModel>& model) const
        noexcept = 0;
};

template <class T>
class ISolverCRTP : public ISolver
{
    size_t hash_code() const noexcept override final
    {
        return typeid(T).hash_code();
    }
};

template <class T, class Tparams>
class ISolverCRTPParams : public ISolverCRTP<T>
{
  public:
    make_from_model_view();

  protected:
    std::shared_ptr<Tparams> m_parameters;
};

} // namespace daisi

#endif
