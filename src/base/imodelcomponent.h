#ifndef DAISI_I_MODEL_COMPONENT_H
#define DAISI_I_MODEL_COMPONENT_H

#include <map>

#include <boost/property_tree/ptree.hpp>

#include <common_tools/child_factory.hpp>
#include <common_tools/propagate_const.hpp>
#include <common_tools/service_locator.hpp>

#include <serreflection/read_json.hpp>

namespace daisi
{

class IModelComponentView;
class IModelComponentViewItem;

class IModelComponent
{
  public:
    virtual std::shared_ptr<IModelComponentView> to_model_view(const std::string& name,
                                                               const std::string& type) const = 0;

    bool from_model_view(const std::shared_ptr<IModelComponentView>& view) noexcept;

    virtual bool from_model_view(const std::string                         name,
                                 const std::map<std::string, std::string>& content) = 0;

    virtual size_t hash_code() const noexcept = 0;

    BUILD_CHILD_FACTORY(std::string, IModelComponent)

  protected:
    static std::pair<bool, std::string>
    extract_content(const std::string fail_msg, const std::map<std::string, std::string>& content,
                    const std::string& label);
};

template <class T>
class IModelComponentCRTP : public IModelComponent
{
    size_t hash_code() const noexcept override final
    {
        return typeid(T).hash_code();
    }
};

#define make_from_model_view()                                                                     \
    bool from_model_view(const std::string                         name,                           \
                         const std::map<std::string, std::string>& content) override final         \
    {                                                                                              \
        BL_FTRACE();                                                                               \
        const std::string fail_msg = "failed to load flow from view " + name;                      \
                                                                                                   \
        auto data = this->extract_content(fail_msg, content, "");                                  \
                                                                                                   \
        if (!data.first)                                                                           \
        {                                                                                          \
            return false;                                                                          \
        }                                                                                          \
                                                                                                   \
        m_parameters = srfl::read_json_string<Tparams>(data.second);                               \
                                                                                                   \
        if (!m_parameters)                                                                         \
        {                                                                                          \
            return false;                                                                          \
        }                                                                                          \
                                                                                                   \
        return true;                                                                               \
    }

template <class T, class Tparams>
class IModelComponentCRTPParams : public IModelComponentCRTP<T>
{
  public:
    make_from_model_view();

  protected:
    std::shared_ptr<Tparams> m_parameters;
};

} // namespace daisi

#endif
