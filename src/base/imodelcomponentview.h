#ifndef DAISI_I_MODEL_VIEW_COMPONENT_H
#define DAISI_I_MODEL_VIEW_COMPONENT_H

#include <list>
#include <string>

#include <boost/property_tree/ptree.hpp>

#include <common_tools/child_factory.hpp>
#include <common_tools/propagate_const.hpp>
#include <common_tools/service_locator.hpp>

namespace daisi
{
class IModelComponentViewItem;
class IDataCommunicator;

class IModelComponentView
{
  public:
    IModelComponentView(const int model_id, const std::string& component_pseudoname,
                        const std::string& view_pseudoname, const std::string& name);

    std::string get_name() const noexcept;

    virtual bool get_is_editable() const = 0;

    virtual std::map<std::string, std::string> get_view_items() = 0;

  protected:

    int m_model_id;

    std::string m_component_pseudoname;
    std::string m_view_pseudoname;
    std::string m_name;

  private:
    virtual bool fill_view_component(const std::shared_ptr<IModelComponentViewItem>& component,
                                     const std::string& label) const = 0;
};
} // namespace daisi

#endif
