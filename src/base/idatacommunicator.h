#ifndef DAISI_IDATA_COMMUNICATOR_H
#define DAISI_IDATA_COMMUNICATOR_H

#include <list>
#include <map>
#include <string>
#include <vector>

#include <vector>

namespace daisi
{
using q_res_t = std::pair<bool, std::list<std::map<std::string, std::string>>>;

class IDataCommunicator
{
  public:
    template <class T, class... Args>
    static IDataCommunicator& get(const Args&... args) noexcept
    {
        if (!m_instance)
        {
            m_instance = new T(args...);
        }
        return *m_instance;
    }

    static IDataCommunicator& get()
    {
        if (!m_instance)
        {
            throw std::runtime_error("instance is not created yet");
        }
        return *m_instance;
    }

    virtual ~IDataCommunicator();

    virtual q_res_t get_model_view_json(const int model_id, const std::string& component_name,
                                        const std::string& pseudoname_name, const bool is_result,
                                        const std::string& name) noexcept = 0;

    virtual q_res_t get_model_components_views(const int model_id) noexcept = 0;

    virtual int add_results(const int model_id, const std::string& solver_name,
                            const std::string& name) = 0;

    virtual bool set_result_status(const int results_id, const std::string& status) noexcept;

    virtual bool set_result_progress(const int results_id, const double& progress) noexcept;

    virtual bool set_model_component_valid(const int model_id, const std::string& component_name,
                                           const bool is_valid, const std::string& name) noexcept;

    virtual bool set_result_data(const int results_id, const std::string& plot_label,
                                 const std::vector<std::vector<float>>& x_data,
                                 const std::vector<std::vector<float>>& y_data) noexcept;

    virtual bool set_result_data(const int results_id, const std::string& plot_label,
                                 const std::vector<std::vector<float>>& x_data,
                                 const std::vector<std::vector<float>>& y_data,
                                 const std::vector<std::string>&        labels) noexcept;

    virtual bool set_result_text_data(const int results_id, const std::string& label,
                                      const std::string& content) noexcept;

    virtual q_res_t get_tasks() noexcept;

    virtual bool set_current_log_data(const std::string& user_name,
                                      const std::string& content_in) noexcept;

    virtual bool set_task_log_data(const int& user_name, const std::string& content_in) noexcept;

    virtual bool clear_current_log_data(const std::string& user_name) noexcept;

  private:
    static IDataCommunicator* m_instance;
};
} // namespace daisi

#endif
