
#include <common_tools/boostlog.hpp>

#include "../common/keywords.h"
#include "imodelcomponent.h"
#include "imodelcomponentview.h"

namespace daisi
{

bool IModelComponent::from_model_view(const std::shared_ptr<IModelComponentView>& view) noexcept
{
    BL_FTRACE();
    auto name = view->get_name();

    try
    {
        auto is_editable = view->get_is_editable();
        if (!is_editable)
        {
            BL_WARNING() << "it is impossble to load model from non-editable view";
            return false;
        }

        auto result = from_model_view(name, view->get_view_items());

        return result;
    }
    catch (const std::exception& ex)
    {
        BL_ERROR() << "error load component: " << name << ex.what();

        return false;
    }
}

std::pair<bool, std::string>
IModelComponent::extract_content(const std::string fail_msg,
                                 const std::map<std::string, std::string>& content,
                                 const std::string& label)
{
    auto result = std::make_pair(false, std::string(""));

    auto it = content.find(label);

    if (content.end() == it)
    {
        BL_ERROR() << fail_msg << " : expected view item label \"" << label
                   << "\", but not exists in model view";
        return result;
    }
    result.first  = true;
    result.second = it->second;
    return result;
}
}
