#ifndef DAISI_I_MODEL_H
#define DAISI_I_MODEL_H

#include <list>
#include <map>

#include <boost/property_tree/ptree.hpp>

#include <common_tools/child_factory.hpp>
#include <common_tools/service_locator.hpp>

namespace daisi
{

class IModelComponent;
class IModelComponentView;

class IModel
{
  public:
    void add_component(const std::shared_ptr<IModelComponent>& component,
                       const std::string&                      name) noexcept;

    bool change_component(const std::string&                          comp_name,
                          const std::shared_ptr<IModelComponentView>& view) noexcept;

    template <class T, class... Args>
    std::shared_ptr<T> get_component(const Args&... args) const noexcept
    {
        return m_components.get<T>(args...);
    }

  private:
    commtools::ServiceLocator<IModelComponent> m_components;
};

// std::unique_ptr<IModel>
// build_model(const std::string name,
//             const std::map<std::string, std::shared_ptr<IModelComponentView>>& view) noexcept;
} // namespace daisi

#endif
