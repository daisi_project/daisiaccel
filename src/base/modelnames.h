#ifndef DAISIACCEL_MODEL_NAMES_H
#define DAISIACCEL_MODEL_NAMES_H

#include <string>

namespace daisi
{
namespace modelnames
{
extern const std::string Synchrotron;
extern const std::string RFQ;
}
}

#endif
