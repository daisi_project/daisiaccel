#include <chrono>
#include <thread>

#include <common_tools/boostlog.hpp>
#include <common_tools/threadpool.hpp>

#include <serreflection/defines.hpp>
#include <serreflection/read_json.hpp>

#include "base/idatacommunicator.h"
#include "base/imodel.h"
#include "base/imodelcomponent.h"
#include "base/imodelcomponentview.h"
#include "base/isolver.h"
#include "common/standartview.h"

#include "common/flow.h"
#include "synchrotron/opticsequence.h"

#include "controller.h"

using namespace std::chrono_literals;

namespace daisi
{
struct ComponentViewInfo
{
    std::string component_type;
    std::string label;
    std::string name;
};
} // namespace daisi

SERIALIZIBLE_STRUCT(daisi::ComponentViewInfo, srfl::CheckModes::FATAL,
                    (std::string, component_type, DEF_D())(std::string, label,
                                                           DEF_D())(std::string, name, DEF_D()))

namespace daisi
{

DaisiController::DaisiController(const int n_tasks) noexcept : m_n_tasks(n_tasks)
{
}

DaisiController::~DaisiController() = default;

bool DaisiController::check_model_component(const int          model_id,
                                            const std::string& component_pseudoname,
                                            const std::string& view_pseudoname,
                                            const std::string& name) noexcept
{
    BL_DEBUG() << "check model component: " << component_pseudoname << ", view: " << view_pseudoname
               << ", model_id = " << model_id;

    auto result =
        make_model_component(model_id, component_pseudoname, view_pseudoname, name) ? true : false;

    IDataCommunicator::get().set_model_component_valid(model_id, component_pseudoname, result,
                                                       name);

    return result;
}

std::shared_ptr<IModelComponent>
DaisiController::make_model_component(const int model_id, const std::string& component_pseudoname,
                                      const std::string& view_pseudoname,
                                      const std::string& name) noexcept
{
    BL_DEBUG() << "make model component: " << component_pseudoname << ", view: " << view_pseudoname
               << ", model_id = " << model_id;

    std::string fail_msg = "Failed to make component (" + component_pseudoname + ")";

    auto view = std::static_pointer_cast<IModelComponentView>(
        std::make_shared<StandartView>(model_id, component_pseudoname, view_pseudoname, name));

    auto component = IModelComponent::make_child(component_pseudoname);

    if (!component)
    {
        BL_ERROR() << fail_msg << ": component with name: " << component_pseudoname
                   << " is not exist in current DAISI version ";
        return nullptr;
    }

    if (!component->from_model_view(view))
    {
        BL_ERROR() << fail_msg << ": error read from view";
        return nullptr;
    }

    BL_DEBUG() << "success make component " << component_pseudoname;

    return component;
}

std::shared_ptr<ISolver> DaisiController::make_solver(const int          model_id,
                                                      const std::string& solver_pseudoname,
                                                      const std::string& name) noexcept
{
    BL_DEBUG() << "make solver: " << solver_pseudoname << ", model_id = " << model_id;

    auto solver = std::static_pointer_cast<ISolver>(
        make_model_component(model_id, solver_pseudoname, "", name));

    return solver;
}

bool DaisiController::add_task(const int model_id, const std::string& solver_pseudoname,
                               const std::string& name) noexcept
{
    BL_DEBUG() << "add_task: " << solver_pseudoname << ", model_id = " << model_id;
    ;

    auto model  = make_model(model_id);
    auto solver = make_solver(model_id, solver_pseudoname, name);

    if (!model || !solver)
    {
        return false;
    }

    try
    {
        IDataCommunicator::get().add_results(model_id, solver_pseudoname, name);
    }
    catch (const std::exception& ex)
    {
        BL_ERROR() << "error add task: " << ex.what();
        return false;
    }

    BL_DEBUG() << "success add task: " << solver_pseudoname;

    return true;
}

bool DaisiController::run_calculation(const int results_id, const int model_id,
                                      const std::string& solver_pseudoname,
                                      const std::string& name) noexcept
{
    if (!IDataCommunicator::get().set_result_status(results_id, "In_process"))
    {
        return false;
    }

    auto result = run_calculation_inner(results_id, model_id, solver_pseudoname, name);

    const std::string status = result ? "Done" : "Error";

    if (!IDataCommunicator::get().set_result_status(results_id, status))
    {
        return false;
    }

    if (!IDataCommunicator::get().set_result_progress(results_id, 100.0))
    {
        return false;
    }

    return result;
}

bool DaisiController::run_calculation_inner(const int results_id, const int model_id,
                                            const std::string& solver_pseudoname,
                                            const std::string& name) noexcept
{
    BL_DEBUG() << "run calculation: " << solver_pseudoname << ", model_id = " << model_id;

    auto model = make_model(model_id);

    if (!model)
    {
        return false;
    }

    auto solver = make_solver(model_id, solver_pseudoname, name);

    if (!solver)
    {
        return false;
    }

    return solver->run(results_id, std::move(model));
}

std::unique_ptr<IModel> DaisiController::make_model(const int model_id) noexcept
{
    BL_DEBUG() << "make model, model_id = " << model_id;
    ;

    std::string fail_msg = "failed to make model (id=" + std::to_string(model_id) + ")";

    auto model = std::make_unique<IModel>();

    auto components_views = IDataCommunicator::get().get_model_components_views(model_id);

    if (!components_views.first)
    {
        return nullptr;
    }

    for (const auto& component : components_views.second)
    {
        auto it = component.find("get_model_components_views_json");

        std::shared_ptr<ComponentViewInfo> v;
        if (it == component.end() || !(v = srfl::read_json_string<ComponentViewInfo>(it->second)))
        {
            BL_ERROR() << fail_msg << ": unexpected ComponentViewInfo data format";
            return nullptr;
        }
        auto component_ptr = make_model_component(model_id, v->component_type, v->label, v->name);
        if (!component_ptr)
        {
            return nullptr;
        }
        model->add_component(component_ptr, v->name);
    }

    BL_TRACE() << "success make model with id = " << model_id;

    return model;
}

bool DaisiController::run() noexcept
{
    BL_DEBUG() << "run";

    while (1)
    {
        auto tasks = IDataCommunicator::get().get_tasks();

        if (tasks.first)
        {

            BL_DEBUG() << "request tasks: " << tasks.second.size() << " in quewe";
            // BL_DEBUG() << "free tasks: " << m_n_tasks;

            if (!tasks.second.empty())
            {
                auto current_task = *tasks.second.begin();

                auto id = current_task.at("id");

                auto f = [&, current_task]() {
                    BL_INFO() << "run async task: " << id;

                    // m_n_tasks--;
                    run_calculation(std::stoi(id), std::stoi(current_task.at("model_id")),
                                    current_task.at("solver_pseudoname"), current_task.at("name"));
                    // m_n_tasks++;
                    BL_INFO() << "finish async task";
                };
                commtools::ThreadPool::get().run_async_master(id, f);
            }
        }

        std::this_thread::sleep_for(500ms);
    }
}
} // namespace daisi
