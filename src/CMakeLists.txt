cmake_minimum_required(VERSION 2.8)

aux_source_directory(synchrotron SRC)
aux_source_directory(synchrotron/solvers SRC)

aux_source_directory(gesa1d SRC)

aux_source_directory(base SRC)
aux_source_directory(common SRC)
aux_source_directory(json SRC)
aux_source_directory(database SRC)
aux_source_directory(. SRC)

add_definitions(-DLOG_CHANNEL=DAISI)

add_library(${PROJECT_NAME} SHARED ${SRC})
target_link_libraries(
                     ${PROJECT_NAME}
                     ${Boost_LIBRARIES}
                     ${PQXX_LIBRARIES}
                     ${ARMADILLO_LIBRARIES}
                     common_tools
                     madx
                     gfortran
                     notk
                     )
